import { WebcamThumbModule } from './webcam-thumb.module';

describe('WebcamThumbModule', () => {
  let webcamSharedModule: WebcamThumbModule;

  beforeEach(() => {
    webcamSharedModule = new WebcamThumbModule();
  });

  it('should create an instance', () => {
    expect(webcamSharedModule).toBeTruthy();
  });
});
