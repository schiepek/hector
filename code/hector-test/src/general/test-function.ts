import {TestExecutor} from '../util/test-executor';
import {TestConfig} from '../util/test-config';

export interface TestFunction {
    (executor: TestExecutor, config: TestConfig): void;
}