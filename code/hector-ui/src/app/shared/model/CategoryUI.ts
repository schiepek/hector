import {CategoryItemType} from '../../../../../hector-api/src/model/category-item-type';

export class CategoryUI {

  code: string;
  name: string;
  size: number;
  link: string;
  type: CategoryItemType;

  constructor(code: string, name: string, size: number, link: string, type: CategoryItemType) {
    this.code = code;
    this.name = name;
    this.size = size;
    this.link = link;
    this.type = type;
  }
}
