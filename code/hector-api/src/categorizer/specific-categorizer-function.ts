import {Cam} from '../model/cam';

export interface SpecificCategorizerFunction {
    (cam: Cam): boolean;
}
