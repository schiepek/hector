import {Cam} from '../model/cam';
import {Loader} from './loader';
import {PropertyEvaluator} from '../util/property-evaluator';
import {DateUtil} from '../util/date-util';
import {HectorConfig} from '../util/hector-config';
import {Categorizer} from '../categorizer/categorizer';
import {provider} from '../model/provider';

export class BongacamsLoader extends Loader {

    constructor(categorizer: Categorizer) {
        super(categorizer, provider.BONGACAMS);
    }

    public getApiUrls(): string[] {
        return HectorConfig.getBongacamsUrls();
    }

    protected getJsonDataObject(res: any): any {
        return JSON.parse(res);
    }

    protected readProperties(camObj: any): Cam {
        let username = camObj["username"];
        let age = camObj["display_age"];
        let isPrivate = camObj["chat_status"] != "public";
        let isNew = DateUtil.dateIsNewerThan10Days(new Date(camObj["signup_date"]));
        let numberOfUsers = camObj["members_count"];
        let gender = PropertyEvaluator.getGenderByString(camObj["gender"]);
        let isHd = false;
        let imageUrl = (camObj["profile_images"]) ? camObj["profile_images"]["thumbnail_image_big_live"]:null;
        let chatRoomUrl = camObj["chat_url_on_home_page"];
        let embedUrl = camObj["embed_chat_url"];
        let title = "";
        let tags = camObj["tags"];
        let turnsOn = camObj["turns_on"];
        let turnsOff = camObj["turns_off"];
        let location = camObj["hometown"];
        let languages = camObj["primary_language_key"] + ((camObj["secondary_language_key"]) ? "," + camObj["secondary_language_key"] : "");
        let languageKeys = PropertyEvaluator.getLanguageKeysByString(languages);
        let country = PropertyEvaluator.getCountryByString(location);
        let hairColor = PropertyEvaluator.getHairColorByString(camObj["hair_color"]);
        let body = "";

        return Cam.of(username, age, isPrivate, isNew, numberOfUsers, gender, isHd, imageUrl, chatRoomUrl, embedUrl, null, title, tags, turnsOn, turnsOff, hairColor, body, location, country, languages, languageKeys, this.getProvider());
    }


}
