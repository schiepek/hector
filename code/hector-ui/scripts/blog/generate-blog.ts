let fs = require('fs');

let allEntries = [];
let pageSize = 10;
let currentPage = 1;
let pageEntries = [];
let maxPage;

let writePage = (page, entries) => {
  let finalJson = {
    "entries": entries,
    "maxPage": maxPage
  }

  fs.writeFile("../../src/assets/blog/blog_entries_" + page + ".json", JSON.stringify(finalJson), function (err) {
      if (err) throw err;
      console.log('complete: ' + "blog_entries_" + page + ".json");
    }
  );
}

// load all entries in specific blog folders
fs.readdirSync("../../src/assets/blog/").forEach(file => {
  console.log(file);
  if (!file.includes(".")) {
    let jsonString = fs.readFileSync("../../src/assets/blog/" + file + "/entry.json", "utf8");
    allEntries.push(JSON.parse(jsonString)["entry"])
  }
});

maxPage = Math.floor(allEntries.length / pageSize) + 1;

let sortedEntries = allEntries.sort((a, b) => {
  return new Date(b["date"]).getTime() - new Date(a["date"]).getTime();
})

for (let i = 0; i < sortedEntries.length; i++) {
  let entry = sortedEntries[i];

  if (pageEntries.length == pageSize) {
    writePage(currentPage, pageEntries);
    currentPage++;
    pageEntries = []
  } else {
    pageEntries.push(entry);
  }
}

if (pageEntries.length > 0) {
  writePage(currentPage, pageEntries)
}
