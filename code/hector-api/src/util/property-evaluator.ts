import {StringUtil} from './string-util';
import countries from '../resources/countries.js'
import countriesDirect from '../resources/countries_direct.js'
import haircolors from '../resources/haircolors.js'
import genders from '../resources/genders.js'
import languages from '../resources/languages.js'
import bodies from '../resources/bodies.js'
import bodiesexclude from '../resources/bodies_exclude.js'
import undetectable from '../resources/undetectable.js'
import undetectableDirect from '../resources/undetectable_direct.js'

export class PropertyEvaluator {

    public static getCountryByString(str: string): string {
        if (str) {
            let candidates = this.prepareCountryString(str).split(" ");
            let exactDirectKey = this.getKeyByEqualsListMultiple(candidates, countriesDirect);
            let exactKey = this.getKeyByEqualsListMultiple(candidates, countries);
            return exactDirectKey ? exactDirectKey : exactKey ? exactKey : this.getKeyByContainsList(str, countries);
        }
        return "";

    }

    private static prepareCountryString(str: string): string {
        let countryString = str.replace(",", " ").replace("  ", " ");
        countryString = countryString.replace("united states", "unitedstates");
        countryString = countryString.replace("united kingdom", "unitedkingdom");
        return countryString;
    }

    public static getHairColorByString(str: string): string {
        return this.getKeyByContainsList(str, haircolors);
    }

    public static getGenderByString(str: string): string {
        return this.getKeyByContainsList(str, genders);
    }

    public static getLanguageKeysByString(str: string): string[] {
        return this.getKeysByContainsList(str, languages);
    }

    public static getBodyByString(str: string): string {
        let suggestedBody = this.getKeyByContainsList(str, bodies);
        let excludeStrings = this.getBodyExcludeStingsByKey(suggestedBody);
        if (excludeStrings && excludeStrings.some(sb => StringUtil.containsCaseIns(str, sb))) {
            return "";
        }
        return suggestedBody;
    }

    public static getBodyExcludeStingsByKey(key: string): string[] {
        return bodiesexclude[key];
    }

    public static getUndetectableLocations(): string[] {
        return undetectable["undetectable"];
    }

    public static getUndetectableDirectLocations(): string[] {
        return undetectableDirect["undetectable"];
    }

    private static getKeyByEqualsListMultiple(candidates: string[], list: any): string {
        for (let key in list) {
            for (let possibleKeyWord of list[key]) {
                for (let candidate of candidates) {
                    if (StringUtil.equalsCaseIns(candidate, possibleKeyWord)) {
                        return key.toLowerCase();
                    }
                }

            }
        }
        return "";
    }

    private static getKeyByContainsList(str: string, list: any): string {
        for (let key in list) {
            for (let possibleKeyWord of list[key]) {
                if (StringUtil.containsCaseIns(str, possibleKeyWord)) {
                    return key.toLowerCase();
                }
            }
        }
        return "";
    }

    private static getKeysByContainsList(str: string, list: any): string[] {
        let keys: string[] = [];

        for (let key in list) {
            for (let possibleKeyWord of list[key]) {
                if (StringUtil.containsCaseIns(str, possibleKeyWord)) {
                    keys.push(key.toLowerCase());
                }
            }

        }
        return keys;
    }

}
