import {NgModule} from '@angular/core';
import {ContentComponent} from '../../components/content/content.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', component: ContentComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  declarations: [
    ContentComponent
  ]
})
export class ContentModule {
}
