import {TranslateLoader} from '@ngx-translate/core';
import {Observable, of} from 'rxjs';
import translationEn from '../../../assets/i18n/en.json';

const TRANSLATIONS = {
  en: translationEn,
};

export class TranslateUniversalLoader implements TranslateLoader {

  public getTranslation(lang: string): Observable<any> {
    return of(TRANSLATIONS[lang]);
  }
}
