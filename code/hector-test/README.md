# HectorTest

## Commands for the testing the hector REST API

The configurations elements for testing are located in config.json

* npm test --endpoint=endpoint-local (for localhost)
* npm test --endpoint=endpoint-dev (for api-gateway dev endpoint)
* npm test --endpoint=endpoint-prod (for api-gateway prod endpoint)

* npm test --endpoint=endpoint-dev --function=category-asian (invoke just one test)
* npm test --testhelp (to see al possible functions)

* npm test --type=ALL (ALL, UI, BACKEND, WARMUP)
