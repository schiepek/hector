import {Cam} from '../model/cam';
import {Loader} from './loader';
import {PropertyEvaluator} from '../util/property-evaluator';
import {HectorConfig} from '../util/hector-config';
import {Categorizer} from '../categorizer/categorizer';
import {provider} from '../model/provider';
import {TagEvaluator} from '../util/tag-evaluator';

export class AwEmpireLoader extends Loader {

    constructor(categorizer: Categorizer) {
        super(categorizer, provider.AWEMPIRE);
    }

    public getApiUrls(): string[] {
        return HectorConfig.getAwEmpireUrls();
    }

    protected getJsonDataObject(res: any): any {
        return JSON.parse(res)["data"]["models"];
    }

    protected readProperties(camObj: any): Cam {
        let username = camObj["performerId"];
        let age = camObj["persons"][0]["age"];
        let isPrivate = camObj["status"] != "free_chat";
        let isNew = false;
        let numberOfUsers = 0;
        let gender = PropertyEvaluator.getGenderByString(camObj["category"]);
        let isHd = camObj["details"]["streamQuality"] > 8;
        let imageUrl = camObj["profilePictureUrl"]["size320x180"];
        let chatRoomUrl = camObj["chatRoomUrl"];
        let embedUrl = "";
        let title = camObj["details"]["about"]["biography"];
        let tags = TagEvaluator.executeForCam(AwEmpireLoader.loadTags(camObj), title);
        let turnsOn = camObj["details"]["about"]["turnsOn"]
        let turnsOff = camObj["details"]["about"]["turnsOff"]
        let location = camObj["location"];
        let languages = camObj["details"]["languages"].toString();
        let languageKeys = PropertyEvaluator.getLanguageKeysByString(languages);
        let country = (camObj["country"] && camObj["country"].length <= 2) ? camObj["country"].toLowerCase() : "";
        let hairColor = PropertyEvaluator.getHairColorByString(camObj["persons"][0]["body"]["hairColor"]);
        let body = PropertyEvaluator.getBodyByString(camObj["persons"][0]["body"]["build"])

        return Cam.of(username, age, isPrivate, isNew, numberOfUsers, gender, isHd, imageUrl, chatRoomUrl, embedUrl, null, title, tags, turnsOn, turnsOff, hairColor, body, location, country, languages, languageKeys, this.getProvider());
    }

    private static loadTags(camObj: any) : string[] {
        let tags = camObj["details"]["willingnesses"];
        let appearances = camObj["details"]["appearances"];

        if(tags && appearances) {
            (tags as string[]).push(...(appearances as string[]))
        }
        if(tags) {
            return tags;
        }
        return [];
    }

}
