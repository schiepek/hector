import {Injectable} from '@angular/core';
import {FooterText} from '../shared/model/footer-text';
import {ActivatedRoute} from '@angular/router';
import {Parameters} from '../shared/model/parameters';
import {Location} from '@angular/common';

@Injectable()
export class StateService {

  page: number = 1;
  minAge?: number;
  maxAge?: number;
  gender?: string;
  isHd?: boolean;
  isNew?: boolean;
  country?: string;
  language?: string;
  haircolor?: string;
  body?: string;
  category?: string;
  tag: string;
  query?: string;
  username?: string;
  provider?: string;
  footerText?: FooterText;
  acHovered: boolean;

  /** These properties won't be reset */
  tracker: string;
  searchTerm: string;

  public reset(): void {
    this.page = 1;
    this.minAge = null;
    this.maxAge = null;
    this.gender = null;
    this.isHd = null;
    this.isNew = null;
    this.country = null;
    this.language = null;
    this.haircolor = null;
    this.body = null;
    this.category = null;
    this.tag = null;
    this.query = null;
    this.username = null;
    this.provider = null;
    this.footerText = null;
    this.acHovered = null;
  }

  public setOldState(oldState: any) {
    this.minAge = oldState.minAge;
    this.maxAge = oldState.maxAge;
    this.gender = oldState.gender;
    this.isHd = oldState.isHd;
    this.isNew = oldState.isNew;
    this.country = oldState.country;
    this.language = oldState.language;
    this.haircolor = oldState.haircolor;
    this.body = oldState.body;
    this.category = oldState.category;
    this.tag = oldState.tag;
    this.query = oldState.query;
    this.username = oldState.username;
    this.provider = oldState.provider;
    this.footerText = oldState.footerText;
  }

  public getTracker(route: ActivatedRoute, location: Location): Promise<string | undefined> {
    if (!this.tracker && route.queryParams) {
      return new Promise(resolve => {
        route.queryParams.subscribe(queryParams => {
          this.tracker = queryParams[Parameters.TRACKER];
          location.replaceState(location.path().split('?')[0], ''); //remove query params from url
          resolve(this.tracker);
        });
      })
    }
    return new Promise(resolve => resolve(this.tracker));
  }

}
