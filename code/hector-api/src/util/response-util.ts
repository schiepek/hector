interface HectorResponse {
    statusCode: number;
    headers: any;
    body: string;
}

export class ResponseUtil {

    static json(body: any): HectorResponse {
        return {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Cache-Control": "no-cache"
            },
            body: JSON.stringify(body)
        };
    }

    static string(body: any): HectorResponse {
        return {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*", // Required for CORS support to work
            },
            body: body
        };
    }

    static notAuthorized(): HectorResponse {
        return {
            statusCode: 511,
            headers: {},
            body: "no_authorized_request"
        };
    }

    static internalServerError(): HectorResponse {
        return {
            statusCode: 500,
            headers: {},
            body: "internal server error"
        };
    }

    static notFound(): HectorResponse {
        return {
            statusCode: 404,
            headers: {},
            body: "not_found"
        };
    }

}