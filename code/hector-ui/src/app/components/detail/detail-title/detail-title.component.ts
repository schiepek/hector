import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {StateService} from '../../../services/state.service';
import {ScreenService} from '../../../services/screen.service';
import {Cam} from '../../../../../../hector-api/src/model/cam';
import {CountryService} from '../../../services/country.service';
import {CategoryService} from '../../../services/category.service';
import {TagService} from '../../../services/tag.service';

@Component({
  selector: 'app-detail-title',
  templateUrl: './detail-title.component.html',
  styleUrls: ['./detail-title.component.css']
})
export class DetailTitleComponent implements OnInit, OnChanges {

  @Input()
  public cam: Cam;

  @Input()
  public countryUrl: string;

  @Input()
  public isRandom: boolean;

  @Input()
  public hasPreviousButton: Function;

  @Input()
  public nextRandomCam: Function;

  @Input()
  public previousRandomCam: Function;

  public moreUrl: string;
  public moreLabel: string;
  public moreLabelCode: string;

  constructor(public countryS: CountryService,
              public state: StateService,
              private tag: TagService,
              public screen: ScreenService,
              private categoryS: CategoryService) {
  }


  ngOnInit() {
    this.initMoreSection();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.ngOnInit();
  }

  private initMoreSection(): void {
    if (this.cam.country) {
      this.moreUrl = this.countryUrl;
      this.moreLabel = this.countryS.getAdjective(this.cam.country);
    } else if (this.cam.categories.length > 0) {
      this.setUrlAndLabelCode(this.cam.categories[0]);
    } else {
      let firstTag = this.tag.findFirstTag(this.cam.tags);
      if (firstTag) {
        this.setUrlAndLabelCode(firstTag, true);
      } else {
        this.setUrlAndLabelCode(this.cam.gender);
      }
    }
  }

  private setUrlAndLabelCode(value: string, isTag?: boolean) {
    this.moreUrl = this.categoryS.findCategory(value, null).path;
    this.moreLabelCode = isTag ? value : 'categories.' + value;
  }

}
