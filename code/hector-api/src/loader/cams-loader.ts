import {Loader} from './loader';
import {ChaturbateLoader} from './chaturbate-loader';
import {BongacamsLoader} from './bongacams-loader';
import {AwEmpireLoader} from './awempire-loader';
import {Cam} from '../model/cam';
import {Categorizer} from '../categorizer/categorizer';
import {CamResult} from '../model/cam-result';
import {S3Util} from '../persistence/s3-util';
import {CamSodaLoader} from './camsoda-loader';
import {StripchatLoader} from './stripchat-loader';
import {ProviderCamResult} from '../model/provider-cam-result';

export class CamsLoader {

    private static instance: CamsLoader;
    private categorizer: Categorizer = new Categorizer();

    public loaders: Loader[] =
        [
            new ChaturbateLoader(this.categorizer),
            new BongacamsLoader(this.categorizer),
            new AwEmpireLoader(this.categorizer),
            new CamSodaLoader(this.categorizer),
            new StripchatLoader(this.categorizer)
        ];

    public static getInstance() {
        return this.instance || (this.instance = new this());
    }

    public getLoader(eventName: string) : Loader {
        return this.loaders.find(l => eventName.includes(l.getProvider()));
    }

    public loadAndSaveProviderCams(loader: Loader): Promise<ProviderCamResult> {
        let newCams: Cam[] = [];

        return new Promise<CamResult>((resolve, reject) => {
            loader.loadCams()
                .then(camsObject => {

                    // fill up loaded cams
                    for (let i = 0; i < camsObject.length; i++) {
                        let loadedCams = camsObject[i] as Array<Cam>;
                        newCams.push(...loadedCams);
                    }

                    if(newCams && newCams.length > 0) {
                        CamsLoader.createResult(newCams, loader.getProvider(), resolve)
                    } else {
                        console.error("the loaded cams are empty: " + loader.getProvider());
                    }

                }).catch((error: any) => {
                    console.error("during execution of loader " + loader.getProvider());
                    console.error(error);
                    reject();
            })
        });
    }

    private static createResult(newCams: Cam[], provider: string, resolve: any) {
        S3Util.saveProviderCamsToS3(
            new ProviderCamResult(newCams, newCams.length, new Date()),
            provider,
            resolve
        )
    }

}
