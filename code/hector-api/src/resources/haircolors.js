export default
{
    "blonde": [
        "blond",
        "rubia",
        "hairColorBlonde"
    ],
    "redhead": [
        "redhair",
        "red hair",
        "redhead",
        "fire red",
        "pink",
        "auburn",
        "hairColorRed"
    ],
    "black": [
        "blackhair",
        "black hair",
        "black",
        "hairColorBlack"
    ],
    "brunette": [
        "brunette",
        "brown hair",
        "brunet",
        "brown"
    ]
}
