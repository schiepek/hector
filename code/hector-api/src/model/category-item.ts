import {CategoryItemType} from './category-item-type';

class SpecificSize {

    filter: string;
    size: number;

    constructor(filter: string, size: number) {
        this.filter = filter;
        this.size = size;
    }
}

export class CategoryItem {

    code: string;
    type: CategoryItemType;
    size: number = 1;
    specificSizes: SpecificSize[]

    constructor(code: string, type: CategoryItemType, size: number) {
        this.code = code;
        this.type = type;
        this.size = size;
    }

    public static initCategoryItemToSave(code: string, type: CategoryItemType, specificFilters: string[]): CategoryItem {
        let categoryItem = new CategoryItem(code, type, 1);
        categoryItem.specificSizes = [];
        if (specificFilters && specificFilters.length > 0) {
            specificFilters.forEach(specificFilter => {
                categoryItem.specificSizes.push(new SpecificSize(specificFilter, 1));
            })
        }
        return categoryItem;
    }

    public increaseSize(specificFilters: string[]) {
        this.size += 1;
        if (specificFilters && specificFilters.length > 0) {
            specificFilters.forEach(specificFilter => {
                let specificSize = this.specificSizes.find(sS => sS.filter === specificFilter);
                if (specificSize) {
                    specificSize.size += 1;
                } else {
                    this.specificSizes.push(new SpecificSize(specificFilter, 1))
                }
            })
        }
    }

}
