declare var $: any;

export class AlchemistsUtil {

  public static async initJQuery(): Promise<void> {
    return new Promise<void>((resolve => {
      "use strict";

      $.fn.exists = function () {
        return this.length > 0;
      };

      resolve();
    }));
  }

}
