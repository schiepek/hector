import {CategorizerFunction} from './categorizer-function';
import {Cam} from '../model/cam';
import {CamUtil} from '../util/cam-util';
import {LatinaCategorizer} from './latina-categorizer';
import {CategoryCode} from '../model/category-code';

export class AsianCategorizer extends CategorizerFunction {

    protected readonly possibleCountries: string[] = ["cn","fj","hk","id","jp","kp","ph","sg","kr","tw","th","vn"];
    protected readonly exlucdeWords: string[] = ["german","french","ebony","latina"];

    constructor() {
        super(CategoryCode.asian,
            (cam) => {
                return this.isAsianCountry(cam) ||
                    (CamUtil.containsBasicInfoButNot(cam, this.exlucdeWords, "asian") && this.isNotLatinCountry(cam));
            });
    }

    public isAsianCountry(cam: Cam) : boolean {
        return this.possibleCountries.filter(c=>cam.country===c).length>0;
    }

    public isNotLatinCountry(cam: Cam): boolean {
        return !new LatinaCategorizer().isLatinCountry(cam);
    }

}