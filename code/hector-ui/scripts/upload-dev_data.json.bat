set S3BUCKET=%1

echo 'upload data.json to %S3BUCKET%'
call aws s3 cp ../../src/assets/data/data.json %S3BUCKET%/assets/data/data.json --profile hector --acl public-read --cache-control max-age=2592000,public

echo 'upload done'

pause
