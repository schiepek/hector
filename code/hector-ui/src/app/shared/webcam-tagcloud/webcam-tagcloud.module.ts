import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TagCloudComponent} from '../../components/webcam/webcam-tagcloud/tag-cloud.component';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    RouterModule
  ],
  declarations: [TagCloudComponent],
  exports: [TagCloudComponent]
})
export class WebcamTagcloudModule { }
