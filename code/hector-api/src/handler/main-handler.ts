import {Handler, Context, Callback} from 'aws-lambda';
import {handleWarmup} from './warmup-handler';
import {handleCams} from './cams-handler';

const main: Handler = (event: any, _context: Context, callback: Callback) => {
    let func = retrieveFunction(event);

    if (func.startsWith("warmup")) {
        handleWarmup(func, _context, callback);
    } else {
        handleCams(func, event, callback)
    }
};

function retrieveFunction(event: any): string {
    if (event["name"]) {
        return event["name"];
    } else if (event.queryStringParameters) {
        return event.queryStringParameters["function"];
    } else {
        return "home";
    }
}

export {main}
