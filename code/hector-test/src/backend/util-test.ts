import {AbstractTest} from './abstract-test';
import {TestType} from '../general/test-type';
import {TagEvaluator} from '../../../hector-api/src/util/tag-evaluator';
import {expect} from 'chai';
import {Cam} from '../../../hector-api/src/model/cam';
import {CategoryItemType} from '../../../hector-api/src/model/category-item-type';
import {CategoryItem} from '../../../hector-api/src/model/category-item';
import {assert} from 'chai';

export class UtilTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {

        this.tests.set("tag-evaluator-cam", (executor) => {

            executor.executeTest("should evaluate tags correctly", () => {
                UtilTest.evaluateTagAndAssert(["squirt", "anal"], "fetish hot girl",
                    ["squirt", "anal", "fetish"])
                UtilTest.evaluateTagAndAssert(["squirt", "anal"], "fetish, hot, girl",
                    ["squirt", "anal", "fetish"])
                UtilTest.evaluateTagAndAssert(["squirt", "anal"], "fetish,big-ass #interracial, #pantyhose #notexisting",
                    ["squirt", "anal", "fetish", "bigass", "interracial", "pantyhose"], ["notexisting"])
                UtilTest.evaluateTagAndAssert(["squirt", "anal"], "squirt,  anal,   fetish",
                    ["squirt", "anal", "fetish"])
                UtilTest.evaluateTagAndAssert(["squirt", "anal"], '',
                    ["squirt", "anal"])
            })

        });

        this.tests.set("tag-evaluator-cams", (executor) => {

            executor.executeTest("should evaluate tags correctly", () => {

                let cam = new Cam();
                cam.country = "de";
                cam.tags = ["squirt", "nonexisting"];
                cam.categories = ["asian", "tattoo"];

                let cam2 = new Cam();
                cam2.tags = ["anal"];
                cam2.country = "de";
                cam2.categories = [];

                let categoryItems = TagEvaluator.executeForCams([cam, cam2]);

                UtilTest.assertCategoryItem("de", CategoryItemType.COUNTRY, 2, categoryItems);
                UtilTest.assertCategoryItem("squirt", CategoryItemType.TAG, 1, categoryItems);
                UtilTest.assertCategoryItem("anal", CategoryItemType.TAG, 1, categoryItems);
                UtilTest.assertCategoryItem("asian", CategoryItemType.CATEGORY, 1, categoryItems);
                UtilTest.assertCategoryItem("tattoo", CategoryItemType.CATEGORY, 1, categoryItems);
                assert.isUndefined(categoryItems.find(c => c.code === "nonexisting"))
            })

        });

    }

    private static evaluateTagAndAssert(tags: string[], title: string, expected: string[], includesNot?: string[]): void {
        let evaluatedTags = TagEvaluator.executeForCam(tags, title);
        expect(evaluatedTags).to.include.members(expected);
        expect(evaluatedTags.length).to.be.eq(expected.length);
        if (includesNot) {
            expect(evaluatedTags).to.not.include.members(includesNot);
        }
    }

    private static assertCategoryItem(code: string, type: CategoryItemType, size: number, categories: CategoryItem[]): void {
        let category = categories.find(c => c.code === code);
        assert.isNotNull(category);
        assert.equal(category!.size, size);
        assert.equal(category!.type, type);
    }


}
