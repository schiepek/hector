import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebcamFilterComponent } from './webcam-filter.component';

describe('WebcamFilterComponent', () => {
  let component: WebcamFilterComponent;
  let fixture: ComponentFixture<WebcamFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebcamFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebcamFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
