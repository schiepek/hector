import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BlogEntryService} from '../../services/blog-entry.service';
import { BlogComponent } from '../../components/blog/blog.component';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {CountryThumbModule} from '../../shared/country-thumb/country-thumb.module';
import {WebcamTagcloudModule} from '../../shared/webcam-tagcloud/webcam-tagcloud.module';
import { BlogPageComponent } from '../../components/blog/blog-page/blog-page.component';
import { BlogEntryComponent } from '../../components/blog/blog-entry/blog-entry.component';

const routes: Routes = [
  {path: '', component: BlogComponent, pathMatch: 'full'}
];

@NgModule({
  declarations: [BlogComponent, BlogPageComponent, BlogEntryComponent],
  imports: [
    CommonModule,
    CountryThumbModule,
    WebcamTagcloudModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
  ],
  providers: [
    BlogEntryService
  ]
})
export class BlogModule { }
