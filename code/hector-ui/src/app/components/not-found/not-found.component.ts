import { Component, OnInit } from '@angular/core';
import {MetaService} from '../../services/meta.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor(private meta: MetaService) { }

  ngOnInit() {
    this.meta.setNotFoundMetaTags();
  }

}
