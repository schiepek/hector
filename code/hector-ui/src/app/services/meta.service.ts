import {Injectable} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {CountryService} from './country.service';
import {Category} from '../shared/model/category';
import {CategoryCode} from '../../../../hector-api/src/model/category-code';
import {StateService} from './state.service';
import {ConstantService} from './constant.service';
import {StringUtil} from '../shared/util/string-util';

@Injectable()
export class MetaService {

  public title: string = "";
  public description: string = "";

  private readonly titleTag: string = "title";
  private readonly descriptionTag: string = "description";
  private readonly keywordsTag: string = "keywords";

  constructor(private meta: Meta,
              private titleS: Title,
              private state: StateService,
              private country: CountryService,
              private constant: ConstantService,
              private translate: TranslateService) {
  }

  public setHomeMetaTags(): void {
    this.setTitle("meta-home.title");
    this.setDescription("meta-home.description");
    this.setKeywords("meta-home.keywords");
  }

  public setCountriesMetaTags(): void {
    this.setTitle("meta-countries.title");
    this.setDescription("meta-countries.description");
    this.setKeywords("meta-home.keywords");
  }

  public setCategoriesMetaTags(filter: string): void {
    this.translate.get("meta-categories.title")
      .subscribe(t =>this.setTitleDirectly(filter ? t + " - " + StringUtil.capitalizeFirstLetter(filter) : t));
    this.translate.get("meta-categories.description")
      .subscribe(t =>this.setDescriptionDirectly(filter ? t + " - " + StringUtil.capitalizeFirstLetter(filter) : t));
  }

  public setSitemapMetaTags(): void {
    this.setTitle("meta-sitemap.title");
    this.setDescription("meta-sitemap.description");
  }

  public setFooterMetaTags(route: string): void {
    this.setTitle("meta-" + route + ".title");
    this.setDescription("meta-" + route + ".description");
  }

  public setDetailMetaTags(username: string, provider: string): void {
    this.setTitle("meta-detail.title", {username: username});
    this.setDescription("meta-detail.description", {username: username, provider: provider});
    this.setKeywords("meta-detail.keywords", {username: username});
  }

  public setRandomMetaTags(): void {
    this.setTitle("meta-random.title");
    this.setDescription("meta-random.description");
  }

  public setNotFoundMetaTags(): void {
    this.setTitle("not-found.title");
    this.setDescription("not-found.description");
  }

  public setGenericMetaTags(page: string): void {
    this.setTitle("meta-" + page + ".title");
    this.setDescription("meta-" + page + ".description");
  }

  public setCamsMetaTags(categories: Category[], page: number): void {
    if (categories.length > 0 && categories[0].code === CategoryCode.query) {
      this.setSearchMetaTags(this.state.query);
    } else if (categories.length > 0 && categories[0].code === CategoryCode.country) {
      this.setCountryMetaTags(this.state.country);
    } else if (categories.length > 0 && categories[0].group === "provider") {
      this.setProviderMetaTags(this.state.provider)
    } else if (categories.length > 0 && categories[0].group === "tag") {
      this.setTagMetaTags(this.state.tag);
    } else if (categories.length > 0 && categories[0].group !== "page") {
      this.setCategoryMetaTags(categories[0].path);
    } else {
      this.setCamsMetaTag(page);
    }
  }


  public setCamsMetaTag(page: number) {
    page && page > 1 ? this.setTitle("meta-cams.title-page", {value: page}) : this.setTitle("meta-cams.title");
    this.setDescription("meta-cams.description");
  }

  public setBlogMetaTag(page: number) {
    page && page > 1 ? this.setTitle("meta-blog.title-page", {value: page}) : this.setTitle("meta-blog.title");
    this.setDescription("meta-blog.description");
  }

  public setMetaTag(title: string, description: string) {
    this.setTitleDirectly(title);
    this.setDescriptionDirectly(description);
  }

  public updateTag(tag: string, content: string) {
    this.meta.updateTag({name: tag, content: content});
  }

  private setSearchMetaTags(query: string): void {
    this.setTitle("meta-search.title", {value: query});
    this.setDescription("meta-search.description", {value: query});
  }

  private setTagMetaTags(tag: string): void {
    this.setTitle("meta-tag.title", {value: tag});
    this.setDescription("meta-tag.description", {value: tag});
  }

  private setCategoryMetaTags(category: string): void {
    this.setTitle("meta-category." + category + "-title");
    this.setDescription("meta-category." + category + "-description");
  }

  private setCountryMetaTags(countryCode: string): void {
    let countryName = this.country.getName(countryCode);
    let countryAdj = this.country.getAdjective(countryCode);
    this.setTitle("meta-country.title", {value1: countryAdj, value2: countryName});
    this.setDescription("meta-country.description", {value1: countryAdj, value2: countryName});
  }

  private setProviderMetaTags(provider: string): void {
    this.translate.get("provider." + provider).subscribe(p => {
      this.setTitle("meta-category.provider-title", {value1: p});
      this.setDescription("meta-category.provider-description", {value1: p})
    });
  }

  private setTitle(translationCode: string, interpolateParams?: Object): void {
    this.translate.get(translationCode, this.getCompleteInterpolateParams(interpolateParams)).subscribe(t => this.setTitleDirectly(t));
  }

  private setDescription(translationCode: string, interpolateParams?: Object): void {
    this.translate.get(translationCode, this.getCompleteInterpolateParams(interpolateParams)).subscribe(t => this.setDescriptionDirectly(t));
  }

  private setKeywords(translationCode: string, interpolateParams?: Object): void {
    this.translate.get(translationCode, interpolateParams).subscribe(t => this.setKeyWordsDirectly(t));
  }

  public getCompleteInterpolateParams(interpolateParams?: Object): Object {
    if (interpolateParams) {
      interpolateParams["subdomain"] = this.constant.getSubDomain();
      return interpolateParams;
    }
    return {subdomain: this.constant.getSubDomain()};
  }

  private setTitleDirectly(content: string): void {
    this.title = content;
    this.titleS.setTitle(content);
    this.updateTag(this.titleTag, content);
  }

  private setDescriptionDirectly(content: string): void {
    this.description = content;
    this.updateTag(this.descriptionTag, content)
  }

  private setKeyWordsDirectly(content: string) {
    this.updateTag(this.keywordsTag, content)
  }

}
