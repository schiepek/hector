import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EmergencyComponent} from '../../components/error/emgergency/emergency.component';

const routes: Routes = [
  {path: '', component: EmergencyComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  declarations: [
    EmergencyComponent
  ]
})
export class EmergencyModule { }
