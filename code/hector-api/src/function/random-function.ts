import {CamFunction} from './cam-function';
import {Cam} from '../model/cam';
import {CamResult} from '../model/cam-result';
import {DataUtil} from '../util/data-util';
import {provider} from '../model/provider';
import {RelatedFunction} from './related-function';
import {PageFunction} from './page-function';

export class RandomFunction {

    private randomCamFunction: CamFunction;
    private offlineFunction: CamFunction;

    private static readonly INCOMPATIBLE_PROVIDERS = [provider.AWEMPIRE];


    constructor(queryStrings: any) {
        if (queryStrings) {
            this.randomCamFunction = new CamFunction("random", (c) => {
                let cams = this.filterLoadedCams(queryStrings, c);
                cams = this.filterGender(queryStrings, cams);
                cams = this.filterIncompatibleProviders(cams);
                return RandomFunction.getRandom(cams);
            });
            this.offlineFunction = new PageFunction(1);
        }
    }

    private filterLoadedCams(queryString: any, cams: Cam[]): Cam[] {
        return queryString["loaded"] ?
            cams.filter(c => !(queryString["loaded"].split(",")).includes(c.username)) :
            cams;
    }

    private filterIncompatibleProviders(cams: Cam[]): Cam[] {
        return cams.filter(c=>!RandomFunction.INCOMPATIBLE_PROVIDERS.includes(c.provider));
    }

    private filterGender(queryString: any, cams: Cam[]): Cam[] {
        return queryString["gender"] ? cams.filter(c=>c.gender === queryString["gender"]) : cams;
    }

    private static getRandom(cams: Cam[]): Cam[] {
        return [cams[Math.floor(Math.random() * cams.length)]];
    }

    /** the first cam is the random cam > the rest are the related cams **/
    public updateResult(result: CamResult, event: any): CamResult {
        let allCams = result.cams;

        let randomCam = this.randomCamFunction.processCams(allCams);

        if (randomCam.length > 0) {
            randomCam = randomCam.concat(new RelatedFunction(randomCam[0], "related").processCams(allCams));
        } else {
            randomCam = this.offlineFunction.processCams(allCams);
        }

        let partners = DataUtil.getPartners(result,event);
        return new CamResult(randomCam, result.countries, 0, 0, undefined, undefined, partners, result.date, result.meta);
    }

}
