export class CountryUI {

    code: string;
    link: string;
    size: number;
    name: string;

  constructor(code: string, link: string, size: number, name: string) {
    this.code = code;
    this.link = link;
    this.size = size;
    this.name = name;
  }
}
