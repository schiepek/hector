import {CamResult} from '../model/cam-result';
import {S3Util} from '../persistence/s3-util';
import {ExpirationData} from '../util/expiration-data';
import {Meta} from '../model/meta';

export class AllLoader {

    private static instance: AllLoader;
    private result: CamResult;
    private expirationData: ExpirationData = new ExpirationData();

    public static getInstance() {
        return this.instance || (this.instance = new this());
    }

    public loadIfNotInMemory(isExpired: boolean): Promise<CamResult> {
        if(!this.result) {
            return this.loadExpiredCams(Meta.S3_NULL)
        }
        if (this.expirationData.isExpired() || isExpired) {
            return this.loadExpiredCams(Meta.S3_EXP);
        }
        return this.getAllCams();
    }

    public loadExpiredCams(meta: Meta): Promise<CamResult> {
        return new Promise<CamResult>((resolve, reject) => {
            let s3Callback = (err: any, data: any) => {
                if(err) {
                    console.error("All Loader: could not load result from s3");
                    reject();
                } else {
                    this.expirationData.setDate();
                    let result;
                    try {
                        result = JSON.parse(data.Body.toString()) as CamResult;
                        if (result && result.cams) {
                            this.result = result;
                            this.result.meta = meta.toString();
                            resolve(this.result);
                        } else {
                            resolve(this.result);
                        }
                    } catch (e) {
                        resolve(this.result);
                    }
                }
            };
            let offlineS3Callback = () => {
                this.expirationData.setDate();
                resolve(this.loadResultDirectly());
            };
            S3Util.getResultFromS3(s3Callback, offlineS3Callback);
        });
    }

    public loadResultDirectly(): CamResult {
        return this.result;
    }

    private getAllCams(): Promise<CamResult> {
        return new Promise<CamResult>((resolve) => {
            this.result.meta = Meta.MEMORY;
            resolve(this.result)
        });
    }

}
