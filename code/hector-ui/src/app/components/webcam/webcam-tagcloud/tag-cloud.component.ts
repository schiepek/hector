import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../../../services/category.service';
import {CategoryCode} from '../../../../../../hector-api/src/model/category-code';
import {ConstantService} from '../../../services/constant.service';

@Component({
  selector: 'app-tag-cloud',
  templateUrl: './tag-cloud.component.html',
  styleUrls: ['./tag-cloud.component.css']
})
export class TagCloudComponent implements OnInit {

  constructor(public categoryS: CategoryService,
              public constant: ConstantService) { }

  ngOnInit() {
  }

  public CategoryCode() {
    return CategoryCode;
  }

}
