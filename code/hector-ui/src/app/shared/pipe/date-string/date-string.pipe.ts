import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';

/**
 * Is used to transform date strings
 */
@Pipe({name: 'datestring'})
export class DateStringPipe extends DatePipe implements PipeTransform {

  private readonly pattern: any = /(\d{2})\.(\d{2})\.(\d{4})/;
  private readonly dateGroups: string = '$3-$2-$1';
  private readonly outputPattern: string = 'MMMM d, y';

  transform(value: any, args?: any): any {
    return super.transform(new Date(value.replace(this.pattern,this.dateGroups)), this.outputPattern);
  }
}
