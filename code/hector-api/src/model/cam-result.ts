import {Cam} from './cam';
import {Country} from './country';
import {Home} from './home';
import {Partner} from './partner';
import {ProjectPartner} from './project-partner';
import {CategoryItem} from './category-item';

export class CamResult {

    cams: Cam[];
    countries: Country[];
    size?: number;
    maxPage?: number;
    error: boolean;
    home?: Home;
    categories?: CategoryItem[];
    partners?: Partner[];
    date?: Date;
    meta?: string;

    // do not use it to give back to UI
    projectPartners: ProjectPartner[];

    constructor(cams: Cam[], countries: Country[], size?: number, maxPage?: number, home?: Home, categories?: CategoryItem[], partners?: Partner[], date?: Date, meta?: string) {
        this.cams = cams;
        this.countries = countries;
        this.size = size;
        this.maxPage = maxPage;
        this.home = home;
        this.categories = categories;
        this.partners = partners;
        this.date = date;
        this.meta = meta;
    }
}
