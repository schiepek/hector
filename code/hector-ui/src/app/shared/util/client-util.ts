import {ElementRef, QueryList, Renderer2} from '@angular/core';

export class ClientUtil {

  public static isClientSide(): boolean {
    return typeof window !== "undefined";
  }

  public static resizeIFrame(element: QueryList<ElementRef>, renderer: Renderer2) {
    if (element && element.first) {
      let document = element.first.nativeElement.contentWindow.document;
      let html = document.getElementsByTagName("html")[0];
      let height = html.scrollHeight;
      renderer.setStyle(element.first.nativeElement, "height", height + "px");
    }
  }

}
