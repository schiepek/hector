import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ActiveFilter} from '../../../shared/model/active-filter';
import {FilterUtil} from '../../../shared/util/filter-util';
import {StateService} from '../../../services/state.service';
import {ScreenService} from '../../../services/screen.service';
import {TranslateService} from '@ngx-translate/core';
import {Category} from '../../../shared/model/category';
import {CategoryService} from '../../../services/category.service';
import {AllFilter} from '../../../shared/model/all-filter';

@Component({
  selector: 'app-webcam-filter',
  templateUrl: './webcam-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./webcam-filter.component.css']
})
export class WebcamFilterComponent implements OnInit{

  @Input()
  public categories: Category[];

  @Input()
  public size: number;

  public activeFilters: ActiveFilter[];
  public filters: AllFilter;

  constructor(private categoryS: CategoryService,
              public translate: TranslateService,
              public screen: ScreenService,
              public state: StateService) {
  }

  ngOnInit(): void {
    this.filters = new AllFilter(this.categoryS, this.categories);
    this.activeFilters = FilterUtil.initFilters(this.state);
  }

}
