import {StateService} from '../../services/state.service';
import {ActiveFilter} from '../model/active-filter';

export class FilterUtil {

  public static initFilters(state: StateService): ActiveFilter[] {
    let af: ActiveFilter[] = [];

    state.query ? af.push(new ActiveFilter("query", null, state.query)) : null;
    state.minAge ? af.push(new ActiveFilter("minage", null, state.minAge)) : null;
    state.maxAge ? af.push(new ActiveFilter("maxage", null, state.maxAge)) : null;
    state.gender ? af.push(new ActiveFilter("gender", "gender-filter." + state.gender, null)) : null;
    state.isHd ? af.push(new ActiveFilter("ishd", null, null)) : null;
    state.isNew ? af.push(new ActiveFilter("isnew", null, null)) : null;
    state.country ? af.push(new ActiveFilter("country", null, state.country)) : null;
    state.language ? af.push(new ActiveFilter("language", null, state.language)) : null;
    state.haircolor ? af.push(new ActiveFilter("haircolor", "hair-filter." + state.haircolor, null)) : null;
    state.body ? af.push(new ActiveFilter("body", "body-filter." + state.body, null)) : null;
    state.category ? af.push(new ActiveFilter("category", "categories." + state.category, null)) : null;
    state.tag ? af.push(new ActiveFilter("tag", null, state.tag)) : null;
    state.provider ? af.push(new ActiveFilter("provider", "provider." + state.provider, null)) : null;

    return af;
  }

}
