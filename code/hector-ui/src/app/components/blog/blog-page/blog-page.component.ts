import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {BlogEntry} from '../../../shared/model/blog-entry';
import {AllPagination} from '../../../shared/model/pagination';
import {ComponentReloader} from '../../../shared/util/component-reloader';
import {BlogEntryService} from '../../../services/blog-entry.service';
import {PaginationUtil} from '../../../shared/util/pagination-util';
import {ActivatedRoute} from '@angular/router';
import {MetaService} from '../../../services/meta.service';

@Component({
  selector: 'app-blog-page',
  templateUrl: './blog-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./blog-page.component.css']
})
export class BlogPageComponent implements OnInit {

  @Input()
  public page: number;

  public blogEntries: BlogEntry[];
  public allPagination: AllPagination;

  constructor(private meta: MetaService,
              private blogEntry: BlogEntryService,
              private cdRef: ChangeDetectorRef,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.blogEntry.getBlogEntries(this.page).then(blogEntryPage => {
      this.blogEntries = blogEntryPage.entries;
      this.setPagination(blogEntryPage.maxPage);
      this.meta.setBlogMetaTag(this.page);
      ComponentReloader.detectChanges(this.cdRef);
    })
  }

  private setPagination(maxPage: number) {
    this.route.parent.url.subscribe(segment => {
      this.allPagination = PaginationUtil.getPaginations(this.page, maxPage, segment)
    });
  }

}
