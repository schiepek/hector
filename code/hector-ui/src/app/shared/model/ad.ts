export class Ad {

  type: AdType;
  src: string;
  translationCode?: string;

  constructor(type: AdType, src: string, translationCode?: string) {
    this.type = type;
    this.src = src;
    this.translationCode = translationCode;
  }

}

export enum AdType {
  HTML, LINK
}
