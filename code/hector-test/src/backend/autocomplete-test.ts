import 'mocha';
import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {AutocompleteFunction} from '../../../hector-api/src/function/autocomplete-function';
import {Cam} from '../../../hector-api/src/model/cam';
import {TestType} from '../general/test-type';

export class AutocompleteTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {

        this.tests.set("autocomplete-blond", (executor, config) => {
            executor.executeRestTestFunction(config.getAutoCompleteFunction(), 'should return autocomplete results for blond', {query: "blonde"},
                res => {
                    assert.isTrue(res["cams"].length > 0, "there should be autocomplete results for blond");
                    assert.isTrue(res["size"] > 0, "The size should be set and the results should be bigger then 0");
                    assert.isTrue(res["size"] <= AutocompleteFunction.RESULTS_MAX_SIZE, "The results size should be within it's limit");

                    let autoCompleteResult = res["cams"] as Cam[];

                    autoCompleteResult.forEach(cam => {
                        assert.isTrue(!!cam.username);
                        assert.isTrue(!!cam.provider);
                    });
                }
            );
        });

        this.tests.set("autocomplete-empty", (executor, config) => {
            executor.executeRestTestFunction(config.getAutoCompleteFunction(), 'should return empty autocomplete results', {query: "this_username_does_for_sure_not_exist_XYZ"},
                res => {
                    assert.isTrue(res["cams"].length == 0, "there should be no autocomplete results for that query");
                    assert.isTrue(res["size"] == 0, "The size should be set and the results should be b0");
                }
            );
        });

    }

}
