import {Component, Input, OnInit} from '@angular/core';
import {Cam} from '../../../../../../hector-api/src/model/cam';
import {ScreenService} from '../../../services/screen.service';
import {AdService} from '../../../services/ad.service';

@Component({
  selector: 'app-detail-related',
  templateUrl: './detail-related.component.html',
  styleUrls: ['./detail-related.component.css']
})
export class DetailRelatedComponent implements OnInit {

  @Input()
  public relatedCams: Cam[];

  @Input()
  public showTitle: boolean;

  constructor(public screen: ScreenService,
              public ad: AdService) {
  }

  ngOnInit() {
  }

}
