import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-roulette-thumb',
  templateUrl: './roulette-thumb.component.html',
  styleUrls: ['./roulette-thumb.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouletteThumbComponent {

}
