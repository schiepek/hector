import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnInit, Inject} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {WebcamService} from '../../services/webcam.service';
import {Home} from '../../../../../hector-api/src/model/home';
import {CategoryCode} from '../../../../../hector-api/src/model/category-code';
import {CountryService} from '../../services/country.service';
import {CountryUI} from "../../shared/model/country-ui";
import {Cam} from '../../../../../hector-api/src/model/cam';
import {ScreenService} from '../../services/screen.service';
import {MetaService} from '../../services/meta.service';
import {LinkService} from '../../services/link.service';
import {environment} from '../../../environments/environment';
import Timer = NodeJS.Timer;
import {ComponentReloader} from '../../shared/util/component-reloader';
import {AdService} from '../../services/ad.service';
import {ClientUtil} from '../../shared/util/client-util';
import {PartnerService} from '../../services/partner.service';
import {FooterService} from '../../services/footer.service';
import {StateService} from '../../services/state.service';
import {Location} from '@angular/common';
import {ConstantService} from '../../services/constant.service';
import {CamUtil} from '../../shared/util/cam-util';
import {CategoryService} from '../../services/category.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public home: Home;
  public randomCountries: CountryUI[];
  public topCams: Cam[];
  public size: number;
  public countrySize: number;

  private readonly topCamsSizeMobile: number = 2;

  private timer: Timer;

  public categoryCode = CategoryCode;

  constructor(public countryS: CountryService,
              public categoryS: CategoryService,
              public screen: ScreenService,
              public ad: AdService,
              public partner: PartnerService,
              public constant: ConstantService,
              private router: Router,
              private state: StateService,
              private route: ActivatedRoute,
              private location: Location,
              private cdRef: ChangeDetectorRef,
              private link: LinkService,
              private meta: MetaService,
              private footer: FooterService,
              private webcam: WebcamService) {
    this.meta.setHomeMetaTags();
    this.footer.setGenericFooterText("home");
  }

  @HostListener('window:focus', ['$event'])
  onFocus(event: any): void {
    ClientUtil.isClientSide() && this.initInvervalLoader();
  }

  @HostListener('window:blur', ['$event'])
  onBlur(event: any): void {
    ClientUtil.isClientSide() && this.clearIntervalLoader();
  }

  ngOnInit() {
    this.link.setCanonicalUrl();
    this.initWebCamsTemporarily();
    this.initWebCams();
    this.initInvervalLoader();
    this.state.getTracker(this.route, this.location);
  }

  private initWebCamsTemporarily(): void {
    let cam = CamUtil.createFakeCam();
    this.home = new Home([cam, cam, cam], [cam, cam, cam, cam], [cam, cam], [], []);
    this.topCams = this.home.topCams;
  }

  private initWebCams(): void {
    this.webcam.getHome().then(res => {
      if (res.error) {
        // do nothing
      } else {
        this.home = res.home;
        this.size = res.size;
        this.countrySize = res.countries.length;
        this.initTopCams(res.home.topCams);
        this.initRandomCountries(res.home);
      }
    });
  }

  private initTopCams(cams: Cam[]): void {
    this.topCams = this.screen.isHiddenLgUp() ? cams.slice(0, this.topCamsSizeMobile) : cams;
  }

  private initRandomCountries(home: Home): void {
    this.randomCountries = home.randomCountries.map(rc => {
      return new CountryUI(rc.code, this.countryS.getUrl(rc.code), rc.size, this.countryS.getName(rc.code));
    });
    ComponentReloader.detectChanges(this.cdRef);
  }

  private initInvervalLoader() {
    ClientUtil.isClientSide() ? this.timer = setInterval(() => this.initWebCams(), environment.refreshInterval * 1000) : null;
  }

  private clearIntervalLoader() {
    ClientUtil.isClientSide() && clearInterval(this.timer);
  }


  public CategoryCode() {
    return CategoryCode;
  }

}
