import {Injectable} from '@angular/core';
import {StateService} from './state.service';
import {CategoryService} from './category.service';
import {AutoComplete} from '../shared/model/auto-complete';
import {CountryService} from './country.service';
import {TranslateService} from '@ngx-translate/core';
import {AutoCompleteType} from '../shared/model/auto-complete-type';
import {CategoryCode} from '../../../../hector-api/src/model/category-code';
import {StringUtil} from '../../../../hector-api/src/util/string-util';
import {ConstantService} from './constant.service';
import {WebcamService} from './webcam.service';
import {CamResult} from '../../../../hector-api/src/model/cam-result';
import {forkJoin, Observable} from 'rxjs';
import {ScreenService} from './screen.service';
import {TagService} from './tag.service';

@Injectable({
  providedIn: 'root'
})
export class AutoSearchService {

  private static readonly RESULT_MAX_SIZE: number = 10;

  constructor(private state: StateService,
              private webcam: WebcamService,
              private screen: ScreenService,
              private constant: ConstantService,
              private translate: TranslateService,
              private country: CountryService,
              private tag: TagService,
              private category: CategoryService) {
  }

  public search(): Observable<[CamResult, AutoComplete[]]> {
    return forkJoin([
      this.webcam.getAutoComplete(),
      this.searchLocallyAsync()
    ]);
  }

  public dataToAutoComplete(data: [CamResult, AutoComplete[]]): AutoComplete[] {
    let results: AutoComplete[] = [];
    results.push(...this.convertCamResultToAutoComplete(data[0]));
    results.push(...data[1]);
    return this.sortAndSlice(results);
  }

  private searchLocallyAsync(): Promise<AutoComplete[]> {
    return new Promise((resolve => resolve(this.searchLocally())));
  }

  private searchLocally(): AutoComplete[] {
    let results = this.searchCategories();
    this.searchTags(results);
    this.searchWorldMap(results);
    this.searchCountries(results);
    return results;
  }

  private searchCategories(): AutoComplete[] {
    return this.category.allCategories.reduce((filtered, category) => {
      let translated = this.translate
        .instant("categories." + CategoryCode[category.code]).toLowerCase();
      if (translated.includes(this.state.searchTerm.toLowerCase())) {
        filtered.push(new AutoComplete(
          AutoCompleteType[category.group],
          StringUtil.toFirstLetterUppercase(translated),
          category.path));
      }
      return filtered;
    }, []);
  }

  private searchTags(results: AutoComplete[]): void {
    let tagResults = this.tag.tagsThatInclude(this.state.searchTerm.toLowerCase())
      .map(t=> new AutoComplete(
        AutoCompleteType.tag,
        StringUtil.toFirstLetterUppercase(t),
        t
      ));
    results.push(...tagResults);
  }

  private searchWorldMap(results: AutoComplete[]): void {
    let translated = this.translate.instant("header." + this.constant.WORLD_MAP);
    if (translated.toLowerCase().includes(this.state.searchTerm.toLowerCase())) {
      results.push(new AutoComplete(
        AutoCompleteType.worldmap,
        StringUtil.toFirstLetterUppercase(translated),
        this.constant.WORLD_MAP));
    }
  }

  private searchCountries(results: AutoComplete[]): void {
    this.country.initAllCountriesAC();
    let filteredCountries = this.country.allCountries.reduce((filtered, country) => {
      if (country.name.toLowerCase().includes(this.state.searchTerm.toLowerCase())) {
        filtered.push(new AutoComplete(
          AutoCompleteType.country,
          country.name,
          country.link));
      }
      return filtered;
    }, []);
    results.push(...filteredCountries);
  }

  private sortAndSlice(results: AutoComplete[]): AutoComplete[] {
    return results.sort((a, b) => {
      let aStartsWith = a.name.toLowerCase().startsWith(this.state.searchTerm.toLowerCase());
      let bStartsWith = b.name.toLowerCase().startsWith(this.state.searchTerm.toLowerCase());

      if (aStartsWith && !bStartsWith) {
        return -1;
      } else if (!aStartsWith && bStartsWith) {
        return 1;
      } else {
        return AutoSearchService.sortPreferredByCategory(a, b);
      }
    }).slice(0, AutoSearchService.RESULT_MAX_SIZE)
  }

  private static sortPreferredByCategory(a: AutoComplete, b: AutoComplete): number {
    let aIsUser = AutoCompleteType.isUser(a.type);
    let bIsUser = AutoCompleteType.isUser(b.type);

    if (aIsUser && !bIsUser) {
      return 1;
    } else if (!aIsUser && bIsUser) {
      return -1
    }
    return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
  }

  private convertCamResultToAutoComplete(result: CamResult): AutoComplete[] {
    if (result && result.cams && result.cams.length > 0) {
      return result.cams.reduce((reduced, c) => {
        let path = "/" + this.constant.SEARCH + "/" + c.username;
        let autoCompleteType = AutoCompleteType.forProvider(c.provider);
        reduced.push(new AutoComplete(
          autoCompleteType,
          c.username,
          path));
        return reduced;
      }, []);
    }
    return [];
  }

}
