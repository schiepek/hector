import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {HomeUtil} from '../../../hector-api/src/util/home-util';
import {TestUtil} from '../util/test-util';
import {ExpiredTest} from './expired-test';
import {TestType} from '../general/test-type';


export class HomeTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {

        this.tests.set("home", (executor, config) => {
            executor.executeRestTestFunction(config.getHomeFunction(), 'should return the home object with different top-cams', {page: 1},
                res => {
                    assert.isTrue(res["cams"].length === 0, "there should be no cams with the home function");
                    assert.isTrue(res["size"] > 1000, "The total cams size should be more than 1000");
                    assert.isTrue(res["maxPage"] === 0, "Max Page should be 0. There is no paging on the home page");

                    // top cams
                    assert.isTrue(res["home"]["topCams"].length === HomeUtil.topCamSize, "there should be exact " + HomeUtil.topCamSize + " top cams");

                    //TODO not sorted by users any more cause Bongacams is boosted (is flaky)
                    //assert.isTrue(TestUtil.isSortedByNumberOfUsers(res["home"]["topCams"]), "the top cams should be sorted by users");

                    //top cams by country
                    assert.isTrue(res["home"]["topCamsCountry"].length === HomeUtil.topCamsByCountrySize, "there should be exact " + HomeUtil.topCamsByCountrySize + " top cams by country");

                    //TODO not sorted by users any more cause Bongacams is boosted (is flaky)
                    //assert.isTrue(TestUtil.isSortedByNumberOfUsers(res["home"]["topCamsCountry"]), "the top cams by country should be sorted by users");
                    assert.isTrue(TestUtil.eachCountryIsDifferent(res["home"]["topCamsCountry"]), "each country should be different in the top cams country list");

                    //top asian cams
                    assert.isTrue(res["home"]["topAsianCams"].length === HomeUtil.categoryListSize, "there should be exact " + HomeUtil.categoryListSize + " top asian cams");

                    //TODO not sorted by users any more cause Bongacams is boosted (is flaky)
                    //assert.isTrue(TestUtil.isSortedByNumberOfUsers(res["home"]["topAsianCams"]), "the top asian cams by country should be sorted by users");
                    res["home"]["topAsianCams"].forEach(c => assert.isTrue(!!c.categories.find(cat => cat === "asian")));

                    //top latin cams
                    assert.isTrue(res["home"]["topLatinCams"].length === HomeUtil.categoryListSize, "there should be exact " + HomeUtil.categoryListSize + " top latin cams");
                    //TODO not sorted by users any more cause Bongacams is boosted (is flaky)
                    //assert.isTrue(TestUtil.isSortedByNumberOfUsers(res["home"]["topLatinCams"]), "the top latin cams by country should be sorted by users");
                    res["home"]["topLatinCams"].forEach(c => assert.isTrue(!!c.categories.find(cat => cat === "latina")));

                    //random countries
                    assert.isTrue(res["home"]["randomCountries"].length === HomeUtil.randomCountrySize, "there should be exact " + HomeUtil.randomCountrySize + " random countries");
                    res["home"]["randomCountries"].forEach(c => assert.isTrue(c.size >= 1, "size of random country cams should be bigger than zero"));

                    //country list for menu
                    assert.isTrue(res["countries"].some(co => co.code === "co"), "colombia should be in the countries list");
                    assert.isTrue(res["countries"].some(co => co.code === "ru"), "russia should be in the countries list");

                    assert.isTrue(ExpiredTest.dateIsNotExpired(res, config), "Home Date shouldn't be expired")
            }
            );
        });

    }
}
