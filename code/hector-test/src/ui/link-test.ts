import {AbstractTest} from '../backend/abstract-test';
import {assert} from 'chai';
import {TestType} from '../general/test-type';


export class LinkTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.UI;
    }

    protected initTests(): void {

        this.tests.set("link", (executor) => {

            let urls: string[] = [];

            urls.push(
                "/recommends/chaturbate-signup",
                "/recommends/bongacams-signup",
                "/recommends/livejasmin-signup",
                "/recommends/camsoda-signup",
                "/recommends/stripchat-signup",
                "/recommends/chaturbate-model-signup",
                "/recommends/bongacams-model-signup",
                "/recommends/livejasmin-model-signup",
                "/recommends/camsoda-model-signup",
                "/recommends/stripchat-model-signup",
            );

            urls.forEach(url => {
                executor.executeRestTest(executor.config.getUiUrlProd() +  url, 'affiliate link should return valid content: ' + url, res => {
                    assert.equal(res.status, 200, url + ' does not return status 200');
                    assert.isFalse(res.server === "CloudFront", url + ' Server cannot be CloudFront');
                    assert.isFalse(res.server === "AmazonS3", url + ' Server cannot be AmazonS3');
                    assert.isFalse(res.text.includes('images_hector'), 'the returned site is lemoncams and not the affiliate partner');
                })
            });
        });


    }
}
