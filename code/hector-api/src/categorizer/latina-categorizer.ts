import {CategorizerFunction} from './categorizer-function';
import {Cam} from '../model/cam';
import {CamUtil} from '../util/cam-util';
import {AsianCategorizer} from './asian-categorizer';
import {CategoryCode} from '../model/category-code';

export class LatinaCategorizer extends CategorizerFunction {

    protected readonly possibleCountries: String[] = ["ar", "bo", "br", "co", "cr", "cu", "dm", "do", "ec", "sv", "gf", "gt", "hn", "mx", "ni", "py", "pe", "uy", "cl"];

    constructor() {
        super(CategoryCode.latina,
            (cam) => {
                return this.isLatinCountry(cam) || (
                    CamUtil.containsBasicInfoButNot(cam, ["german", "french", "asian"], "latin") && this.isNotAsianCountry(cam));
            });
    }

    public isLatinCountry(cam: Cam): boolean {
        return this.possibleCountries.filter(c => cam.country === c).length > 0;
    }

    public isNotAsianCountry(cam: Cam): boolean {
        return !new AsianCategorizer().isAsianCountry(cam);
    }

}