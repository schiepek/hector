import {Inject, Injectable, Optional, PLATFORM_ID} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Router} from '@angular/router';
import {WINDOW} from '@ng-toolkit/universal';
import {REQUEST} from '@nguniversal/express-engine/tokens';
import {ConstantService} from './constant.service';
import {environment} from '../../environments/environment';

@Injectable()
export class LinkService {

  private canonical: HTMLLinkElement;
  private icon: HTMLLinkElement;

  constructor(@Inject(DOCUMENT) private doc,
              @Inject(WINDOW) private win,
              @Optional() @Inject(REQUEST) private request: any,
              @Inject(PLATFORM_ID) private platformId: Object,
              private constant: ConstantService,
              private router: Router) {
  }

  public setCanonicalUrl(link?: string) {
    if (this.canonical === undefined) {
      this.canonical = this.doc.createElement('link');
      this.canonical.setAttribute('rel', 'canonical');
      this.doc.head.appendChild(this.canonical);
    }
    link ? this.canonical.setAttribute('href', link) : this.canonical.setAttribute('href', this.constant.getCanonicalOrigin() + this.router.url);
  }

  public setCanonicalUrlFirstParam() {
    let paths = this.router.url.split('/');
    paths.length >= 2 ? this.setCanonicalUrl(this.constant.getCanonicalOrigin() + '/' + paths[1]) : this.setCanonicalUrl();
  }

  public setStyleLink(url) {
    // Create link
    let link = document.createElement('link');
    link.href = url;
    link.rel = 'stylesheet';
    link.type = 'text/css';

    let head = document.getElementsByTagName('head')[0];
    let links = head.getElementsByTagName('link');
    let style = head.getElementsByTagName('style')[0];

    // Check if the same style sheet has been loaded already.
    let isLoaded = false;
    for (var i = 0; i < links.length; i++) {
      var node = links[i];
      if (node.href.indexOf(link.href) > -1) {
        isLoaded = true;
      }
    }
    if (isLoaded) return;
    head.insertBefore(link, style);
  }
}
