import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit,
  ViewChild
} from '@angular/core';
import {WebcamService} from '../../services/webcam.service';
import {Cam} from '../../../../../hector-api/src/model/cam';
import {StateService} from '../../services/state.service';
import {PaginationUtil} from '../../shared/util/pagination-util';
import {AllPagination} from '../../shared/model/pagination';
import {ActivatedRoute, Router, UrlSegment} from '@angular/router';
import {ScreenService} from '../../services/screen.service';
import Timer = NodeJS.Timer;
import {environment} from '../../../environments/environment';
import {MetaService} from '../../services/meta.service';
import {CategoryService} from '../../services/category.service';
import {Category} from '../../shared/model/category';
import {CountryService} from '../../services/country.service';
import {RouterUtil} from '../../shared/util/router-util';
import {LinkService} from '../../services/link.service';
import {CamResult} from '../../../../../hector-api/src/model/cam-result';
import {ComponentReloader} from '../../shared/util/component-reloader';
import {AdService} from '../../services/ad.service';
import {ClientUtil} from '../../shared/util/client-util';
import {FooterService} from '../../services/footer.service';
import {Location} from '@angular/common';
import {CamUtil} from '../../shared/util/cam-util';

@Component({
  selector: 'app-webcam',
  templateUrl: './webcam.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./webcam.component.css']
})
export class WebcamComponent implements OnInit, OnDestroy {

  @ViewChild('filterComponent', {static: false})
  filterComponent;


  public cams: Cam[];
  public size: number;
  public allPagination: AllPagination;
  public noResults: boolean = false;
  public categories: Category[] = [];

  public isCams: boolean;
  public isServerSide: boolean;

  private timer: Timer;

  constructor(private router: Router,
              public meta: MetaService,
              public ad: AdService,
              private countryS: CountryService,
              private link: LinkService,
              private location: Location,
              private footer: FooterService,
              private cdRef: ChangeDetectorRef,
              public categoryS: CategoryService,
              public webcam: WebcamService,
              public state: StateService,
              public screen: ScreenService,
              public route: ActivatedRoute) {

  }

  @HostListener('window:focus', ['$event'])
  onFocus(event: any): void {
    ClientUtil.isClientSide() && this.initInvervalLoader();
  }

  @HostListener('window:blur', ['$event'])
  onBlur(event: any): void {
    ClientUtil.isClientSide() && this.clearIntervalLoader();
  }

  ngOnInit() {
    this.link.setCanonicalUrlFirstParam();
    this.initCamsTemporarely();
    this.initParamsAndWebcams();
    this.initIsCams();
    this.isServerSide = !ClientUtil.isClientSide();
    this.initInvervalLoader();
    this.state.getTracker(this.route, this.location);
  }

  ngOnDestroy(): void {
    this.timer ? clearInterval(this.timer) : null;
  }

  private initCamsTemporarely() {
    let cam = CamUtil.createFakeCam();
    this.cams = [cam, cam, cam, cam, cam, cam, cam, cam];
  }

  private initIsCams(): void {
    this.route.parent.url.subscribe(smts => this.isCams = environment.isSubSite ? !!smts[0] : smts[0].path === "cams");
  }

  private initParamsAndWebcams(): void {
    this.state.reset();
    this.initParamsWithCategories();
  }

  private initParamsWithCategories() {
    this.route.paramMap.subscribe(params => {
      params.keys.forEach(k => {
        let category = this.categoryS.findCategory(params.get(k), k);
        if (category) {
          category.func(this.state);
          this.categories.push(category);
        }
      });
      this.checkForInvalidParams();
      this.meta.setCamsMetaTags(this.categories, this.state.page);
      this.footer.setCamsFooterText(this.categories);
      this.initWebCams();
    });
  }

  private checkForInvalidParams() {
    this.route.parent.url.subscribe((segments: UrlSegment[]) => {
      let hasNoCategories = this.categories.length === 0;
      if (WebcamComponent.isNotValidPath(segments) && hasNoCategories) {
          RouterUtil.goTo404(this.router);
      }
    })
  }

  private static isNotValidPath(segments: UrlSegment[]): boolean {
    if (environment.isSubSite) {
      return segments[0] ? segments[0].path !== "search" : false;
    } else {
      return segments[0] ? segments[0].path !== "cams" && segments[0].path !== "search" : true;
    }
  }

  private initWebCams(): void {
    this.webcam.getCams().then(res => {
      if (res.error) {
        // do nothing
      }
      if (res.cams.length > 0) {
        this.setLoadedCams(res);
      } else {
        this.noResults = res.cams.length < 1;
        this.initDefaultWebCams()
      }
    });
  }

  private setLoadedCams(res: CamResult): void {
    this.cams = res.cams;
    this.size = res.size;
    this.setPagination(res.maxPage);
    ComponentReloader.detectChanges(this.cdRef);
  }

  private setPagination(maxPage: number) {
    this.route.parent.url.subscribe(segment => {
      this.allPagination = PaginationUtil.getPaginations(this.state.page, maxPage, segment)
    });
  }

  private initDefaultWebCams(): void {
    let oldState = {...this.state};
    this.state.reset();
    this.initWebCams();
    this.state.setOldState(oldState);
  }

  private initInvervalLoader() {
    ClientUtil.isClientSide() && !environment.devMode ? this.timer = setInterval(() => this.initWebCams(), environment.refreshInterval * 1000) : null;
  }

  private clearIntervalLoader() {
    ClientUtil.isClientSide() && !environment.devMode && clearInterval(this.timer);
  }

}
