import {Cam} from '../model/cam';
import {Loader} from './loader';
import {PropertyEvaluator} from '../util/property-evaluator';
import {HectorConfig} from '../util/hector-config';
import {Categorizer} from '../categorizer/categorizer';
import {provider} from '../model/provider';
import {TagEvaluator} from '../util/tag-evaluator';

export class StripchatLoader extends Loader {

    private static readonly defaultAge: number = 30;
    private static readonly maxValidAge: number = 95;
    private readonly userId: string = "6fd475387efaf4a8de1a4ab6e5bd66a097b936b7b66b3250827a950917ccd9ca";
    private readonly chatRoomUrl: string = "https://go.alxbgo.com/?userId=" + this.userId + "&path=/cams/{username}"
    private readonly embeddedUrl: string = "https://lite-iframe.stripcdn.com/{username}?userId=" + this.userId;

    private readonly tagMap: Map<string, string> = new Map([
        [ "ethnicityWhite", "white" ],
        [ "ethnicityAsian", "asian" ],
        [ "ethnicityIndian", "indian" ],
        [ "ethnicityLatino", "latina" ],
        [ "ethnicityEbony", "ebony" ],
        [ "ethnicityMiddleEastern", "arab" ],
        [ "ethnicityMixed", "mixed" ],
        [ "specificsBigAss", "bigass" ],
        [ "specificsBigTits", "bigtits" ],
        [ "specificShaven", "shaved" ],
        [ "specificsHairy", "hairy" ],
        [ "specificSmallTits", "smalltits" ],
        [ "specificsBigCock", "bigcock"],
        [ "specificsTattoos", "tattoo"],
        [ "specificsPiercing", "piercing"],
        [ "specificTrimmed", "trimmed"],
        [ "specificFtM", "ftm"],
        [ "specificHairyMan", "hairy"],
        [ "specificTG", "transgender"],
        [ "specificShemale", "shemale"],
        [ "specificTS", "trans"],
    ]);

    constructor(categorizer: Categorizer) {
        super(categorizer, provider.STRIPCHAT);
    }

    public getApiUrls(): string[] {
        return HectorConfig.getStripchatUrls();
    }

    protected getJsonDataObject(res: any): any {
        return JSON.parse(res)["models"];
    }

    protected readProperties(camObj: any): Cam {
        let username = camObj["username"]; //
        let age = StripchatLoader.calculateAgeOutOfBirthday(camObj["profileBirthDate"]) //
        let isPrivate = camObj["status"] != "public"; //
        let isNew = camObj["isNew"]; //
        let numberOfUsers = camObj["viewersCount"]; //
        let gender = PropertyEvaluator.getGenderByString(camObj["gender"]); //
        let isHd = camObj["broadcastHD"]; //
        let imageUrl = camObj["widgetPreviewUrl"]; //
        let chatRoomUrl = this.chatRoomUrl.replace("{username}", username); //
        let embedUrl = this.embeddedUrl.replace("{username}", username); //
        let title = camObj["goalMessage"]; //
        let tags = TagEvaluator.executeForCam(this.calculateTags(camObj["profileEthnicity"], camObj["profileSpecifics"]), title);
        let turnsOn = "";
        let turnsOff = "";
        let location = "";
        let languages = "";
        let languageKeys = camObj["languages"];
        let country = camObj["modelsCountry"];
        let hairColor = PropertyEvaluator.getHairColorByString(camObj["profileHairColor"]);
        let body = PropertyEvaluator.getBodyByString(camObj["profileBodyType"]);

        return Cam.of(username, age, isPrivate, isNew, numberOfUsers, gender, isHd, imageUrl, chatRoomUrl, embedUrl, null, title, tags, turnsOn, turnsOff, hairColor, body, location, country, languages, languageKeys, this.getProvider());
    }

    private static calculateAgeOutOfBirthday(birthdateStr: string) : number {
        if (birthdateStr) {
            const today = new Date();
            const birthDate = new Date(birthdateStr);
            let age = today.getFullYear() - birthDate.getFullYear();
            const m = today.getMonth() - birthDate.getMonth();

            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age > StripchatLoader.maxValidAge ? StripchatLoader.defaultAge : age;
        }
        return StripchatLoader.defaultAge;
    }

    private calculateTags(ethnicity: string, specifics: string[]): string[] {
        let tags: string[] = [];

        let ethnicityTag = this.getValidTagValue(ethnicity);
        ethnicityTag ? tags.push(ethnicityTag) : null;

        if(specifics) {
            for(let specific of specifics) {
                let specificTag = this.getValidTagValue(specific);
                specificTag ? tags.push(specificTag) : null;
            }
        }

        return tags;
    }

    private getValidTagValue(origin: string) : string {
        if(origin) {
            let tag = this.tagMap.get(origin);
            return tag ? tag : origin;
        }
        return null;
    }

}
