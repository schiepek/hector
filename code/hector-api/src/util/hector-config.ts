export class HectorConfig {

    private static readonly pageSize = 36;

    private static readonly awEmpirePrefix: string = "http://pto.ptawe.com/xml/feed/index.php?siteId=jasmin&psId=swissTrader2000&psTool=213_1&psProgram=revs&campaignId=&category=";
    private static readonly awEmpireSuffix: string = "&limit=1000&imageSizes=320x180&imageType=erotic&showOffline=0&extendedDetails=1&responseFormat=json&performerId=&subAffId={SUBAFFID}";

    private static readonly bongacamsUrl: string = "https://ssl-tools.bongacams.com/promo.php?c=588492&lang=en&type=api&api_v=1&api_type=json&categories[]=female&categories[]=male&categories[]=transsexual";

    private static readonly chaturbateUrl: string = "https://chaturbate.com/affiliates/api/onlinerooms/?format=json&wm=xAq5D";

    private static readonly camSodaUrl: string = "https://feed.camsoda.com/api/v1/browse/online_embed?id=swissTrader2000&type=revs";

    private static readonly stripchatUrl: string = "https://go.stripchat.com/api/models?limit=0";

    public static getPageSize(): number {
        return this.pageSize;
    }

    public static getAwEmpireUrls(): string[] {
        return [
            this.awEmpirePrefix + "girl" + this.awEmpireSuffix,
            this.awEmpirePrefix + "gay" + this.awEmpireSuffix,
            this.awEmpirePrefix + "couple" + this.awEmpireSuffix,
            this.awEmpirePrefix + "transgender" + this.awEmpireSuffix
        ];
    }

    public static getBongacamsUrls(): string[] {
        return [this.bongacamsUrl]
    }

    public static getChaturbateUrls(): string[] {
        return [this.chaturbateUrl]
    }

    public static getCamSodaUrls(): string[] {
        return [this.camSodaUrl];
    }

    public static getStripchatUrls(): string[] {
        return [this.stripchatUrl];
    }
}
