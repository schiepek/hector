import {CamFunction} from './cam-function';
import {Cam} from '../model/cam';
import {CamUtil} from '../util/cam-util';

export class RelatedFunction extends CamFunction {

    // actual grid size is 9, but we add one advertisement
    private readonly maxRelatedCams: number = 9;
    private readonly ageOffset: number = 3;

    constructor(dc: Cam, queryString: any) {
        super(queryString,
            (unfilteredCams) => {

                let cams = unfilteredCams.filter(c => c.username !== dc.username);

                let cams1 = cams.filter(c => CamUtil.isSameCountry(dc, c) && CamUtil.hasAgeOffset(dc, c, this.ageOffset));
                if (this.isEnough(cams1)) {
                    return this.sliceCams(cams1);
                }

                let cams2 = cams.filter(c => CamUtil.isSameCountry(dc, c));
                if (this.isEnough(cams2)) {
                    return this.sliceCams(cams2);
                }

                let cams3 = cams.filter(c => CamUtil.hasAgeOffset(dc, c, this.ageOffset));
                if (this.isEnough(cams3)) {
                    return this.sliceCams(cams3);
                }

                return this.sliceCams(cams);

            });
    }

    private isEnough(cams: Cam[]): boolean {
        return cams.length >= this.maxRelatedCams
    }

    private sliceCams(cams: Cam[]): Cam[] {
        return cams.sort((a, b) => b.numberOfUsers - a.numberOfUsers).slice(0, this.maxRelatedCams);
    }


}