set S3BUCKET=%1

echo 'upload dist/browser folder to %S3BUCKET%'
call aws s3 rm %S3BUCKET% --profile hector --recursive
call aws s3 cp ../../dist/browser %S3BUCKET% --exclude "assets/content/*/*.html" --exclude "index.html" --profile hector --recursive --acl public-read --cache-control max-age=2592000,public
call aws s3 cp ../../dist/browser %S3BUCKET% --exclude "*" --include "assets/content/*/*.html" --profile hector --recursive --acl public-read --cache-control max-age=604800,public
call aws s3 cp ../../dist/browser %S3BUCKET% --exclude "*" --include "index.html" --profile hector --recursive --acl public-read --cache-control no-cache,public

echo 'upload done'

pause
