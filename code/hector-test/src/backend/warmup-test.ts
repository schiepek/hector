import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {TestType} from '../general/test-type';
import {provider} from '../../../hector-api/src/model/provider';

export class WarmupTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.WARMUP;
    }

    protected initTests(): void {

        provider.ALL.forEach(p => {
            this.tests.set("warmup-provider-" + p, (executor, config) => {

                executor.executeRestTestFunction(config.getProviderWarmupFunction(p), 'should just warmup the cams of ' + p, {},
                    (r) => {
                        assert.isTrue(r["result"].indexOf("successfully") > -1, "The warmup response should contain with a 'successfully' string");
                        assert.equal(r["function"], config.getProviderWarmupFunction(p));
                        let numberOfCams = r["result"].replace(/[^0-9]/g, '') as number;
                        assert.isTrue(numberOfCams > 0, "There should be more than 0 cams of " + p);
                    }
                );
            })
        });

        this.tests.set("warmup-all", (executor, config) => {
            executor.executeRestTestFunction(config.getWarmupAllFunction(), 'should just warmup (load) the cams', {},
                (r) => {
                    assert.isTrue(r["result"].indexOf("successfully") > -1, "The warmup response should contain with a 'successfully' string");
                    assert.equal(r["function"], config.getWarmupAllFunction());
                    let numberOfCams = r["result"].replace(/[^0-9]/g, '') as number;
                    assert.isTrue(numberOfCams > 0, "There should be more than 0 cams in result");
                }
            );
        });

    }

}
