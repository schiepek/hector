import {CamResult} from '../model/cam-result';
import {DataUtil} from '../util/data-util';
import {CategoryItem} from '../model/category-item';

export class CategoriesFunction {

    private readonly specificFilter?: string;

    constructor(queryStrings: any) {
        this.specificFilter = queryStrings["filter"];
    }

    /** the first cam is the detail cam > the rest are the related cams **/
    public updateResult(result: CamResult, event: any): CamResult {
        let categories;
        if (this.specificFilter) {
            categories = this.reduceCategoriesForExistingFilters(result)
        } else {
            categories = result.categories.map(cat => new CategoryItem(cat.code, cat.type, cat.size))
        }

        return new CamResult([], result.countries, result.cams.length,
            0, undefined, categories,
            DataUtil.getPartners(result, event), result.date,
            result.meta);
    }

    private reduceCategoriesForExistingFilters(result: CamResult) {
        return result.categories.reduce((filtered, category) => {
            let specificSize = category.specificSizes.find(sS => sS.filter === this.specificFilter);
            if (specificSize) {
                filtered.push(new CategoryItem(category.code, category.type, specificSize.size))
            }
            return filtered;
        }, []);
    }
}
