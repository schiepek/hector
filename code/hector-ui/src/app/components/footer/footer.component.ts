import {Component, OnInit} from '@angular/core';
import {PartnerService} from '../../services/partner.service';
import {ConstantService} from '../../services/constant.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit{

  public year: number;
  public showImage: boolean;

  constructor(public constant: ConstantService,
              public partner: PartnerService) { }

  ngOnInit(): void {
    this.year = new Date().getFullYear()
  }

  public onIntersection({target, visible}: { target: Element; visible: boolean }) {
    visible ? this.showImage = true : null;
  }


}
