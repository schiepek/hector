import {AutoCompleteType} from './auto-complete-type';

export class AutoComplete {

  type: AutoCompleteType;
  name: string;
  path: string;

  constructor(type: AutoCompleteType, name: string, path: string) {
    this.type = type;
    this.name = name;
    this.path = path;
  }
}
