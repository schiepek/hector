import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from '../../components/home/home.component';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {WebcamThumbModule} from '../../shared/webcam-thumb/webcam-thumb.module';
import {AdModule} from '../../shared/ad/ad.module';
import {WebcamTagcloudModule} from '../../shared/webcam-tagcloud/webcam-tagcloud.module';
import {CountryThumbModule} from '../../shared/country-thumb/country-thumb.module';
import {ModelThumbModule} from '../../shared/model-thumb/model-thumb.module';
import {RouletteThumbModule} from '../../shared/roulette-thumb/roulette-thumb.module';
import {CategoryThumbModule} from '../../shared/category-thumb/category-thumb.module';


const routes: Routes = [
  {path: '', component: HomeComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    WebcamThumbModule,
    WebcamTagcloudModule,
    CountryThumbModule,
    RouletteThumbModule,
    CategoryThumbModule,
    ModelThumbModule,
    AdModule
  ],
  declarations: [
    HomeComponent
  ]
})
export class HomeModule {
}
