export class provider {

    public static readonly CHATURBATE: string = "chaturbate";
    public static readonly BONGACAMS: string = "bongacams";
    public static readonly AWEMPIRE: string = "awempire";
    public static readonly CAMSODA: string = "camsoda";
    public static readonly STRIPCHAT: string = "stripchat";

    public static readonly ALL: string[] = [
        provider.CHATURBATE,
        provider.BONGACAMS,
        provider.AWEMPIRE,
        provider.CAMSODA,
        provider.STRIPCHAT
    ]

}
