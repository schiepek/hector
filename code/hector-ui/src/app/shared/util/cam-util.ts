import {ScreenService} from '../../services/screen.service';
import {Cam} from '../../../../../hector-api/src/model/cam';

export class CamUtil {

  /**
   * If there is no embedded cam or it is mobile > we redirect to provider
   */
  public static hasToBeRedirectedToDetailPage(cam: Cam, screen: ScreenService) {
    return !(!cam.embedUrl || screen.isHiddenLgUp());
  }

  public static createFakeCam(): Cam {
    let cam = new Cam();
    cam.imageUrl = "/assets/images_hector/fake_cam.png";
    cam.numberOfUsers = 0;
    cam.title = "";
    cam.username = "";
    cam.tags = [];
    return cam;
  }

}
