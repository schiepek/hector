import {NavigationEnd, Router} from '@angular/router';
import {ChangeDetectorRef} from '@angular/core';
import {ClientUtil} from './client-util';

/**
 * Sometimes the component doesn't reload if parameters change. Subscribe
 * the ComponentLoader to the routes queryparams to force reload
 */
export class ComponentReloader {

  /**
   * if the params change we need to reload the whole component
   */
  public static reloadComponent(router: Router) {

    // override the route reuse strategy
    router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {

        // trick the Router into believing it's last link wasn't previously loaded
        router.navigated = false;

        // if you need to scroll back to top, here is the right place
        ClientUtil.isClientSide() ? window.scrollTo(0, 0) : null;
      }
    });
  }

  public static detectChanges(cdRef: ChangeDetectorRef): void {
    if (cdRef && !cdRef['destroyed']) {
      cdRef.detectChanges();
    }
  }
}
