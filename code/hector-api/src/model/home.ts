import {Cam} from './cam';
import {Country} from './country';

export class Home {

    topCams: Cam[];
    topCamsCountry: Cam[];
    topAsianCams: Cam[];
    topLatinCams: Cam[];
    randomCountries: Country[];

    constructor(topCams: Cam[], topCamsCountry: Cam[], topAsianCams: Cam[], topLatinCams: Cam[], randomCountries: Country[]) {
        this.topCams = topCams;
        this.topCamsCountry = topCamsCountry;
        this.topAsianCams = topAsianCams;
        this.topLatinCams = topLatinCams;
        this.randomCountries = randomCountries;
    }
}