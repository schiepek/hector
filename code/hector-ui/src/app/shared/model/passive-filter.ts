export class PassiveFilter {

  public isChecked: boolean;
  public link: string;
  public translationCode: string;

  constructor(isChecked: boolean, link: string, translationCode: string) {
    this.isChecked = isChecked;
    this.link = link;
    this.translationCode = translationCode;
  }
}
