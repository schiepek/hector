import {Component} from '@angular/core';
import {StateService} from '../../../services/state.service';
import {ConstantService} from '../../../services/constant.service';

@Component({
  selector: 'app-footer-page',
  templateUrl: './footer-page.component.html',
  styleUrls: ['./footer-page.component.css']
})
export class FooterPageComponent {

  constructor(public constant: ConstantService,
              public state: StateService) {
  }

}
