import {ChangeDetectorRef, Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {StateService} from './state.service';
import {debounceTime} from 'rxjs/operators';
import {EventUtil} from '../shared/util/event-util';
import {AutoSearchService} from './auto-search.service';
import {AutoComplete} from '../shared/model/auto-complete';
import {ComponentReloader} from '../shared/util/component-reloader';
import {ConstantService} from './constant.service';
import {CamResult} from '../../../../hector-api/src/model/cam-result';

@Injectable({
  providedIn: 'root'
})
export class AutoCompleteService {

  private static readonly UNSELECTED: number = -1;
  private static readonly DEBOUNCE_TIME: number = 200;

  private readonly subject: Subject<KeyboardEvent> = new Subject();

  public autocompleteResults: AutoComplete[] = [];
  public selectedIndex: number = AutoCompleteService.UNSELECTED;

  private cdRef: ChangeDetectorRef;

  constructor(private autoSearch: AutoSearchService,
              private constant: ConstantService,
              private state: StateService) {
    this.initAutoCompleteDebounce();
  }

  public init(cdRef: ChangeDetectorRef) {
    this.cdRef = cdRef;
  }

  public onKeyUp(event: KeyboardEvent) {
    this.subject.next(event);
  }

  private initAutoCompleteDebounce() {
    this.subject.pipe(
      debounceTime(AutoCompleteService.DEBOUNCE_TIME)
    ).subscribe((event: KeyboardEvent) => this.onSearchKeyUp(event));
  }

  private setAutoCompleteResult() {
    this.autoSearch.search().subscribe((data: [CamResult, AutoComplete[]])=> {
      this.autocompleteResults = this.autoSearch.dataToAutoComplete(data);
      this.selectedIndex = AutoCompleteService.UNSELECTED;
      ComponentReloader.detectChanges(this.cdRef)
    });
  }

  public clearAutocomplete(forced: boolean = false) {
    if(forced || !this.state.acHovered) {
      this.autocompleteResults = [];
      this.selectedIndex = AutoCompleteService.UNSELECTED;
      ComponentReloader.detectChanges(this.cdRef)
    }
  }

  public onSearchKeyUp(event: KeyboardEvent) {
    if (!EventUtil.isArrowOrEnterOrEsc(event)) {
      if (this.state.searchTerm && this.state.searchTerm.length > 1) {
        this.setAutoCompleteResult();
      } else {
        this.clearAutocomplete(null);
      }
    }
  }

  public onArrowDown() {
    if (this.hasResults()) {
      this.selectedIndex = this.selectedIndex >= 0 && this.selectedIndex < this.autocompleteResults.length - 1 ?
        this.selectedIndex + 1 : 0;
      this.setTermFromACIndex();
      ComponentReloader.detectChanges(this.cdRef)
    }

  }

  public onArrowUp() {
    if(this.hasResults()) {
      this.selectedIndex = this.selectedIndex > 0 ?
        this.selectedIndex - 1 : this.autocompleteResults.length - 1;
      this.setTermFromACIndex();
      ComponentReloader.detectChanges(this.cdRef)
    }
  }

  public setTermFromACIndex() {
    this.state.searchTerm = this.autocompleteResults[this.selectedIndex].name;
  }

  public isSelectedIndex(index: number) {
    return this.selectedIndex == index;
  }

  public hasResults(): boolean {
    return this.autocompleteResults.length > 0;
  }

  public getSearchOrResultPath(): string {
    if (this.selectedIndex < 0) {
      let result = this.autocompleteResults.find(r => this.searchTermMatchesResult(r));
      return result ? result.path : this.constant.SEARCH + "/" + this.state.searchTerm;
    }
    return this.autocompleteResults[this.selectedIndex].path;
  }

  private searchTermMatchesResult(autocomplete: AutoComplete): boolean {
    return this.state.searchTerm.toLowerCase() == autocomplete.name.toLowerCase() ||
      this.state.searchTerm.toLowerCase() == autocomplete.path.toLowerCase()
  }

  public mouseOver(index: number): void {
    this.selectedIndex = index;
    this.state.acHovered = true;
  }

  public mouseOut(): void {
    this.selectedIndex = AutoCompleteService.UNSELECTED;
    this.state.acHovered = false;
  }
}
