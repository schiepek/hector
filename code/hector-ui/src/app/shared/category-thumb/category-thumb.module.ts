import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {CategoryThumbComponent} from '../../components/category/category-thumb/category-thumb.component';

@NgModule({
  declarations: [CategoryThumbComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild()
  ],
  exports: [
    CategoryThumbComponent
  ]
})
export class CategoryThumbModule {
}
