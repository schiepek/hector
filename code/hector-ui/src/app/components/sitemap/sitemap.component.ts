import {Component, OnInit} from '@angular/core';
import {CountryService} from '../../services/country.service';
import {WebcamService} from '../../services/webcam.service';
import {MetaService} from '../../services/meta.service';
import {StateService} from '../../services/state.service';
import {TagService} from '../../services/tag.service';

@Component({
  selector: 'app-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.css']
})
export class SitemapComponent implements OnInit {

  public tags: string[];

  constructor(private webcam: WebcamService,
              private meta: MetaService,
              private state: StateService,
              private tagS: TagService,
              public countryS: CountryService) {
    this.meta.setSitemapMetaTags();
  }

  ngOnInit() {
    this.state.reset();
    this.tags = this.tagS.getAllTags();
    this.webcam.getMenu().then(res => {
      this.countryS.initAllCountries(res.countries);
    })
  }

}
