export class ExpirationData {
    private camsDate: Date;

    public readonly maxExpirationInMinutes: number = 1;

    private static readonly defaultInvalidInMinutes: number = 5;

    private static maxInvalidInMinutes: number = process.env.STAGE === "dev" ?
        (process.env.DEV_MAX_INVALID_IN_MINUTES ? +process.env.DEV_MAX_INVALID_IN_MINUTES : ExpirationData.defaultInvalidInMinutes) :
        (process.env.PROD_MAX_INVALID_IN_MINUTES ? +process.env.PROD_MAX_INVALID_IN_MINUTES : ExpirationData.defaultInvalidInMinutes)

    public static readonly surelyExpiredTimeInMinutes: number = 1440;

    public setDate(): void {
        this.camsDate = new Date();
    }

    public isExpired(): boolean {
        if (this.camsDate) {
            let maximumExpiredDate = new Date();
            maximumExpiredDate.setTime(new Date().getTime() - (this.maxExpirationInMinutes * 60 * 1000));

            return maximumExpiredDate > this.camsDate;
        }
        return true;
    }

    public static isValidCamsDate(date: Date): boolean {
        let lastValidDate = new Date();
        lastValidDate.setTime(new Date().getTime() - (this.maxInvalidInMinutes * 60 * 1000));

        return new Date(date) > lastValidDate;
    }

    public static createExpiredDate(): Date {
        let expiredDate = new Date();
        expiredDate.setTime(new Date().getTime() - (this.surelyExpiredTimeInMinutes * 60 * 1000));
        return expiredDate;
    }
}
