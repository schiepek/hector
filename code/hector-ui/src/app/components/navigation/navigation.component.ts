import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ScreenService} from '../../services/screen.service';
import {StateService} from '../../services/state.service';
import {CountryService} from '../../services/country.service';
import {CategoryService} from '../../services/category.service';
import {CategoryCode} from '../../../../../hector-api/src/model/category-code';
import {TranslateService} from '@ngx-translate/core';
import {ClientUtil} from '../../shared/util/client-util';
import {LanguageUtil} from '../../shared/util/language-util';
import {ConstantService} from '../../services/constant.service';
import {PartnerService} from '../../services/partner.service';
import {AutoCompleteService} from '../../services/auto-complete.service';

declare var $: any;

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  @ViewChild('searchInput', {static: false})
  searchInput: ElementRef;

  public language: string;

  constructor(private translate: TranslateService,
              public constant: ConstantService,
              public screen: ScreenService,
              public categoryS: CategoryService,
              public countryS: CountryService,
              private cdRef: ChangeDetectorRef,
              public autoComplete: AutoCompleteService,
              public partnerS: PartnerService,
              public state: StateService) {
  }

  ngOnInit(): void {
    this.partnerS.initNavigationChangeDetector(this.cdRef);
    this.autoComplete.init(this.cdRef);
    this.language = LanguageUtil.getCurrentLanguage(this.translate);
  }

  public changeLanguage(languageCode: string) {
    ClientUtil.isClientSide() && window.location.reload();
    this.translate.use(languageCode);
    this.language = languageCode;
  }

  public CategoryCode() {
    return CategoryCode;
  }

  public focusOnSearchInputAndToggle() {
    if(ClientUtil.isClientSide()) {
      $('#header-mobile__search-icon').toggleClass('header-mobile__search-icon--close');
      $('.header-mobile').toggleClass('header-mobile--expanded');
      ClientUtil.isClientSide() ? $('#searchInput').focus() : null;
    }
  }

  public closeKeyboard() {
    this.searchInput.nativeElement.blur();
  }

  public toggleMobileSubMenu(menuId: string): void {

    if (ClientUtil.isClientSide() && this.screen.isHiddenLgUp()) {
      let identifier = '#' + menuId;

      $(identifier).find('.main-nav__toggle').toggleClass('main-nav__toggle--rotate')
        .parent().siblings().children().removeClass('main-nav__toggle--rotate');

      $(identifier).find(".main-nav__sub, .main-nav__megamenu").not($('#' + menuId).find('.main-nav__toggle').siblings('.main-nav__sub, .main-nav__megamenu')).slideUp('normal');

      $(identifier).find('.main-nav__toggle').siblings('.main-nav__sub').slideToggle('normal');
      $(identifier).find('.main-nav__toggle').siblings('.main-nav__megamenu').slideToggle('normal');
    }

  }

  public toggleDesktopSubMenu(menuId: string): void {
    (ClientUtil.isClientSide() && this.screen.isHiddenMdDown()) ? $('#' + menuId).find('.main-nav__toggle').siblings('.main-nav__megamenu').css({'display': 'block'}) : null;
  }

  public closeNavMegaMenu(menuId?: string): void {
    ClientUtil.isClientSide() && this.screen.isHiddenLgUp() ? $('.site-wrapper').toggleClass('site-wrapper--has-overlay') : null;
    ClientUtil.isClientSide() && this.screen.isHiddenMdDown() ? $('#' + menuId).find('.main-nav__toggle').siblings('.main-nav__megamenu').css({display: 'none'}) : null;
  }

  public toggleMobileMenu(): void {
    $('.site-wrapper').toggleClass('site-wrapper--has-overlay');
  }

}
