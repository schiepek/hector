import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {TestType} from '../general/test-type';


export class MenuTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {

        this.tests.set("menu-country", (executor, config) => {
            executor.executeRestTestFunction(config.getMenuFunction(), 'should return the list of all countries with menu path', {page: 1},
                res => {
                    assert.isTrue(res["cams"].length === 0, "there should be no cams with the menu function");

                    assert.isTrue(res["countries"].length > 10, "there should be more than 10 cams in the countries list");
                    assert.isTrue(res["countries"].some(co => co.code === "us"), "the usa should be in the countries list");
                    assert.isTrue(res["countries"].some(co => co.code === "co"), "colombia should be in the countries list");
                    assert.isTrue(res["countries"].some(co => co.code === "ru"), "russia should be in the countries list");
                }
            );
        });

    }
}
