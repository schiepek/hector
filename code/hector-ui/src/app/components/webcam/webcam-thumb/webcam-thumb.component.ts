import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {Cam} from '../../../../../../hector-api/src/model/cam';
import {TranslateService} from '@ngx-translate/core';
import {CountryService} from '../../../services/country.service';
import {ScreenService} from '../../../services/screen.service';
import {Router} from '@angular/router';
import {ClientUtil} from '../../../shared/util/client-util';
import {provider} from '../../../../../../hector-api/src/model/provider';
import {ConstantService} from '../../../services/constant.service';
import {CamUtil} from '../../../shared/util/cam-util';

@Component({
  selector: 'app-webcam-thumb',
  templateUrl: './webcam-thumb.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./webcam-thumb.component.css']
})
export class WebcamThumbComponent implements OnInit {

  @Input()
  public cam: Cam;

  @Input()
  public rank?: number;

  @Input()
  public isTopList?: boolean;

  public countryTitle: string;
  public countryLink: string;

  public mainText: string;
  public detailLink: string;
  public tooltip: string;
  public locationInfo: string;
  public hasEmbedCam: boolean;
  public isAwEmpire: boolean;
  public currentUrl: string;
  public imageUrl: string;
  public showImage: boolean;

  private from: string;

  constructor(public countryS: CountryService,
              public screen: ScreenService,
              public constant: ConstantService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private translate: TranslateService) {
  }

  ngOnInit() {
    this.initImage();
    this.translate.get("cam-page.from").subscribe(res => this.from = res);
    this.mainText = this.getMainText();
    this.isAwEmpire = this.cam.provider === provider.AWEMPIRE;
    this.detailLink = '/' + this.cam.provider + '/' + this.cam.username;
    this.tooltip = this.getTooltip();
    this.locationInfo = this.getLocationInfo();
    this.currentUrl = this.router.url;
    this.countryTitle = this.cam.country ? this.countryS.getName(this.cam.country) : this.cam.title;
    this.countryLink = this.cam.country ? this.countryS.getUrl(this.cam.country) : undefined;
    this.hasEmbedCam = this.getHasEmbedCam();
  }

  public onIntersection({target, visible}: { target: Element; visible: boolean }) {
    visible ? this.showImage = true : null;
  }

  private initImage() : void {
    this.imageUrl = this.isNotFakeCam() ? this.cam.imageUrl + '?t=' + new Date().getTime() : this.cam.imageUrl;
  }

  private isNotFakeCam() : boolean {
    return !this.cam.imageUrl.includes("fake_cam.png")
  }

  public getMainText(): string {
    if (this.cam.title) {
      return this.cam.title;
    }
    if (this.cam.turnsOn) {
      return this.cam.turnsOn;
    }
    let newTitle = this.cam.username;
    if (this.cam.hairColor) {
      newTitle += " (" + this.cam.hairColor + ")";
    }
    if (this.cam.location) {
      newTitle += " " + this.from + " " + this.cam.location;
    }
    return newTitle;
  }

  public getTooltip(): string {
    let tooltip = "";
    tooltip += this.cam.username ? "username: " + this.cam.username : "";
    tooltip += this.cam.age ? "(" + this.cam.age + ")" + this.constant.NEW_LINE : this.constant.NEW_LINE;
    tooltip += this.cam.title ? "title: " + this.cam.title + this.constant.NEW_LINE : "";
    tooltip += this.cam.tags.length > 0 ? "tags: " + this.cam.tags + this.constant.NEW_LINE : "";
    tooltip += this.cam.categories && this.cam.categories.length > 0 ? "categories: " + this.cam.categories + this.constant.NEW_LINE : "";
    tooltip += this.cam.location ? "location: " + this.cam.location + this.constant.NEW_LINE : "";
    tooltip += this.cam.languages ? "languages: " + this.cam.languages + this.constant.NEW_LINE : "";
    tooltip += this.cam.hairColor ? "haircolor: " + this.cam.hairColor + this.constant.NEW_LINE : "";
    tooltip += this.cam.body ? "body: " + this.cam.body + this.constant.NEW_LINE : "";
    tooltip += this.cam.numberOfUsers ? this.cam.numberOfUsers + " users" : "";
    return tooltip;
  }

  public getLocationInfo(): string {
    if (this.cam.country && this.cam.location) {
      let locationInfo = this.countryS.getName(this.cam.country);
      if (locationInfo.toLowerCase() !== this.cam.location.toLowerCase()) {
        locationInfo += " (" + this.cam.location + ")";
      }
      return locationInfo;
    }
    if (this.cam.country) {
      return this.countryS.getName(this.cam.country);
    }
    if (this.cam.location) {
      return this.cam.location;
    }
    if(this.cam.categories && this.cam.categories.length > 0) {
      return this.cam.categories.toLocaleString()
    }
    if(this.cam.title) {
      return this.cam.title;
    }
    if(this.cam.hairColor) {
      return this.cam.hairColor;
    }
    return "";
  }

  private getHasEmbedCam(): boolean {
    return CamUtil.hasToBeRedirectedToDetailPage(this.cam, this.screen)
  }

  /**
   * Just happening with providers without embedded URLs
   */
  public navigateToDetail() {
    ClientUtil.isClientSide() && window.open(this.cam.chatRoomUrl, "_blank");
  }


}
