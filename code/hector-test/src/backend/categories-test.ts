import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {TestType} from '../general/test-type';
import {CategoryItemType} from '../../../hector-api/src/model/category-item-type';
import {CategoryItem} from '../../../hector-api/src/model/category-item';


export class CategoriesTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {
        this.tests.set("categories", (executor, config) => {
            executor.executeRestTestFunction(config.getCategoriesFunction(), 'should return cams with asian countries or tag asian', {},
                r => {
                    assert.isNotNull(r.categories, "categories should be loaded");
                    assert.isAtLeast(r.categories!.length, 250, "There should be more than 250 elements in categories");

                    CategoriesTest.assertCategoryItem("asian", CategoryItemType.CATEGORY, 30, r.categories);
                    CategoriesTest.assertCategoryItem("bigtits", CategoryItemType.CATEGORY, 20, r.categories);
                    CategoriesTest.assertCategoryItem("de", CategoryItemType.COUNTRY, 5, r.categories);
                    CategoriesTest.assertCategoryItem("co", CategoryItemType.COUNTRY, 20, r.categories);
                    CategoriesTest.assertCategoryItem("squirt", CategoryItemType.TAG, 20, r.categories);
                    CategoriesTest.assertCategoryItem("anal", CategoryItemType.TAG, 20, r.categories);
                }
            );
        });

        this.tests.set("categories-gender", (executor, config) => {
            ["female", "male", "couple", "shemale"].forEach(gender => {
                executor.executeRestTestFunction(config.getCategoriesFunction(), 'should return cams with asian countries or tag asian', {filter: gender},
                    r => {
                        assert.isNotNull(r.categories, "categories should be loaded");
                        assert.isAtLeast(r.categories!.length, 150, "There should be more than 250 elements in categories");
                        CategoriesTest.assertCategoryItem("asian", CategoryItemType.CATEGORY, 5, r.categories);
                    }
                );
            })
        });
    }



    private static assertCategoryItem(code: string, type: CategoryItemType, minSize: number, categories: CategoryItem[]) {
        let category = categories.find(cit=>cit.code === code);
        assert.isNotNull(category, category + " was not present in categories");
        assert.equal(category!.type, type);
        assert.isAtLeast(category!.size, minSize, "category size of " + code + " should be greater than " + minSize);
    }


}
