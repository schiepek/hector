import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {Partner} from '../../../hector-api/src/model/partner';
import {TestType} from '../general/test-type';


export class PartnerTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {
        this.tests.set("partner-home", (executor, config) => executor.executeRestTestFunction(config.getHomeFunction(), 'home function should have partners', {
            page: 1,
            project: "lemoncams",
            path: "."
        }, res => PartnerTest.assertPartner(res["partners"], ".", true)));
        this.tests.set("partner-cams", (executor, config) => executor.executeRestTestFunction(config.getCamsFunction(), 'cams function category UK should have partners', {
            page: 1,
            project: "lemoncams",
            path: ".united-kingdom"
        }, res => PartnerTest.assertPartner(res["partners"], ".united-kingdom", false)));
        this.tests.set("partner-none", (executor, config) => executor.executeRestTestFunction(config.getCamsFunction(), 'cams function category none existing shouldn\'t have partners', {
            page: 1,
            project: "lemoncams",
            path: ".nonexistingcategory"
        }, res => PartnerTest.assertPartnerEmpty(res["partners"], false)));
    }

    public static assertPartner(partner: Partner[], path: string, hasTop: boolean): void {
        let topPartner = partner.find(p => p.type === "top");
        let listPartner = partner.find(p => p.type === "list");

        hasTop ? assert.isTrue(topPartner && !!topPartner.text && !!topPartner.link, "no top partner or a property of it is missing") : null;
        assert.isTrue(listPartner && !!listPartner.text && !!listPartner.link, "no list partner or a property of it is missing");

        partner.filter(p => p.type === "list").forEach(p => assert.equal(p.page, path, "partner path should match"));
    }

    public static assertPartnerEmpty(partner: Partner[], hasTop: boolean): void {
        let topPartner = partner.find(p => p.type === "top");
        let listPartner = partner.find(p => p.type === "list");

        hasTop ? assert.isTrue(topPartner && !!topPartner.text && !!topPartner.link, "no top partner or a property of it is missing"): null;
        assert.isFalse(!!listPartner, "There should be no partners with list type");
    }
}
