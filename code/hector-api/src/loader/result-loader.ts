import {Cam} from '../model/cam';
import {CamResult} from '../model/cam-result';
import {CountryUtil} from '../util/country-util';
import {HomeUtil} from '../util/home-util';
import {DataUtil} from '../util/data-util';
import {SortFunction} from '../function/sort-function';
import {S3Util} from '../persistence/s3-util';
import {ExpirationData} from '../util/expiration-data';
import {Meta} from '../model/meta';
import {CamsLoader} from './cams-loader';
import {ProviderCamResult} from '../model/provider-cam-result';
import {Categorizer} from '../categorizer/categorizer';
import {TagEvaluator} from '../util/tag-evaluator';

export class ResultLoader {

    private static instance: ResultLoader;
    private categorizer: Categorizer = new Categorizer();

    public static getInstance() {
        return this.instance || (this.instance = new this());
    }

    public loadAndSaveAllCams(): Promise<CamResult> {
        return new Promise<CamResult>((resolve) => {

            let providerCamsPromises = CamsLoader.getInstance()
                .loaders
                .map(loader => this.loadProviderCams(loader.getProvider()));

            let allCams: Cam[] = []

            Promise.all(providerCamsPromises).then(results => {
                results.forEach(result => {
                    if (ExpirationData.isValidCamsDate(result.date)) {
                        allCams.push(...result.cams);
                    } else {
                        console.error("S3 Cams Expired, they are too old " + result.date);
                    }
                })
                return allCams;
            }).then(collectedCams => {
                this.createFinalResult(collectedCams, resolve);
            })
        });
    }

    public loadProviderCams(provider: string): Promise<ProviderCamResult> {
        return new Promise<ProviderCamResult>((resolve, reject) => {
            let s3Callback = (err: any, data: any) => {
                if(err) {
                    console.error("cams could not be loaded from s3 " + provider);
                    resolve(ProviderCamResult.expiredEmpty());
                } else {
                    try {
                        let result = JSON.parse(data.Body.toString()) as ProviderCamResult;
                        if (result && result.cams) {
                            resolve(result);
                        } else {
                            console.error("data was loaded from s3 but result is empty " + provider);
                            reject();
                        }
                    } catch (e) {
                        console.error("data was loaded from s3, but not parsable " + provider);
                        reject();
                    }
                }
            };
            let offlineS3Callback = () => {
                resolve(new ProviderCamResult([], 0, new Date()));
            };
            S3Util.getCamsFromS3(s3Callback, provider, offlineS3Callback);
        });
    }

    private createFinalResult(newCams: Cam[], resolve: any) {
        let countries = CountryUtil.getCountrySet(newCams);
        let sortedCams = new SortFunction("balanced").processCams(newCams);
        let home = HomeUtil.createHome(sortedCams, countries, this.categorizer);
        let tags = TagEvaluator.executeForCams(sortedCams);
        DataUtil.getData().then((res: any) => {
            let result = new CamResult(sortedCams, countries, sortedCams.length, 0, home, tags, [], new Date(), Meta.WARMUP);
            result.projectPartners = DataUtil.reducePartners(res["partners"]);
            S3Util.saveCamsToS3(result, resolve);
        });
    }

}
