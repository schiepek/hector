import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {MetaService} from '../../../services/meta.service';
import {CountryService} from '../../../services/country.service';
import {StateService} from '../../../services/state.service';
import {ScreenService} from '../../../services/screen.service';
import {Category} from '../../../shared/model/category';
import {CategoryCode} from '../../../../../../hector-api/src/model/category-code';
import {ConstantService} from '../../../services/constant.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-webcam-title',
  templateUrl: './webcam-title.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./webcam-title.component.css']
})
export class WebcamTitleComponent implements OnInit {

  public readonly tranlateCategoryTitle: string = "category-title.";
  public readonly translateTitle: string = "-title";
  public readonly translateDescription: string = "-description";

  @Input()
  public size?: number;

  @Input()
  public noResults?: boolean;

  @Input()
  public categories: Category[];

  public title: string;
  public description: string;

  constructor(private constant: ConstantService,
              private country: CountryService,
              private translate: TranslateService,
              public meta: MetaService,
              public countryS: CountryService,
              public state: StateService,
              public screen: ScreenService) {
  }

  ngOnInit(): void {
    this.setTitleAndDescription()
  }

  public setTitleAndDescription(): void {
    if (this.categories.length > 0 && this.categories[0].code === CategoryCode.query) {
      this.setSearchText(this.state.query);
    } else if (this.categories.length > 0 && this.categories[0].code === CategoryCode.country) {
      this.setCountryText(this.state.country);
    } else if(this.categories.length > 0 && this.categories[0].group === "provider") {
      this.setProviderText(this.state.provider);
    } else if(this.categories.length > 0 && this.categories[0].group === "tag") {
      this.setTagText(this.state.tag);
    } else if (this.categories.length > 0 && this.categories[0].group !== "page") {
      this.setCategoryText(this.categories[0].path);
    } else {
      this.setCategoryText(this.constant.CAMS);
    }
  }

  private setCategoryText(route: string): void {
    let titleTransCode = this.tranlateCategoryTitle + route + this.translateTitle;
    let descrTransCode = this.tranlateCategoryTitle + route + this.translateDescription;
    this.setText(titleTransCode,descrTransCode);
  }

  private setProviderText(provider: string): void {
    this.translate.get("provider." + provider).subscribe(p => {
      this.setText("category-title.provider-title", "category-title.provider-description", {value1: p})
    })
  }

  private setSearchText(query: string): void {
    this.setText("meta-search.title","meta-search.description", {value: query});
  }

  private setTagText(tag: string): void {
    this.setText("meta-tag.title","meta-tag.description", {value: tag});
  }

  private setCountryText(countryCode: string): void {
    let countryName = this.country.getName(countryCode);
    let countryAdj = this.country.getAdjective(countryCode);
    this.setText("category-title.country-title","category-title.country-description", {value1: countryAdj, value2: countryName});
  }

  private setText(titleTransCode: string, descrTransCode: string, interpolateParams?: Object): void {
    let completeParams = this.meta.getCompleteInterpolateParams(interpolateParams);
    this.translate.get(titleTransCode, completeParams).subscribe(t => this.title= t);
    this.translate.get(descrTransCode, completeParams).subscribe(d => this.description= d);
  }


}
