import * as config from '../../config.json';
import {TestType} from '../general/test-type';

export class TestConfig {

    private envVariables: any;


    constructor(envVariables: any) {
        this.envVariables = envVariables;
    }

    public isHelp(): boolean {
        return !!this.envVariables.npm_config_testhelp;
    }

    public getTestType(): TestType {
        if(this.envVariables.npm_config_type) {
            switch (this.envVariables.npm_config_type) {
                case "ALL": return TestType.ALL;
                case "UI": return TestType.UI;
                case "BACKEND": return TestType.BACKEND;
                case "WARMUP": return TestType.WARMUP;
            }
        }
        return TestType.ALL;
    }

    public getEndpoint(): string {
        if (this.envVariables.npm_config_endpoint) {
            return config[this.envVariables.npm_config_endpoint];
        }
        return config["endpoint-dev"];

    }

    public getUiUrlProd(): string {
        return config["ui-prod"];
    }

    public getUiRenderUrlProd(): string {
        return config["ui-render-prod"];
    }

    public getTestFunction(): string {
        return this.envVariables.npm_config_function;
    }

    public getMainPath(): string {
        return config["mainPath"];
    }

    public getCamsFunction(): string {
        return config["camsFunction"];
    }

    public getCamFunction(): string {
        return config["camFunction"];
    }

    public getMenuFunction(): string {
        return config["menuFunction"];
    }

    public getHomeFunction(): string {
        return config["homeFunction"];
    }

    public getCategoriesFunction(): string {
        return config["categoriesFunction"];
    }

    public getRandomFunction(): string {
        return config["randomFunction"];
    }

    public getAutoCompleteFunction(): string {
        return config["autocompleteFunction"];
    }

    public getWarmupAllFunction(): string {
        return config["warmupAllFunction"];
    }

    public getProviderWarmupFunction(provider: string) : string {
        return config["warmupFunction"] + "-" + provider;
    }

    public getExpiredFunction(): string {
        return config["expiredFunction"];
    }

    public getDevWarmupTime() : number {
        return config["devWarmupTime"];
    }


}
