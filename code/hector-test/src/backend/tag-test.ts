import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {TestType} from '../general/test-type';


export class TagTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {
        this.tests.set("tag-anal", (executor, config) => {
            executor.executeRestTestCams('should return cams with tag \"anal\"', {
                    page: 1,
                    tag: "anal"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 anal cams");

                    r.cams.forEach(c => {
                        assert.isTrue(c.tags.some(ca => ca === "anal"), "tag should be set");
                    })
                }
            );
        });

    }
}
