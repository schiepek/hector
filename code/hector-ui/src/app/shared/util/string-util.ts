export class StringUtil {

  public static startsWithAlphanumeric(str: string): boolean {
    if(str != null && str.length > 0) {
      let firstCharCode = str.charCodeAt(0);
      return (firstCharCode > 64 && firstCharCode < 91) || (firstCharCode > 96 && firstCharCode < 123);
    }
    return false;
  }

  public static capitalizeFirstLetter(str: string) : string {
    if(str) {
      return str.charAt(0).toUpperCase() + str.slice(1);
    }
    return "";
  }

}
