import {TranslateService} from '@ngx-translate/core';

export class LanguageUtil {

  public static readonly defaultLanguage = "en";

  private static possibleLanguages = ["en", "es", "de"];

  public static setLanguage(translate: TranslateService) {
    translate.setDefaultLang(this.defaultLanguage);
    translate.use(this.defaultLanguage);
    // let cookieLanguage = cookie.get(Constants.COOKIE_LANGUAGE);
    //translate.use(cookieLanguage ? cookieLanguage : this.getNavigatorOrDefaultLanguage());
  }

  public static getCurrentLanguage(translate: TranslateService): string {
    return translate.currentLang ? translate.currentLang : this.defaultLanguage;
  }

  /*  private static getNavigatorOrDefaultLanguage(): string {
      let navLanguage = navigator.language ? navigator.language.slice(0, 2) : null;
      if (navLanguage) {
        return navLanguage ? navLanguage :this.possibleLanguages.find(l => l === navLanguage) ? navLanguage : this.defaultLanguage;
      }
      return this.defaultLanguage;
    }*/

}
