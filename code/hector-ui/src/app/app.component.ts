import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {LanguageUtil} from './shared/util/language-util';
import {ComponentReloader} from './shared/util/component-reloader';
import {ClientUtil} from './shared/util/client-util';
import {MetaService} from './services/meta.service';
import {AlchemistsUtil} from './shared/util/alchemists-util';
import {LinkService} from './services/link.service';
import {AnalyticsService} from './services/analytics.service';

declare var gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router,
              private analytics: AnalyticsService,
              private route: ActivatedRoute,
              private meta: MetaService,
              private link: LinkService,
              private translate: TranslateService) {
    ClientUtil.isClientSide() ? AlchemistsUtil.initJQuery() : null;
    ClientUtil.isClientSide() ? this.analytics.init() : null;
    LanguageUtil.setLanguage(this.translate);
    this.setOrientationChangeListener();
    this.initComponentReloader();
  }

  private initComponentReloader() {
    this.route.params.subscribe(() => {
      ComponentReloader.reloadComponent(this.router);
    });
  }

  public setOrientationChangeListener() {
    if (ClientUtil.isClientSide()) {
      window.addEventListener("orientationchange", function () {
        window.location.reload(false);
      }, false);
    }

  }

}
