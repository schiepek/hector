import { CountryThumbModule } from './country-thumb.module';

describe('CountryThumbModule', () => {
  let countryThumbModule: CountryThumbModule;

  beforeEach(() => {
    countryThumbModule = new CountryThumbModule();
  });

  it('should create an instance', () => {
    expect(countryThumbModule).toBeTruthy();
  });
});
