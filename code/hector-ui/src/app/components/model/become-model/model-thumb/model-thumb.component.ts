import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ConstantService} from '../../../../services/constant.service';

@Component({
  selector: 'app-model-thumb',
  templateUrl: './model-thumb.component.html',
  styleUrls: ['./model-thumb.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelThumbComponent implements OnInit {

  constructor(public constant: ConstantService) { }

  ngOnInit() {
  }

}
