import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Cam} from '../../../../hector-api/src/model/cam';

import {CountryUI} from '../shared/model/country-ui';
import {TranslateService} from '@ngx-translate/core';
import {ArrayUtil} from '../shared/util/array-util';
import {ScreenService} from './screen.service';
import {Country} from '../../../../hector-api/src/model/country';
import {StringUtil} from '../../../../hector-api/src/util/string-util';

import countries_en from 'assets/i18n/countries_en.json';
import countries_adj_en from 'assets/i18n/countries_adj_en.json';
import countries_continent from 'assets/i18n/countries_continent.json';
import continents_en from 'assets/i18n/continents_en.json';
import countries_url from 'assets/i18n/countries_url.json';
import {LanguageUtil} from '../shared/util/language-util';
import {environment} from '../../environments/environment';

const NAMES = {
  en: countries_en,
};

const ADJECTIVES = {
  en: countries_adj_en,
};

const CONTINENTS = {
  en: continents_en,
}

@Injectable()
export class CountryService {


  public countriesMenu: Country[][] = [];
  public countriesMap: CountryUI[];
  public continentsMap: Map<string, CountryUI[]>;
  public allCountries: CountryUI[] = [];

  private readonly noOfColumnsDesktop: number = 4;
  private readonly noOfRowsDesktop: number = 8;

  private readonly noOfColumnsMobile: number = 1;
  private readonly noOfRowsMobile: number = 21;

  public readonly defaultUserLogo: string = "assets/images_hector/logo/" + environment.project + "/user_logo.png";

  private readonly countryLib: string = "https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/flags/1x1/";
  private readonly countryLibExtension: string = ".svg";
  private readonly notFoundName: string = "unknown";

  private readonly columns: number;
  private readonly rows: number;

  private readonly lang: string;

  constructor(private http: HttpClient,
              private screen: ScreenService,
              private translate: TranslateService) {
    this.columns = this.screen.isHiddenMdDown() ? this.noOfColumnsDesktop : this.noOfColumnsMobile;
    this.rows = this.screen.isHiddenMdDown() ? this.noOfRowsDesktop : this.noOfRowsMobile;
    this.lang = LanguageUtil.getCurrentLanguage(translate);
  }

  public getFlagUrl(cam: Cam): string {
    return cam.country ? this.getFlagUrlWithCode(cam.country) : this.defaultUserLogo;
  }

  public getFlagUrlWithCode(countryCode: string): string {
    return this.countryLib + countryCode + this.countryLibExtension;
  }

  public getName(code: string): string {
    return NAMES[this.lang][code.toUpperCase()];
  }

  public getAdjective(code: string): string {
    return ADJECTIVES[this.lang][code.toUpperCase()];
  }

  public getUrl(code: string): string {
    return countries_url[code.toUpperCase()].toLowerCase();
  }

  public getCode(url: string): string {
    try {
      return Object.keys(countries_url).find(k => countries_url[k].toLowerCase() === url);
    } catch (err) {
      return null;
    }
  }

  public getCountriesUI(resCountries: Country[], coJson: any, coUrlJson: any): CountryUI[] {
    let countries: CountryUI[] = [];
    resCountries.forEach(c => countries.push(new CountryUI(c.code, this.getCountryTranslatedName(c.code, coUrlJson).toLowerCase(), c.size, this.getCountryTranslatedName(c.code, coJson))));
    return countries;
  }

  public initCountryGridMenu(resCountries: Country[]): void {
    this.countriesMenu = ArrayUtil.getGridArrayRemoveLast(this.columns, this.rows, this.getCountriesUI(resCountries, NAMES[this.lang], countries_url)) as CountryUI[][];
    this.initCountryGridPage(resCountries);
  }

  public initCountryGridPage(resCountries: Country[]): void {
    this.allCountries = this.getCountriesUI(resCountries, NAMES[this.lang], countries_url).sort((a, b) => a.name.localeCompare(b.name));
    this.countriesMap = Object.assign([], this.allCountries).sort((a: CountryUI, b: CountryUI) => b.size - a.size);
  }

  public getContinentCodes(): string[] {
    return Object.keys(this.continentsMap);
  }

  public getContinentName(code: string) : string {
    return CONTINENTS[this.lang][code];
  }

  public initContinentsMap() : void {
    this.continentsMap ? this.continentsMap.clear() :
      this.continentsMap = new Map<string, CountryUI[]>();

    for(let key of Object.keys(CONTINENTS[this.lang])) {
      this.continentsMap[key] = [];
    }

    for(let countryUI of this.countriesMap) {
      let continent = countries_continent[countryUI.code.toUpperCase()];
      if(continent) {
        this.continentsMap[continent].push(countryUI);
      }
    }
  }

  public initAllCountriesAC(): void {
    this.allCountries.length == 0 ? this.initAllCountries(this.countriesMap) : null;
  }

  public initAllCountries(resCountries: Country[]): void {
    this.allCountries = this.getCountriesUI(resCountries, NAMES[this.lang], countries_url).sort((a, b) => a.name.localeCompare(b.name));
  }

  public filterCountries(searchTerm: string) {
    if (searchTerm) {
      this.countriesMap = this.allCountries.filter(c => StringUtil.containsCaseIns(c.name, searchTerm))
        .sort((a: CountryUI, b: CountryUI) => b.size - a.size);
    } else {
      this.countriesMap = Object.assign([], this.allCountries)
        .sort((a: CountryUI, b: CountryUI) => b.size - a.size);
    }
    this.initContinentsMap();

  }

  public getCountryTranslatedName(countryCode: string, countryJson: any) {
    let translatedName = countryJson[countryCode.toUpperCase()];
    return translatedName ? translatedName : this.notFoundName;
  }

}
