# HectorLambda

## Commands for the serverless framework

* sls deploy
* sls deploy function -f bot
* sls remove

* sls invoke local -f bot -p test/viewer_request_2.json

* rmdir /S .build

## Libraries used

* For Rest Requests: https://github.com/request/request-promise
