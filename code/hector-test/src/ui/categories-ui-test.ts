import {AbstractTest} from '../backend/abstract-test';
import {assert} from 'chai';
import {TestType} from '../general/test-type';
import {parse} from 'node-html-parser';

export class CategoriesUiTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.UI;
    }

    protected initTests(): void {

        this.tests.set("categories-ui", (executor) => {

            let url = executor.config.getUiRenderUrlProd() + "/categories"

            executor.executeRestTest(url, url + ': categories page should have valid content', res => {
                assert.equal(res.status, 200, url + ' does not return status 200');

                CategoriesUiTest.assertAllTagsAreThere(res.text);
                CategoriesUiTest.assertThumbIsComplete(res.text);

            })
        });

        this.tests.set("categories-ui-gender", (executor) => {

            let url = executor.config.getUiRenderUrlProd() + "/categories/male"

            executor.executeRestTest(url, url + ': categories page should have valid content', res => {
                assert.equal(res.status, 200, url + ' does not return status 200');

                CategoriesUiTest.assertAllTagsAreThere(res.text);
                CategoriesUiTest.assertThumbIsComplete(res.text);

            })
        });

    }

    private static assertAllTagsAreThere(html: string): void {
        let doc = parse(html);
        let allTagsContainer = doc.querySelectorAll(".all-tags-padding");
        assert.isNotEmpty(allTagsContainer);
    }

    private static assertThumbIsComplete(html: string): void {
        let doc = parse(html);
        let allTagListBlocks = doc.querySelectorAll(".tag-list-block");

        assert.isNotEmpty(allTagListBlocks);
        let tagListsB = parse(allTagListBlocks[1].querySelectorAll(".grid-container").toString()).querySelectorAll(".tag-list");
        assert.isTrue(tagListsB.toString().includes("Bigtits"));

        let tagListsC = parse(allTagListBlocks[2].querySelectorAll(".grid-container").toString()).querySelectorAll(".tag-list");
        assert.isTrue(tagListsC.toString().includes("Colombia"));
        assert.isTrue(tagListsC.toString().includes("co.svg"));
        assert.isTrue(tagListsC.toString().includes("Cumface"));

        let tagListCSpans = parse(tagListsC.toString()).querySelectorAll(".tag-list-span");
        assert.isNotEmpty(tagListCSpans);
        tagListCSpans.forEach(span=> assert.isTrue(+span.innerHTML > 0));
    }

}
