import {Serverless} from 'serverless/plugins/aws/provider/awsProvider';

const serverlessConfiguration: Serverless = {
    service: {
        name: 'hector-api',
    },
    frameworkVersion: '>=1.72.0',
    custom: {
        webpack: {
            webpackConfig: './webpack.config.js',
            includeModules: true
        },
        apiCloudfront: { // Set default TTL and maximum TTL to 60
            domain: 'api-v2-${self:provider.stage}.lemoncams.com', // Create the DNS entries manually (A and AAAA Alias to Cloudfront)
            certificate: 'arn:aws:acm:us-east-1:224928506089:certificate/1e52bc1c-dee4-498c-ba4d-8fe87c32ffe3', // Was created manually in aws for *.lemoncams.com
            compress: true, // Set compress to true manually
            cookies: 'none'
        }
    },
    plugins: [
        'serverless-api-cloudfront',
        'serverless-webpack',
        'serverless-offline'],
    provider: {
        name: 'aws',
        runtime: 'nodejs12.x',
        stage: 'dev', // Default is dev, to deploy prod > set to 'prod'
        region: 'us-east-1',
        profile: 'hector',
        deploymentBucket: {
            name: 'hector-api-${self:provider.stage}' // if not exists > need to create manually, need public access
        },
        apiGateway: {
            minimumCompressionSize: 1024,
        },
        environment: {
            AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
            DEV_URL: 'http://d2keht4nzd8oxe.cloudfront.net',
            PROD_URL: 'https://www.lemoncams.com',
            DEV_MAX_INVALID_IN_MINUTES: '1440',
            PROD_MAX_INVALID_IN_MINUTES: '5',
            CAMS_BUCKET: 'hector-api-cams-${self:provider.stage}', // if not exists > need to create manually and lambda role needs S3 access
            STAGE: '${self:provider.stage}',
        }
    },
    functions: {
        main: {
            handler: 'src/handler/main-handler.main',
            name: 'main-handler-v2-${self:provider.stage}',
            description: 'main api function v2',
            timeout: 45,
            events: [
                {
                    http: {
                        method: 'get',
                        path: 'main', //Set dev 512MB Ram and prod 1024MB Ram
                    }
                },
                {
                    schedule: {
                        name: 'warmup-chaturbate-${self:provider.stage}',
                        input: {
                            name: 'warmup-chaturbate'
                        },
                        rate: 'rate(1 minute)' // on dev 20 minutes > on prod 1 minute
                    }
                },
                {
                    schedule: {
                        name: 'warmup-bongacams-${self:provider.stage}',
                        input: {
                            name: 'warmup-bongacams'
                        },
                        rate: 'rate(1 minute)' // on dev 20 minutes > on prod 1 minute
                    }
                },
                {
                    schedule: {
                        name: 'warmup-awempire-${self:provider.stage}',
                        input: {
                            name: 'warmup-awempire'
                        },
                        rate: 'rate(1 minute)' // on dev 20 minutes > on prod 1 minute
                    }
                },
                {
                    schedule: {
                        name: 'warmup-camsoda-${self:provider.stage}',
                        input: {
                            name: 'warmup-camsoda'
                        },
                        rate: 'rate(1 minute)' // on dev 20 minutes > on prod 1 minute
                    }
                },
                {
                    schedule: {
                        name: 'warmup-stripchat-${self:provider.stage}',
                        input: {
                            name: 'warmup-stripchat'
                        },
                        rate: 'rate(1 minute)' // on dev 20 minutes > on prod 1 minute
                    }
                },
                {
                    schedule: {
                        name: 'warmup-all-${self:provider.stage}',
                        input: {
                            name: 'warmup-all'
                        },
                        rate: 'rate(1 minute)' // on dev 20 minutes > on prod 1 minute
                    }
                },
            ]
        }
    }
}

module.exports = serverlessConfiguration;
