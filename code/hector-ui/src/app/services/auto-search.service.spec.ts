import { TestBed } from '@angular/core/testing';

import { AutoSearchService } from './auto-search.service';

describe('AutoSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutoSearchService = TestBed.get(AutoSearchService);
    expect(service).toBeTruthy();
  });
});
