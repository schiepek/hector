set S3BUCKET=%1
set CLOUDFRONT_DISTRIBUTION_ID=%2

echo 'upload data.json to %S3BUCKET%'
call aws s3 cp ../../src/assets/data/data.json %S3BUCKET%/assets/data/data.json --profile hector --acl public-read --cache-control max-age=2592000,public
call aws cloudfront create-invalidation --profile hector --distribution-id %CLOUDFRONT_DISTRIBUTION_ID% --paths /assets/data/*

echo 'upload done'

pause
