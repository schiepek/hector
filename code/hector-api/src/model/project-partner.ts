import {PagePartner} from './page-partner';

export class ProjectPartner {
    project: string;
    pagePartners: PagePartner[] = [];

    constructor(project: string) {
        this.project = project;
    }
}