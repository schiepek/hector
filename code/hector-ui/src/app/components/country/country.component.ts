import {Component, OnInit} from '@angular/core';
import {StateService} from '../../services/state.service';
import {WebcamService} from '../../services/webcam.service';
import {CountryService} from '../../services/country.service';
import {ScreenService} from '../../services/screen.service';
import {MetaService} from '../../services/meta.service';
import {LinkService} from '../../services/link.service';
import {ScriptLoaderService} from '../../services/script-loader.service';
import {ClientUtil} from '../../shared/util/client-util';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

//the style css is include in the angular project
const jvectormapJs = "./assets/js_hector/jquery-jvectormap-2.0.3.min.js";
const jvectormapWorldMillJs = "./assets/js_hector/jquery-jvectormap-world-mill-en.js";

declare var $: any;

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css',]
})
export class CountryComponent implements OnInit {

  private vectorMap: any;

  constructor(private webcam: WebcamService,
              private meta: MetaService,
              private link: LinkService,
              private scriptLoader: ScriptLoaderService,
              private router: Router,
              private location: Location,
              private route: ActivatedRoute,
              public countryS: CountryService,
              public screen: ScreenService,
              public state: StateService) {
    this.meta.setCountriesMetaTags();
  }

  ngOnInit() {
    this.state.reset();
    this.link.setCanonicalUrl();
    this.webcam.getMenu().then(res => {
      this.countryS.initCountryGridPage(res.countries);
      this.countryS.initContinentsMap();
      this.initWorldMapComplete();
    });
    this.state.getTracker(this.route, this.location);
  }

  private initWorldMapComplete(): void {
    if (ClientUtil.isClientSide()) {
      this.scriptLoader.load(jvectormapJs, jvectormapWorldMillJs).then(() => {
        this.initWorldMap();
      })
    }
  }

  private initWorldMap(): void {

    this.vectorMap = $('#world-map').vectorMap({
      map: 'world_mill_en',
      series: {
        regions: [{
          values: this.transformCamData(),
          scale: ['#aff751', '#273513'],
          normalizeFunction: 'polynomial'
        }]
      },
      backgroundColor: "transparent",
      onRegionTipShow: (e, el, code) => {
        let countryUI = this.countryS.allCountries.find(cUI => cUI.code === code.toLowerCase());
        (countryUI) ? el.html(el.html() + ' (' + countryUI.size + ' cams)') : null;
      },
      onRegionClick: (e, code) => {
        let countryUI = this.countryS.allCountries.find(cUI => cUI.code === code.toLowerCase());
        if (countryUI) {
          (countryUI) ? this.router.navigate(['/' + countryUI.link]) : null;
          // the tooltips of the selected country stay and have to be removed
          this.vectorMap.remove();
        }
      }
    });

    this.vectorMap = $('#world-map .jvectormap-container').data('mapObject');

  }

  private transformCamData(): any {
    let obj = {};
    this.countryS.countriesMap.forEach(cUI => obj[cUI.code.toUpperCase()] = cUI.size);
    return obj;
  }


  public onKey(event: any): void {
    this.countryS.filterCountries(event.target.value);
    if (ClientUtil.isClientSide()) {
      this.vectorMap.remove();
      this.initWorldMap();
    }
  }

}
