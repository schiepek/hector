export default
{
    "slim": [
        "skinny",
        "slim",
        "athletic",
        "thin",
        "flaca",
        "flaco",
        "delgada",
        "delgado",
        "bodyTypeThin",
        "bodyTypeAthletic"
    ],
    "petite": [
        "petite"
    ],
    "fat": [
        "fat",
        "plump",
        "chubby",
        "gordo",
        "gorda",
        "obese",
        "curvy",
        "bodyTypeCurvy",
        "bodyTypeLarge"
    ]
}
