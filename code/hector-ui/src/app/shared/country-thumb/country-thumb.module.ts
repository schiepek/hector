import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryThumbComponent } from '../../components/country/country-thumb/country-thumb.component';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild()
  ],
  declarations: [
    CountryThumbComponent
  ],
  exports: [
    CountryThumbComponent
  ]
})
export class CountryThumbModule { }
