import {CamFunction} from './cam-function';
import {StringUtil} from '../util/string-util';

export class SearchFunction extends CamFunction {

    constructor(queryString: any) {
        super(queryString,
            (cams) => {
                return cams.filter(c => {
                    let contents: string[] = [];
                    contents.push(
                        c.username,
                        c.title,
                        c.tags.toString(),
                        c.turnsOn,
                        c.turnsOff,
                        c.hairColor,
                        c.location,
                        c.languages,
                        c.categories.toString()
                    );
                    return contents.some(co => StringUtil.containsCaseIns(co, queryString))
                })
            });
    }

}
