import tagsList from '../resources/tags.js'
import {CategoryItem} from '../model/category-item';
import {Cam} from '../model/cam';
import {CategoryItemType} from '../model/category-item-type';

export class TagEvaluator {

    private static readonly TAGS_LIST: string[] = tagsList["tags"];

    public static executeForCam(camTags: string[], title?: string) {
        if(title) {
            let strings = title.split(/[ ,]+/);
            if(strings) {
                let extractedTags = strings.map(str=>str.replace(/[^a-zA-Z]/g, ""))
                    .map(cleanedStr=>cleanedStr.toLowerCase())
                    .filter(possibleTag => TagEvaluator.isOfficialTag(possibleTag));

                camTags.push(...extractedTags);
                return camTags.filter((x, i, a) => a.indexOf(x) === i)
            }
        }
        return camTags;
    }

    private static isOfficialTag(possibleTag: string) : boolean {
        return TagEvaluator.TAGS_LIST.includes(possibleTag)
    }

    public static executeForCams(cams: Cam[]) : CategoryItem[] {
        const itemMap : Map<string, CategoryItem> = new Map();

        for(let cam of cams) {
            let specificFilters = this.collectSpecificFilters(cam);
            cam.categories.forEach(c=>this.addOrIncreaseItem(itemMap, c, CategoryItemType.CATEGORY, specificFilters));
            cam.tags.filter(t=>TagEvaluator.isOfficialTag(t))
                .forEach(tag=>this.addOrIncreaseItem(itemMap, tag, CategoryItemType.TAG, specificFilters))
            cam.country ? this.addOrIncreaseItem(itemMap, cam.country, CategoryItemType.COUNTRY, specificFilters): null;
        }

        return Array.from(itemMap.values())
            .sort((a,b) => a.code.localeCompare(b.code));
    }

    private static addOrIncreaseItem(itemMap: Map<string, CategoryItem>, code: string, type: CategoryItemType, specificFilters: string[]) {
        let item = itemMap.get(code);
        if (item) {
            item.increaseSize(specificFilters);
        } else {
            itemMap.set(code, CategoryItem.initCategoryItemToSave(code, type, specificFilters));
        }
    }

    private static collectSpecificFilters(cam: Cam) : string[] {
        let specificFilters: string[] = [];
        cam.gender ? specificFilters.push(cam.gender) : null;
        return specificFilters;
    }
}
