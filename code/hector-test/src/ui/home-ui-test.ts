import {AbstractTest} from '../backend/abstract-test';
import {assert} from 'chai';
import {TestType} from '../general/test-type';
import {parse} from 'node-html-parser';
import {HectorConfig} from '../../../hector-api/src/util/hector-config';


export class HomeUiTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.UI;
    }

    protected initTests(): void {

        this.tests.set("home-ui", (executor) => {

            let url = executor.config.getUiRenderUrlProd();

            executor.executeRestTest(url, 'home should have valid content', res => {
                assert.equal(res.status, 200, url + ' does not return status 200');

                HomeUiTest.assertTopRow(res.text);
                HomeUiTest.assertNumberOfUsersAndCountries(res.text);
                HomeUiTest.assertAllCountryCamsAreLoaded(res.text);
            })
        });


    }

    private static assertTopRow(html: string): void {
        let doc = parse(html);

        let topRow = doc.querySelector(".posts");
        assert.equal(3, topRow.childNodes.length);

        let containsChaturbate: boolean = false;
        let containsStripchat: boolean = false;

        topRow.childNodes.forEach(node => {
            assert.isNotEmpty(parse(node.toString()).querySelector(".footer-username").innerText,
                "title of cam should be set");

            containsChaturbate = (node.toString().includes("chaturbate_thumb.png"));
            containsStripchat = (node.toString().includes("stripchat_thumb.png"));
        })

        assert.isTrue(containsChaturbate || containsStripchat, "the first 3 cams should contain at least one chaturbate or stripchat cam");
    }

    private static assertNumberOfUsersAndCountries(html: string): void {
        let doc = parse(html);
        let numberOfUsers = doc.querySelectorAll(".general-highlight")[0].innerText;
        let numberOfCountries = doc.querySelectorAll(".general-highlight")[1].innerText;
        assert.isTrue(+numberOfUsers > 6000, "number of models online should be higher than 6000");
        assert.isTrue(+numberOfCountries > 60, "number of countries should be higher than 60");
    }

    private static assertAllCountryCamsAreLoaded(html: string): void {
        let doc = parse(html);
        let countryRow = doc.querySelectorAll(".posts")[1];
        assert.equal(HectorConfig.getPageSize(), countryRow.childNodes.length)
    }

}
