// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  project: "lemoncams",
  apiUrl: "https://api-v2-dev.lemoncams.com",
  apiUrl2: "http://localhost:3000",
  domain: "www.lemoncams.com",
  website: "Lemoncams",
  analytics: "UA-120903295-1",
  showCategories: true,
  showCountries: true,
  isSubSite: false,
  refreshInterval: 60,
  devMode: false
};
