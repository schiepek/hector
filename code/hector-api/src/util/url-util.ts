export class UrlUtil {

    public static readonly defaultUrl: string = "https://www.lemoncams.com";

    public static getUrl(): string {
        let url = (process.env.STAGE === "dev") ? process.env.DEV_URL : process.env.PROD_URL;
        return url ? url : UrlUtil.defaultUrl;
    }

}
