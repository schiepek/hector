import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {DetailComponent} from '../../components/detail/detail.component';
import {DetailRelatedComponent} from '../../components/detail/detail-related/detail-related.component';
import {DetailTagComponent} from '../../components/detail/detail-tag/detail-tag.component';
import {DetailModelComponent} from '../../components/detail/detail-model/detail-model.component';
import {TranslateModule} from '@ngx-translate/core';
import {WebcamThumbModule} from '../../shared/webcam-thumb/webcam-thumb.module';
import {AdModule} from '../../shared/ad/ad.module';
import {PipeModule} from '../../shared/pipe/pipe.module';
import {WebcamTagcloudModule} from '../../shared/webcam-tagcloud/webcam-tagcloud.module';
import {CountryThumbModule} from '../../shared/country-thumb/country-thumb.module';
import {ModelThumbModule} from '../../shared/model-thumb/model-thumb.module';
import {DetailTitleComponent} from '../../components/detail/detail-title/detail-title.component';
import {RouletteThumbModule} from '../../shared/roulette-thumb/roulette-thumb.module';

const routes: Routes = [
  {path: '', component: DetailComponent, pathMatch: 'full'}
];


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        TranslateModule.forChild(),
        WebcamThumbModule,
        WebcamTagcloudModule,
        CountryThumbModule,
        ModelThumbModule,
        AdModule,
        PipeModule,
        RouletteThumbModule
    ],
  declarations: [
    DetailComponent,
    DetailRelatedComponent,
    DetailTagComponent,
    DetailModelComponent,
    DetailTitleComponent,
  ]
})
export class DetailModule {
}
