import {Injectable} from '@angular/core';
import {HectorConfig} from '../../../../hector-api/src/util/hector-config';
import {provider} from '../../../../hector-api/src/model/provider';
import {environment} from '../../environments/environment';


@Injectable({providedIn: 'root'})
export class ConstantService {

  public readonly TWITTER_LINK: string = "https://twitter.com/Lemoncams_com";
  public readonly NEW_LINE: string = "\n";

  public readonly FACBEOOK_SHARE: string = "https://www.facebook.com/sharer/sharer.php?u=";
  public readonly TWITTER_SHARE: string = "https://twitter.com/share?url=";
  public readonly REDDIT_SHARE: string = "https://www.reddit.com/submit?url=";
  public readonly TUMBLR_SHARE: string = "http://www.tumblr.com/share/link?url=";
  public readonly WHATSAPP_SHARE: string = "https://wa.me/?text=";

  private readonly redirect = "/recommends/";

  public readonly chaturbateSignupUrl: string = this.redirect + "chaturbate-signup";
  public readonly bongaCamsSignupUrl: string = this.redirect + "bongacams-signup";
  public readonly awempireSignupUrl: string = this.redirect + "livejasmin-signup";
  public readonly camsodaSignupUrl: string = this.redirect + "camsoda-signup";
  public readonly stripchatSignupUrl: string = this.redirect + "stripchat-signup";

  public readonly chaturbateModelSignupUrl: string = this.redirect + "chaturbate-model-signup";
  public readonly bongacamsModelSignupUrl: string = this.redirect + "bongacams-model-signup";
  public readonly awempireModelSignupUrl: string = this.redirect + "livejasmin-model-signup";
  public readonly camsodaModelSignupUrl: string = this.redirect + "camsoda-model-signup";
  public readonly stripchatModelSignupUrl: string = this.redirect + "stripchat-model-signup";

  /** Routes **/
  public readonly CAMS: string = "cams";
  public readonly SEARCH: string = "search";
  public readonly WORLD_MAP: string = "world-map-of-sex-cams";

  private subdomain: string;
  private canonicalOrigin: string;

  constructor() {
    let lowerCaseSubdomain = this.getDomain().replace("www.", "");
    this.subdomain = lowerCaseSubdomain.charAt(0).toUpperCase() + lowerCaseSubdomain.slice(1);
    this.canonicalOrigin = "https://" + this.getDomain();
  }

  public hectorConfig(): any {
    return HectorConfig;
  }

  public getSignupUrl(providerStr: string, tracker?: string): string {
    switch (providerStr) {
      case provider.CHATURBATE:
        return this.chaturbateSignupUrl;
      case provider.BONGACAMS:
        return this.bongaCamsSignupUrl;
      case provider.AWEMPIRE:
        return this.awempireSignupUrl;
      case provider.CAMSODA:
        return this.camsodaSignupUrl;
      case provider.STRIPCHAT:
        return this.stripchatSignupUrl;
      default :
        return "";
    }
  }

  public getModelSignupUrl(providerStr: string, tracker?: string): string {
    switch (providerStr) {
      case provider.CHATURBATE:
        return this.chaturbateModelSignupUrl;
      case provider.BONGACAMS:
        return this.bongacamsModelSignupUrl;
      case provider.AWEMPIRE:
        return this.awempireModelSignupUrl;
      case provider.CAMSODA:
        return this.camsodaModelSignupUrl;
      case provider.STRIPCHAT:
        return this.stripchatModelSignupUrl;
      default :
        return "";
    }
  }

  public getWebsite(): string {
    return environment.website;
  }

  public getDomain(): string {
    return environment.domain;
  }

  public getSubDomain(): string {
    return this.subdomain;
  }

  public getCanonicalOrigin() : string {
    return this.canonicalOrigin;
  }

  public getProject(): string {
    return environment.project;
  }

  public isSubSite(): boolean {
    return environment.isSubSite;
  }

  public showCountries(): boolean {
    return environment.showCountries;
  }

  public showCategories(): boolean {
    return environment.showCategories;
  }

  public getTrackerAddon(tracker: string): string {
    return tracker ? this.getWebsite() + "_" + tracker : this.getWebsite();
  }

}
