import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebcamThumbComponent } from './webcam-thumb.component';

describe('WebcamThumbComponent', () => {
  let component: WebcamThumbComponent;
  let fixture: ComponentFixture<WebcamThumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebcamThumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebcamThumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
