import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-detail-tag',
  templateUrl: './detail-tag.component.html',
  styleUrls: ['./detail-tag.component.css']
})
export class DetailTagComponent {

  @Input()
  public show: boolean;

  @Input()
  public newWindow: boolean;

  @Input()
  public identifier: string;

  @Input()
  public value: string;

  @Input()
  public link: string;

  @Input()
  public links: Map<string,string>;

  constructor() { }



}
