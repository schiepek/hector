import {UrlUtil} from './url-util';
import {Partner} from '../model/partner';
import {CamResult} from '../model/cam-result';
import {ProjectPartner} from '../model/project-partner';
import {PagePartner} from '../model/page-partner';

let rp = require('request-promise');

export class DataUtil {

    private static readonly DATA_JSON: string = "/assets/data/data.json";
    private static readonly TIMEOUT: number = 10000;

    public static getData(): Promise<any> {
        return rp({uri: UrlUtil.getUrl() + DataUtil.DATA_JSON, json: true, timeout: DataUtil.TIMEOUT});
    }

    public static reducePartners(partnerData: Partner[]): ProjectPartner[] {

        let projectPartners: ProjectPartner[] = [];

        for (let partner of partnerData) {
            let existingProjectPartner = projectPartners.find(pp => pp.project === partner.project);
            let projectPartner = existingProjectPartner ? existingProjectPartner : new ProjectPartner(partner.project);
            !existingProjectPartner ? projectPartners.push(projectPartner) : null;

            let existingPagePartner = projectPartner.pagePartners.find(pp => pp.page === partner.page);
            let pagePartner = existingPagePartner ? existingPagePartner : new PagePartner(partner.page);
            !existingPagePartner ? projectPartner.pagePartners.push(pagePartner) : null;

            pagePartner.partners.push(partner);
        }

        return projectPartners;
    }

    public static getPartners(result: CamResult, event: any): Partner[] {
        const path = event.queryStringParameters["path"];
        const project = event.queryStringParameters["project"];

        if (result.partners && path && project) {
            let projectPartner = result.projectPartners.find(pp => pp.project === project);
            let pagePartner = projectPartner ? projectPartner.pagePartners.find(pp => pp.page === path) : null;
            return pagePartner ? pagePartner.partners : [];
        }
        return [];
    }

}
