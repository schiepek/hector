import {Handler, Context, Callback} from 'aws-lambda';
import {BotRecognizer} from '../util/bot-recognizer';
import {LinkUtil} from '../util/link-util';
import {DomainUtil} from '../util/domain-util';

let rp = require('request-promise');
let zlib = require('zlib');

// Define HERE the URI where were we collect our redirection links
let linksUri = "https://www.lemoncams.com/assets/links/links.json";

const execute: Handler = (event: any, context: Context, callback: Callback) => {

    let request = event.Records[0].cf.request;

    // redirect if its a link
    if (request.uri.startsWith('/recommends/')) {
        rp(linksUri).then((response: any) => {
            let link = LinkUtil.getLink(request, response);
            if (link) {
                let resp = {
                    status: '302',
                    statusDescription: 'Found',
                    headers: {
                        location: [{
                            key: 'Location',
                            value: link,
                        }],
                    },
                };
                callback(null, resp);
            }
        });
    }

    // prerender if it's a bot or not found
    else if (BotRecognizer.hasToBePrerendered(request)) {
        let options = {
            method: 'GET',
            uri: DomainUtil.extractDomain(request) + request.uri,
            resolveWithFullResponse: true
        };

        rp(options).then((response: any) => {

            let buffer = zlib.gzipSync(response.body);
            let base64EncodedBody = buffer.toString('base64');
            let contentType = response.headers['content-type'];
            let status = request.uri.startsWith('/not-found') ? '404' : '200';

            // Generate the final response and call the callback
            let prerenderResponse = {
                status: status,
                statusDescription: 'OK',
                headers: {
                    'content-type': [{
                        key: 'Content-Type',
                        value: contentType,
                    }],
                    'access-control-allow-origin': [{
                        key: 'Access-Control-Allow-Origin',
                        value: "*"
                    }],
                    'content-encoding': [{
                        key: 'Content-Encoding',
                        value: 'gzip'
                    }]
                },
                body: base64EncodedBody,
                bodyEncoding: 'base64',
            };
            callback(null, prerenderResponse);
        });
    }

    // normal callback
    else {
        callback(undefined, request);
    }
};

export {execute}
