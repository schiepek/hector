import {Router} from '@angular/router';
export class RouterUtil {

  public static goToEmergency(router: Router) {
    router.navigate(["/cams2"]);
  }

  public static goToHome(router: Router) {
    router.navigate(["/"]);
  }

  public static goTo404(router: Router) {
    router.navigate(["/not-found"]);
  }

  public static goToSearch(router: Router, searchTerm: string) {
    router.navigate(["/search/" + searchTerm])
  }

}
