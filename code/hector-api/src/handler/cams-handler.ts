import {Callback, Handler} from 'aws-lambda';
import {CamResult} from '../model/cam-result';
import {CamsFunction} from '../function/cams-function';
import {ResponseUtil} from '../util/response-util';
import {DetailFunction} from '../function/detail-function';
import {DataUtil} from '../util/data-util';
import {AutocompleteFunction} from '../function/autocomplete-function';
import {UndetectedCountryFunction} from '../function/undetected-country-function';
import {AllLoader} from '../loader/all-loader';
import {RandomFunction} from '../function/random-function';
import {CategoriesFunction} from '../function/categories-function';

const allLoader = AllLoader.getInstance();

const handleCams: Handler = (func: string, event: any, callback: Callback) => {
    allLoader.loadIfNotInMemory(func === "expired").then(result => {
        handleFunction(func, event, callback, result);
    }).catch(error => {
        console.error("load if not in memory failed");
        console.error(error);
        handleFunction(func, event, callback, allLoader.loadResultDirectly())
    });
}

function handleFunction(func: string, event: any, callback: Callback, result: CamResult) {
    func = (result.cams.length > 0) ? func : "error";

    switch (func) {
        case "cams":
            let updatedCams = new CamsFunction(event.queryStringParameters).updateResult(result, event);
            callback(undefined, ResponseUtil.json(updatedCams));
            break;
        case "cam":
            let updatedCam = new DetailFunction(event.queryStringParameters).updateResult(result, event);
            callback(undefined, ResponseUtil.json(updatedCam));
            break;
        case "home":
            let homeResult = new CamResult([], result.countries, result.cams.length, 0, result.home, undefined, DataUtil.getPartners(result, event), result.date, result.meta);
            callback(undefined, ResponseUtil.json(homeResult));
            break;
        case "categories":
            let categoriesResult = new CategoriesFunction(event.queryStringParameters).updateResult(result, event);
            callback(undefined, ResponseUtil.json(categoriesResult));
            break;
        case "menu":
            let updatedMenu = new CamResult([], result.countries, 0, 0, undefined, undefined, DataUtil.getPartners(result, event), result.date, result.meta);
            callback(undefined, ResponseUtil.json(updatedMenu));
            break;
        case "random":
            let randomCam = new RandomFunction(event.queryStringParameters).updateResult(result, event);
            callback(undefined, ResponseUtil.json(randomCam));
            break;
        case "autocomplete":
            let autoCompleteResult = new AutocompleteFunction(event.queryStringParameters).updateResult(result);
            callback(undefined, ResponseUtil.json(autoCompleteResult));
            break;
        case "expired":
            callback(undefined, ResponseUtil.json({"expired": result.cams.length + " loaded successfully"}));
            break;
        case "undetectedcountries":
            let undetectedCountries = new UndetectedCountryFunction().getUndetectedCountries(result.cams);
            callback(undefined, ResponseUtil.json(undetectedCountries));
            break;
        case "error":
            let errorResult = new CamResult([], []);
            errorResult.error = true;
            callback(undefined, ResponseUtil.json(errorResult));
            break;
        default: {
            callback(undefined, ResponseUtil.notFound());
            break;
        }
    }
}

export {handleCams}
