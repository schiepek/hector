import {ChangeDetectorRef, Injectable} from '@angular/core';
import {Partner} from '../../../../hector-api/src/model/partner';
import {ComponentReloader} from '../shared/util/component-reloader';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  private readonly top_type: string = "top";
  private readonly list_type: string = "list";
  private cdRef: ChangeDetectorRef;

  public tops: Partner[];
  public lists: Partner[];

  public initPartners(partners: Partner[]): void {
    this.tops = partners.filter(p => p.type === this.top_type);
    this.lists = partners.filter(p => p.type === this.list_type);
    ComponentReloader.detectChanges(this.cdRef);
  }

  public initNavigationChangeDetector(cdRef: ChangeDetectorRef) : void {
    this.cdRef = cdRef;
  }


}
