import {AllPagination, Pagination} from '../model/pagination';
import {UrlSegment} from '@angular/router';
export class PaginationUtil {

  /**
   * Pagination like amazon
   *
   * @param page the current page
   * @param maxPage maximum pages
   * @param segment the current url segments
   * @returns {Pagination[]}
   */
  public static getPaginations(page: number, maxPage: number, segment: UrlSegment[]): AllPagination {
    if (maxPage < 6) {
      return new AllPagination(this.getPaginationForLowNumbers(page, maxPage), this.getBasicUrl(segment));
    }
    return new AllPagination(this.getPaginationForHighNumbers(page, maxPage), this.getBasicUrl(segment));
  }

  /**
   * Get the basic url without any page parameter
   */
  private static getBasicUrl(segment: UrlSegment[]): string {
    return segment.filter(s=>isNaN(Number(s.path))).join('/');
  }

  private static getPaginationForLowNumbers(page: number, maxPage: number): Pagination[] {
    let paginations: Pagination[] = [];

    for (let i = 1; i <= 5; i++) {
      (i <= maxPage) ? paginations.push(new Pagination(i, true, page)) : null;
    }
    return paginations;
  }

  private static getPaginationForHighNumbers(page: number, maxPage: number): Pagination[] {
    let paginations: Pagination[] = [];

    paginations.push(new Pagination(1, true, page));

    if (page < 4) {
      paginations.push(new Pagination(2, true, page));
      paginations.push(new Pagination(3, true, page));
      if (page === 3) {
        paginations.push(new Pagination(4, true, page));
      }
      paginations.push(new Pagination(0, false, page));
    }

    if (page >= 4 && page <= (maxPage - 3)) {
      paginations.push(new Pagination(0, false, page));
      paginations.push(new Pagination(page - 1, true, page));
      paginations.push(new Pagination(page, true, page));
      paginations.push(new Pagination(page + 1, true, page));
      paginations.push(new Pagination(0, false, page));
    }

    if (page > maxPage - 3) {
      paginations.push(new Pagination(0, false, page));
      if (page === (maxPage - 2)) {
        paginations.push(new Pagination(maxPage - 3, true, page))
      }
      paginations.push(new Pagination(maxPage - 2, true, page));
      paginations.push(new Pagination(maxPage - 1, true, page));
    }
    paginations.push(new Pagination(maxPage, true, page));


    return paginations;
  }

}
