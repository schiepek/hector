import {BlogEntry} from './blog-entry';

export class BlogEntryPage {
  entries: BlogEntry[];
  maxPage: number;
}
