export class FooterText {

  public id: string;
  public title: string;
  public descriptions: string[];
  public features: string[];
  public showIcobox: boolean;

}
