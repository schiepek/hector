import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {TestType} from '../general/test-type';
import {provider} from '../../../hector-api/src/model/provider';


export class RandomTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }


    protected initTests(): void {

        this.tests.set("random-cam", (executor, config) => {
            executor.executeRestTestFunction(config.getRandomFunction(), 'should a random cam without specific usernames', {loaded: "cam_model1,cam_model2"},
                r => {
                    assert.equal(r.cams.length, 10, "there should be only one random cam and 9 related cams");
                    assert.isTrue(r.countries.length > 0);
                    assert.isTrue(r.countries[0].size > 500);
                    assert.isFalse(r.cams[0].provider === provider.AWEMPIRE);
                }
            );
        });

        this.tests.set("random-cam-gender", (executor, config) => {
            executor.executeRestTestFunction(config.getRandomFunction(), 'should a random cam with shemale', {gender: "shemale"},
                r => {
                    assert.equal(r.cams.length, 10, "there should be only one random cam and 9 related cams");
                    assert.isTrue(r.cams[0].gender === "shemale");
                    assert.isTrue(r.countries.length > 0);
                    assert.isTrue(r.countries[0].size > 500);
                    assert.isFalse(r.cams[0].provider === provider.AWEMPIRE);
                }
            );
        });

    }

}
