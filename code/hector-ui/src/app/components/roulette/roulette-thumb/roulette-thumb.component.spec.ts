import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouletteThumbComponent } from './roulette-thumb.component';

describe('RouletteThumbComponent', () => {
  let component: RouletteThumbComponent;
  let fixture: ComponentFixture<RouletteThumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouletteThumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouletteThumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
