import {StringUtil} from '../../../hector-api/src/util/string-util';
import {Cam} from '../../../hector-api/src/model/cam';
export class TestUtil {

    private static readonly maxNumber: number = 999999999;

    public static isPositiveNumber(num: any): boolean {
        let isNumber = !isNaN(num);
        let isPositiveNumber = num >= 0 && num <= this.maxNumber;
        return isNumber && isPositiveNumber;
    }

    public static isBoolean(bool: any): boolean {
        return bool.toString() === "false" || bool.toString() === "true";
    }

    public static isIframe(iframe: string): boolean {
        return this.isUrl(StringUtil.extractEmbedUrl(iframe));

    }

    public static isUrl(url: string): boolean {
        return !!url.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    };

    public static isImageUrl(url: string): boolean {
        return this.isUrl(url) && url.match(/\.(jpeg|jpg|gif|png)\.*/) != null;
    }

    public static isStringOrNull(str: any): boolean {
        return str === null || this.isString(str);
    }

    public static isString(str: any): boolean {
        return typeof str === "string";
    }

    public static isStringNotEmpty(str: any): boolean {
        return this.isString(str) && str !== "";
    }

    public static isArrayNotEmpty(arr: any): boolean {
        return this.isArray(arr) && arr.length > 0
    }

    public static isArray(arr: any): boolean {
        return arr instanceof Array;
    }

    public static isObject(obj: any): boolean {
        return obj === Object(obj);
    }

    public static isOneOfStrings(strs: string[], str): boolean {
        return strs.some(s => s === str);
    }

    public static isOneOfStringsOrNull(strs: string[], str): boolean {
        return strs && str ? strs.some(s => s === str) : true;
    }

    public static strContainsOneOfStrings(str, strs: string[]): boolean {
        return strs.some(s => StringUtil.containsCaseIns(str, s));
    }

    public static strContainsOneOfStringsOrNull(str, strs: string[]): boolean {
        return str ? strs.some(s => StringUtil.containsCaseIns(str, s)) : true;
    }

    public static arrayContainsOneOfStringsOrNull(strs: string[], values: string[],): boolean {
        return values ? values.every(str => strs.indexOf(str) > -1) : true;
    }

    public static isDate(date: any): boolean {
        return isNaN(date) && !isNaN(Date.parse(date));
    }

    public static isDateOrNull(date: any): boolean {
        return date ? this.isDate(date) : true;
    }

    public static isSortedByNumberOfUsers(cams: Cam[]): boolean {
        let numberOfUsers = 9999999;

        for (let cam of cams) {
            if (numberOfUsers < cam.numberOfUsers) {
                return false;
            }
            numberOfUsers = cam.numberOfUsers;
        }
        return true;
    }

    public static eachCountryIsDifferent(cams: Cam[]): boolean {
        return new Set(cams.map(c => c.country)).size === cams.length;
    }

    public static isHttpsUrl(url: string) {
        if (url && url.startsWith("http")) {
            return url.startsWith("https")
        }
        return true;
    }

}
