# HECTOR API #

## How to deploy
* sls deploy
* sls --stage prod

## Search for string in Cloudwatch insights
fields @timestamp, @message
| filter @message like /your text to search/
| sort @timestamp desc
| limit 20
