import {Injectable} from '@angular/core';
import tagsJson from '../../../../hector-api/src/resources/tags.js';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  public getTag(tag: string): string {
    return tagsJson["tags"].includes(tag) ? tag : undefined;
  }

  public tagExists(tag: string): boolean {
    return !!this.getTag(tag);
  }

  public tagsThatInclude(searchTerm: string): string[] {
    return this.getAllTags()
      .filter(t=>t.includes(searchTerm));
  }

  public findFirstTag(tags: string[]): string | undefined {
    return tags ? tags.find(t => this.tagExists(t)) : undefined;
  }

  public getAllTags(): string[] {
    return tagsJson["tags"];
  }

}
