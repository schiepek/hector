import 'mocha';
import chai = require('chai');
import {Cam} from '../../../hector-api/src/model/cam';
import {TestConfig} from './test-config';
import {assert} from 'chai';
import {CamResult} from '../../../hector-api/src/model/cam-result';
import {provider} from '../../../hector-api/src/model/provider';

export class TestExecutor {

    private static readonly TIMEOUT: number = 60000;
    public config: TestConfig;

    constructor(config: TestConfig) {
        this.config = config;
    }

    public executeRestTestCams(description: string, parameters: any, callback: (result: CamResult) => void): void {
        it(description, (done) => {
            parameters["function"] = this.config.getCamsFunction();
            chai.request(this.config.getEndpoint()).get(this.config.getMainPath()).query(parameters)
                .end((err, res) => {
                    let result = JSON.parse(res.text);
                    let cams = result["cams"];
                    assert.isTrue(cams && cams.length > 0, "cams should be filled, but it's empty!");
                    this.assertCams(cams);
                    callback(result);
                    done();
                });
        }).timeout(TestExecutor.TIMEOUT);
    }

    public executeRestTestFunction(functionName: string, description: string, parameters: any, callback: (result: any) => void): void {
        it(description, (done) => {
            parameters["function"] = functionName;
            chai.request(this.config.getEndpoint()).get(this.config.getMainPath()).query(parameters)
                .end((err, res) => {
                    let result = JSON.parse(res.text);
                    callback(result);
                    done();
                });
        }).timeout(TestExecutor.TIMEOUT);
    }

    public executeRestTestFunctionWithSecondInvocation(functionName: string, description: string, parameters: any, callback: (result: any, done: any) => void): void {
        it(description, (done) => {
            parameters["function"] = functionName;
            chai.request(this.config.getEndpoint()).get(this.config.getMainPath()).query(parameters)
                .end((err, res) => {
                    let result = JSON.parse(res.text);
                    callback(result, done);
                });
        }).timeout(TestExecutor.TIMEOUT);
    }

    public executeRestJsonTest(url: string, description: string, callback: (result: any) => void): void {
        it(description, (done) => {
            chai.request(url).get("")
                .end((err, res) => {
                    let result = JSON.parse(res.text);
                    callback(result);
                    done();
                });
        }).timeout(TestExecutor.TIMEOUT);
    }

    public executeRestTest(url: string, description: string, callback: (result: any) => void): void {
        it(description, (done) => {
            chai.request(url).get("")
                .end((err, res) => {
                    callback(res);
                    done();
                });
        }).timeout(TestExecutor.TIMEOUT);
    }

    public executeTest(description: string, callback: () => void): void {
        it(description, (done) => {
            callback();
            done();
        }).timeout(TestExecutor.TIMEOUT);
    }

    private assertCams(cams: Cam[]): void {
        cams.forEach(c => {

            // Chaturbate doesn't provide the age always
            if (c.provider !== provider.CHATURBATE) {
                assert.exists(c.age, "age has to be set");
            }

            assert.exists(c.isPrivate, "isPrivate has to be set");
            assert.exists(c.isNew, "isNew has to be set");
            assert.exists(c.gender, "gender has to be set");
            assert.exists(c.isHd, "isHd has to be set");
            assert.exists(c.chatRoomUrl, "chatRoomUrl has to be set");
            assert.exists(c.provider, "provider has to be set");
            this.assertAffiliateLinks(c);
        })
    }

    private assertAffiliateLinks(cam: Cam): void {
        switch (cam.provider) {
            case provider.CHATURBATE:
                assert.isTrue(cam.chatRoomUrl.indexOf('track=default') >= 0, "Chaturbate chatroom url should contain track=default");
                assert.isTrue(cam.embedUrl.indexOf('track=embed') >= 0, "Chaturbate chatroom url should contain track=embed");
                break;
            case provider.BONGACAMS:
                assert.isTrue(cam.chatRoomUrl.indexOf('track?c=') >= 0, "Bongacams chatroom url should contain track?c=");
                assert.isTrue(cam.embedUrl.indexOf('track') >= 0, "Bongacams chatroom url should contain track");
                break;
            case provider.AWEMPIRE:
                assert.isTrue(cam.chatRoomUrl.indexOf('swissTrader2000') >= 0, "Livejasmin chatroom url should contain swissTrader2000");
                break;
            default:
                return;
        }
    }

}
