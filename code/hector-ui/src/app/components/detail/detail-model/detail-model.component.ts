import {Component, Input, OnInit} from '@angular/core';
import {Cam} from '../../../../../../hector-api/src/model/cam';
import {ConstantService} from '../../../services/constant.service';
import {CategoryService} from '../../../services/category.service';
import {CountryService} from '../../../services/country.service';
import {TagService} from '../../../services/tag.service';

@Component({
  selector: 'app-detail-model',
  templateUrl: './detail-model.component.html',
  styleUrls: ['./detail-model.component.css']
})
export class DetailModelComponent implements OnInit{

  @Input()
  public cam: Cam;

  @Input()
  public countryName: string;

  @Input()
  public countryUrl: string;

  public shareTwitter: string;

  public providerLink: string;

  public tagLinks: Map<string, string>;

  public categoryLinks: Map<string, string>;

  public locationLink: string;

  constructor(public constant: ConstantService,
              private country: CountryService,
              private tag: TagService,
              private category: CategoryService) {
  }

  ngOnInit(): void {
    this.shareTwitter = this.constant.TWITTER_SHARE + window.location.href;
    this.providerLink = "/" + this.category.findProviderLink(this.cam.provider);
    this.initCategoryLinks();
    this.initTagLinks();
    this.initLocationLink();
  }

  private initCategoryLinks() : void {
    this.categoryLinks = this.cam.categories ? Object.assign({}, ...this.cam.categories.map(category => ({[category]: category}))) : null;
  }

  private initTagLinks(): void {
    this.tagLinks = this.cam.tags ? Object.assign({}, ...this.cam.tags.map(tag => ({[tag]: this.getTagLink(tag)}))) : null;
  }

  private getTagLink(tag: string) : string {
    let searchableTag = tag.replace("-","").toLowerCase();
    if(this.category.categoryPathExists(searchableTag) || this.tag.tagExists(searchableTag)) {
      return searchableTag;
    }
    return this.constant.SEARCH + "/" + tag;
  }

  private initLocationLink() : void  {

    if(this.countryName && this.cam.location &&
      this.countryName.toLowerCase() === this.cam.location.toLowerCase()) {
      this.locationLink = "/" + this.countryUrl;
    } else {
      this.locationLink = "/" + this.constant.SEARCH + "/" + this.cam.location;
    }
  }

}
