import {StringUtil} from '../../../hector-api/src/util/string-util';
import {assert} from 'chai';
import {HectorConfig} from '../../../hector-api/src/util/hector-config';
import {AbstractTest} from './abstract-test';
import {provider} from '../../../hector-api/src/model/provider';
import {TestUtil} from '../util/test-util';
import {TestType} from '../general/test-type';


export class PropertyTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    private readonly maxNumber: number = 999999999;

    protected initTests(): void {

        this.tests.set("property-page", (executor) => {
            executor.executeRestTestCams('should return the cams from page 0', {page: 1},
                r => {
                    assert.equal(r.cams.length, HectorConfig.getPageSize(), "page size doesn't match");
                    assert.isFalse(TestUtil.isSortedByNumberOfUsers(r.cams), "cams shouldn't be sorted by users > is balanced out");
                }
            );
        });

        this.tests.set("property-maxage", (executor) => {
            executor.executeRestTestCams('should return the cams with a maximum age', {page: 1, maxage: 50},
                r => r.cams.forEach(c => assert.isTrue(c.age <= 50), "age has to be less than 50")
            );
        });

        this.tests.set("property-minage", (executor) => {

            executor.executeRestTestCams('should return the cams with a minimum age', {page: 1, minage: 50},
                r => r.cams.forEach(c => assert.isTrue(c.age >= 50), "age has to be more than 50")
            );
        });


        this.tests.set("property-isnew", (executor) => {

            executor.executeRestTestCams('should only return new cams', {page: 1, isnew: true},
                r => r.cams.forEach(c => assert.isTrue(c.isNew), "all the cams should be new")
            );
        });


        this.tests.set("property-gender", (executor) => {

            ["male", "female", "shemale", "couple"].forEach(g => {
                executor.executeRestTestCams('should only return cams with the given gender (all providers) ' + g, {
                        page: 1,
                        gender: g
                    },
                    r => r.cams.forEach(c => assert.equal(g, c.gender, "gender" + c.gender + " instead of " + g))
                );
            });

            ["male", "female", "shemale", "couple"].forEach(g => {
                executor.executeRestTestCams('should only return cams with the given gender (chaturbate) ' + g, {
                        page: 1,
                        gender: g,
                        provider: provider.CHATURBATE
                    },
                    r => r.cams.forEach(c => assert.equal(g, c.gender, "gender" + c.gender + " instead of " + g))
                );
            });

            ["female", "couple", "male", "shemale"].forEach(g => {
                executor.executeRestTestCams('should only return cams with the given gender (bongacams) ' + g, {
                        page: 1,
                        gender: g,
                        provider: provider.BONGACAMS
                    },
                    r => r.cams.forEach(c => assert.equal(g, c.gender, "gender" + c.gender + " instead of " + g))
                );
            });
        });

        this.tests.set("property-ishd", (executor) => {

            executor.executeRestTestCams('should only return hd cams', {page: 1, ishd: true},
                r => r.cams.forEach(c => assert.isTrue(c.isHd), "all cams should be hd")
            );
        });


        this.tests.set("property-query1", (executor) => {

            executor.executeRestTestCams('should only return results with country string', {page: 1, query: "colombia"},
                r => r.cams.forEach(c => assert.isTrue(StringUtil.containsCaseIns(JSON.stringify(c), "colombia"), "cam should contain 'colombia'")));
        });


        this.tests.set("property-query2", (executor) => {

            executor.executeRestTestCams('should only return results with tag string', {page: 1, query: "blowjob"},
                r => r.cams.forEach(c => assert.isTrue(StringUtil.containsCaseIns(JSON.stringify(c), "blowjob"), "cam should contain 'blowjob'")));
        });


        this.tests.set("property-country", (executor) => {

            ["de", "us", "co", "ru"].forEach(ct => {
                executor.executeRestTestCams('should only return cams with the given country ' + ct, {
                        page: 1,
                        country: ct
                    },
                    r => r.cams.forEach(c => assert.equal(ct, c.country, "country " + c.country + " instead of " + ct))
                );
            });
        });


        this.tests.set("property-haircolor", (executor) => {

            ["brunette", "blonde", "redhead"].forEach(hc => {
                executor.executeRestTestCams('should only return cams with the given haircolor ' + hc, {
                        page: 1,
                        haircolor: hc
                    },
                    r => r.cams.forEach(c => assert.equal(hc, c.hairColor, "haircolor " + c.hairColor + " instead of " + hc))
                );
            });
        });


        this.tests.set("property-body", (executor) => {

            ["slim", "petite", "fat"].forEach(b => {
                executor.executeRestTestCams('should only return cams with the given body ' + b, {page: 1, body: b},
                    r => r.cams.forEach(c => assert.equal(b, c.body, "body " + c.body + " instead of " + b))
                );
            });
        });


        this.tests.set("property-language", (executor) => {

            ["de", "es", "ru"].forEach(l => {
                executor.executeRestTestCams('should only return cams with the given language ' + l, {
                        page: 1,
                        language: l
                    },
                    r => r.cams.forEach(c => assert.isTrue(c.languageKeys.indexOf(l) > -1), "should contain language " + l)
                );
            });
        });

        this.tests.set("property-balanced", (executor) => {

            executor.executeRestTestCams('should return the cams ordered by number of users but balanced by provider', {page: 1, order: "balanced"},
                r => {
                    assert.isFalse(TestUtil.isSortedByNumberOfUsers(r.cams), "cams shouldn't be sorted by users > is balanced out");
                });
        });


        this.tests.set("property-most", (executor) => {

            executor.executeRestTestCams('should return the cams ordered by number of users', {page: 1, order: "most"},
                r => {
                    let noOfUsers = this.maxNumber;
                    for (let cam of r.cams) {
                        assert.isTrue(noOfUsers >= cam.numberOfUsers, "user order not correct");
                        noOfUsers = cam.numberOfUsers;
                    }
                });
        });


        this.tests.set("property-youngest", (executor) => {

            executor.executeRestTestCams('should return the cams ordered by age (youngest)', {
                    page: 1,
                    order: "youngest"
                },
                r => {
                    let age = 18;
                    for (let cam of r.cams) {
                        assert.isTrue(age <= cam.age, "age order not correct");
                        age = cam.age;
                    }
                });
        });


        this.tests.set("property-oldest", (executor) => {

            executor.executeRestTestCams('should return the cams ordered by age (youngest)', {page: 1, order: "oldest"},
                r => {
                    let age = this.maxNumber;
                    for (let cam of r.cams) {
                        assert.isTrue(age >= cam.age, "age order not correct");
                        age = cam.age;
                    }
                });
        });


        this.tests.set("property-combination1", (executor) => {

            executor.executeRestTestCams('should return the cams with the combination of parameters',
                {
                    page: 1,
                    minage: 18,
                    maxage: 42,
                    gender: "female",
                    country: "us",
                    haircolor: "blonde",
                    language: "en",
                    order: "most"
                },
                r => {
                    let noOfUsers = this.maxNumber;
                    for (let cam of r.cams) {
                        assert.isTrue(cam.age <= 42, "age should be equal or less than 28");
                        assert.isTrue(cam.age >= 18, "age should be equal or more than 18");
                        assert.equal(cam.gender, "female", "gender should be female");
                        assert.equal(cam.country, "us", "country should be us");
                        assert.equal(cam.hairColor, "blonde", "haircolor should be blonde");
                        assert.isTrue(cam.languageKeys.indexOf("en") > -1, "en should be among the language keys");
                        assert.isTrue(noOfUsers >= cam.numberOfUsers, "order of the number of users is not correct");
                        noOfUsers = cam.numberOfUsers;
                    }
                });
        });

        this.tests.set("property-combination2-stripchat", (executor) => {

            executor.executeRestTestCams('should return the cams of stripchat with age between 18 and 95',
                {
                    provider: "stripchat"
                },
                r => {
                    for (let cam of r.cams) {
                        assert.isTrue(cam.age <=95, "age should be equal or less than 95");
                        assert.isTrue(cam.age >=18, "age should be equal or more than 18");
                    }
                });
        });


        this.tests.set("property-provider", (executor) => {

            provider.ALL.forEach(p => {
                executor.executeRestTestCams('should only return cams with the given provider ' + p, {
                        page: 1,
                        provider: p
                    },
                    r => r.cams.forEach(c => assert.equal(p, c.provider, "provider " + c.provider + " instead of " + p))
                );
            });
        });


        this.tests.set("property-countries", (executor) => {

            executor.executeRestTestCams('should return the list of all countries with cams path', {page: 1},
                r => {
                    assert.isTrue(r.countries.length > 10, "there should be more than 10 cams in the countries list");
                    assert.isTrue(r.countries.some(co => co.code === "us"), "the usa should be in the countries list");
                    assert.isTrue(r.countries.some(co => co.code === "co"), "colombia should be in the countries list");
                    assert.isTrue(r.countries.some(co => co.code === "ru"), "russia should be in the countries list");
                }
            );
        });
    }


}
