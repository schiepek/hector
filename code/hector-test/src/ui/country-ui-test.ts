import {AbstractTest} from '../backend/abstract-test';
import {assert} from 'chai';
import {TestType} from '../general/test-type';
import {parse} from 'node-html-parser';

export class CountryUiTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.UI;
    }

    protected initTests(): void {

        this.tests.set("country-ui", (executor) => {

            let url = executor.config.getUiRenderUrlProd() + "/world-map-of-sex-cams"

            executor.executeRestTest(url, url + ': world map should have valid content', res => {
                assert.equal(res.status, 200, url + ' does not return status 200');

                CountryUiTest.assertWorldMapContainerIsThere(res.text);

                CountryUiTest.assertThumbIsComplete(res.text);

            })
        });

    }

    private static assertWorldMapContainerIsThere(html: string): void {
        let doc = parse(html);
        let vectorMapContainer = doc.querySelectorAll(".hector-map");
        assert.isNotEmpty(vectorMapContainer);
    }

    private static assertThumbIsComplete(html: string): void {
        let doc = parse(html);
        let allCountryClouds = doc.querySelectorAll(".widget-tagcloud");

        CountryUiTest.assertContinentNode(allCountryClouds[0], "North America", ["usa", "canada"])
        CountryUiTest.assertContinentNode(allCountryClouds[1], "South America", ["colombia"])
        CountryUiTest.assertContinentNode(allCountryClouds[2], "Europe", ["russia", "united kingdom", "germany"])
        CountryUiTest.assertContinentNode(allCountryClouds[3], "Africa", ["south africa"])
        CountryUiTest.assertContinentNode(allCountryClouds[4], "Asia", ["philippines", "japan"])
        CountryUiTest.assertContinentNode(allCountryClouds[5], "Oceania", ["australia"])
    }

    private static assertContinentNode(element: any, continent: string, mandatoryCountries: string[]): void {
        assert.equal(element.firstChild.firstChild.innerText, continent);
        let doc = parse(element.toString());

        doc.querySelectorAll(".tag-cloud-btn-country")[0].innerText
        let countryStrings = doc.querySelectorAll(".tag-cloud-btn-country")
            .map(countryNode=>countryNode.innerText);
        mandatoryCountries.forEach(mandatoryCountry => {
            assert.isTrue(countryStrings.some(cs=>cs.toLowerCase().includes(mandatoryCountry)));
        })

    }
}
