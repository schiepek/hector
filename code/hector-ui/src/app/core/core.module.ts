import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WebcamService} from '../services/webcam.service';
import {StateService} from '../services/state.service';
import {ScreenService} from '../services/screen.service';
import {CountryService} from '../services/country.service';
import {CategoryService} from '../services/category.service';
import {MetaService} from '../services/meta.service';
import {LinkService} from '../services/link.service';
import {FooterService} from '../services/footer.service';
import {PartnerService} from '../services/partner.service';
import {ConstantService} from '../services/constant.service';
import {NavigationComponent} from '../components/navigation/navigation.component';
import {FooterComponent} from '../components/footer/footer.component';
import {FooterPageComponent} from '../components/footer/footer-page/footer-page.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {NgtUniversalModule} from '@ng-toolkit/universal';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from '../app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {TranslateUniversalLoader} from '../shared/util/TranslateUniversalLoader';
import {InViewportModule} from 'ng-in-viewport';

export function HttpLoaderFactory(): TranslateLoader {
  return new TranslateUniversalLoader()
}

@NgModule({
  imports: [
    CommonModule,
    NgtUniversalModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory
      }
    }),
    InViewportModule
  ],
  declarations: [
    NavigationComponent,
    FooterComponent,
    FooterPageComponent
  ],
  providers: [
    WebcamService,
    StateService,
    ScreenService,
    CountryService,
    CategoryService,
    MetaService,
    LinkService,
    FooterService,
    PartnerService,
    ConstantService
  ],
  exports: [
    NavigationComponent,
    FooterComponent,
    FooterPageComponent,
    CommonModule,
  ]
})
export class CoreModule {
}
