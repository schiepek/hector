export default
{
    "ar": [
        "arabic"
    ],
    "de": [
        "german",
        "deutsch"
    ],
    "nl": [
        "Dutch"
    ],
    "en": [
        "english",
        "ingles"
    ],
    "es": [
        "spanish",
        "espanol",
        "español"
    ],
    "it": [
        "italian"
    ],
    "he": [
        "hebrew"
    ],
    "ja": [
        "Japanese"
    ],
    "fr": [
        "Français",
        "francais",
        "french"
    ],
    "pt": [
        "Portuguese"
    ],
    "th": [
        "thai"
    ],
    "uk": [
        "Ukrainian"
    ],
    "ro": [
        "Romanian"
    ],
    "ru": [
        "russian",
        "Русский"
    ],
    "vls": [
        "Flemish",
        "flemisch"
    ],
    "zh": [
        "chinese"
    ]
}