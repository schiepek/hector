import {Handler, Context, Callback} from 'aws-lambda';
import {ResponseUtil} from '../util/response-util';
import {CamResult} from '../model/cam-result';
import {CamsLoader} from '../loader/cams-loader';
import {ProviderCamResult} from '../model/provider-cam-result';
import {Loader} from '../loader/loader';
import {ResultLoader} from '../loader/result-loader';

const camsLoader = CamsLoader.getInstance();
const resultLoader = ResultLoader.getInstance();

const handleWarmup: Handler = (func: string, _context: Context, callback: Callback) => {
    let loader = camsLoader.getLoader(func);

    if (loader) {
        loadAndSaveProviderCams(loader, func, callback);
    } else if (func === "warmup-all") {
        loadAndSaveAllCams(func, callback);
    } else {
        callback(undefined, ResponseUtil.notFound());
    }
};

function loadAndSaveProviderCams(loader: Loader, func: string, callback: Callback) {
    camsLoader.loadAndSaveProviderCams(loader)
        .then(result => {
            console.info("Provider successfully loaded and saved: " + loader.getProvider() + " (" + result.size + " cams)")
            providerWarmupResult(func, callback, result)
        })
        .catch(error => {
            console.error("warmup failed: " + func);
            console.error(error);
        })
}

function loadAndSaveAllCams(func: string, callback: Callback) {
    resultLoader.loadAndSaveAllCams()
        .then(result => {
            console.info("All cams successfully loaded and saved: " + result.size + " cams) (" +
                providersFromResult(result).toString() + ")");
            allWarmupResult(func, callback, result)
        })
        .catch(error => {
            console.error("warmup failed: " + func);
            console.error(error);
        })
}

function providersFromResult(result: CamResult) {
    return result.cams ? result.cams
        .map(c => c.provider)
        .filter((x, i, a) => a.indexOf(x) === i) : [];
}

function providerWarmupResult(func: string, callback: Callback, result: ProviderCamResult) {
    callback(undefined, ResponseUtil.json({
        "function": func,
        "result": result.size + " cams loaded successfully"
    }));
}

function allWarmupResult(func: string, callback: Callback, result: CamResult) {
    callback(undefined, ResponseUtil.json({
        "function": func,
        "result": result.size + " cams loaded successfully"
    }));
}

export {handleWarmup}
