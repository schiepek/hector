import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnInit, QueryList, Renderer2,
  ViewChildren
} from '@angular/core';
import {AdService} from '../../services/ad.service';
import {Ad, AdType} from '../../shared/model/ad';
import {ComponentReloader} from '../../shared/util/component-reloader';
import {TranslateService} from '@ngx-translate/core';
import {ScreenService} from '../../services/screen.service';
import {ClientUtil} from '../../shared/util/client-util';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./ad.component.css']
})
export class AdComponent implements OnInit {

  @ViewChildren('adElement')
  public adElement: QueryList<ElementRef>;

  @Input()
  public ad: Ad;

  public isLink: boolean;
  public isHtml: boolean;
  public hasScript: boolean;

  public title: string;
  public linkSrc: string;
  public htmlSrc: string;

  public showAd: boolean;

  constructor(public screen: ScreenService,
              private renderer: Renderer2,
              private adS: AdService,
              private translate: TranslateService,
              private cdRef: ChangeDetectorRef,) {
  }

  ngOnInit() {
    this.initAdType();
    this.initTitle();
    this.loadSourceAndInitAd()
  }

  public onIntersection({ target, visible }: { target: Element; visible: boolean }) {
    visible ? this.showAd = true: null;
  }

  private initTitle(): void {
    this.translate.get(this.ad.translationCode).subscribe(t => this.title = t);
  }

  private initAdType(): void {
    this.isLink = this.ad.type === AdType.LINK;
    this.isHtml = this.ad.type === AdType.HTML;
  }

  private loadSourceAndInitAd(): void {
    this.adS.loadAd(this.ad.src).subscribe(res => {
      (this.ad.type === AdType.HTML) ? this.setHtmlAd(res) : null;
      (this.ad.type === AdType.LINK) ? this.linkSrc = res : null;
      ComponentReloader.detectChanges(this.cdRef);
    });
  }

  private setHtmlAd(res: any): void {
    this.hasScript = this.adS.hasScript(res);
    this.htmlSrc = this.hasScript ? this.adS.loadAdPath(this.ad.src): res;
  }

  public onLoad() {
    this.hasScript && ClientUtil.resizeIFrame(this.adElement,this.renderer);
  }

}
