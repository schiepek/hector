export class ActiveFilter {

  private readonly af_translate: string = "active-filter.";

  public identifier: string;
  public textValue?: string;
  public realValue?: any;

  constructor(identifier: string, textValue?: string, realValue?: any) {
    this.identifier = this.af_translate + identifier;
    this.textValue = textValue;
    this.realValue = realValue;
  }
}
