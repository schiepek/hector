import {CamResult} from '../../../../../hector-api/src/model/cam-result';
import {environment} from '../../../environments/environment';
import {Cam} from '../../../../../hector-api/src/model/cam';


export class DevModeUtil {

  private static readonly devThumbs = "assets/images_hector/dev_thumbs/";
  private static readonly devVideoUrl = 'https://www.youtube.com/embed/qWSYOO8mtmE';

  public static initDevMode(result: CamResult) {
    if (environment.devMode) {
      this.getAllCams(result).forEach(c => {
        let randomNumber = Math.floor(Math.random() * ((9 - 1) + 1) + 1);
        c.imageUrl = this.devThumbs + randomNumber + ".jpg";
        c.embedUrl = this.devVideoUrl;
        c.chatRoomUrl = this.devVideoUrl;
      });
    }
  }

  public static isDev():boolean {
    return environment.devMode;
  }

  private static getAllCams(result: CamResult): Cam[] {
    let allCams = Object.assign([], result.cams);
    if (result.home) {
      allCams.push(...result.home.topCams);
      allCams.push(...result.home.topCamsCountry);
      allCams.push(...result.home.topAsianCams);
      allCams.push(...result.home.topLatinCams);
    }
    return allCams;
  }

}
