import {CloudFrontRequest} from 'aws-lambda';

export class DomainUtil {

    private static readonly renderUriPrefix: string = "https://render.";

    /**
     * Extracts the domain (for example lemoncams.com out the host (works only with simple domain endings like .com (not with .co.uk for example)
     */
    public static extractDomain(request: CloudFrontRequest): string {
        let host = request.headers['host'][0].value;
        let domainName = host.substring(host.lastIndexOf(".", host.lastIndexOf(".") - 1) + 1);
        return this.renderUriPrefix + domainName;
    }

}