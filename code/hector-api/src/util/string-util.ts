export class StringUtil {

    public static containsCaseIns(left: string, right: string): boolean {
        if (left && right) {
            let leftStr = left.toLowerCase();
            let rightStr = right.toLowerCase();


            if (leftStr.indexOf(rightStr) >= 0) {
                return true;
            }
        }
        return false;
    }

    public static containsCaseInsArr(left: string, right: string[]) {
        if (left && right) {
            return right.some(r => this.containsCaseIns(left, r));
        }
        return false;
    }

    public static equalsCaseIns(left: string, right: string): boolean {
        if (left && right) {
            return left.toLowerCase() === right.toLowerCase();
        }
        return false;
    }

    public static extractEmbedUrl(iframe: string): string {
        let regex = /<iframe.*?src='(.*?)'/;
        let src = regex.exec(iframe);
        return src ? src[1] : '';
    }

    public static toFirstLetterUppercase(str: string): string {
        return str ? str.charAt(0).toUpperCase() + str.slice(1) : str;
    }

}
