export const environment = {
  production: true,
  project: "lemoncams",
  apiUrl: "https://api-v2-prod.lemoncams.com",
  domain: "www.lemoncams.com",
  website: "Lemoncams",
  analytics: "UA-120903295-1",
  showCategories: true,
  showCountries: true,
  isSubSite: false,
  refreshInterval: 60,
  devMode: false
};
