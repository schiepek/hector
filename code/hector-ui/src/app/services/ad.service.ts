import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {DevModeUtil} from '../shared/util/devmode-util';

@Injectable({
  providedIn: 'root'
})
export class AdService {

  private readonly assetsFolder: string = "assets/ads/";
  private readonly devAd: string = "dev/dev.html";

  constructor(private http: HttpClient) {
  }

  public loadAd(location: string): Observable<any> {
    return this.http.get(this.loadAdPath(location), {responseType: 'text'});
  }

  public loadAdPath(location: string): string {
    return DevModeUtil.isDev() ? this.assetsFolder + this.devAd : this.assetsFolder + location;
  }

  public hasScript(res: string): boolean {
    return res.indexOf("<script") !== -1;
  }

}
