import {Cam} from '../model/cam';

export interface SpecificCamFunction {
    (cams: Cam[]): Cam[];
}
