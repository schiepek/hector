import {CamFunction} from './cam-function';
import {SearchFunction} from './search-function';
import {SortFunction} from './sort-function';
import {PageFunction} from './page-function';
import {CamResult} from '../model/cam-result';
import {HectorConfig} from '../util/hector-config';
import {DataUtil} from '../util/data-util';

/**
 * POSSIBLE QUERY STRING PARAMETERS
 *
 * maxage={number}
 * minage={number}
 * isnew=true,false
 * gender=female,male,couple,shemale
 * ishd=true,false
 * query={string}
 * country={countries.js.keys}
 * haircolor=brunette,blonde,redhead,black
 * body=slim,petite,fat
 * language{languages.js.keys}
 * provider=chaturbate,bongacams,awempire
 * order=most,youngest,oldest
 * page={number}
 * category=asian
 * tag=squirt
 */
export class CamsFunction {

    private functions: CamFunction[] = [];
    private pageFunction: CamFunction;

    constructor(queryStrings: any) {
        if (queryStrings) {
            this.functions =
                [
                    new CamFunction(queryStrings["maxage"], (c) => c.filter(cam => cam.age <= queryStrings["maxage"])),
                    new CamFunction(queryStrings["minage"], (c) => c.filter(cam => cam.age >= queryStrings["minage"])),
                    new CamFunction(queryStrings["isnew"], (c) => c.filter(cam => cam.isNew)),
                    new CamFunction(queryStrings["gender"], (c) => c.filter(cam => cam.gender === queryStrings["gender"])),
                    new CamFunction(queryStrings["ishd"], (c) => c.filter(cam => cam.isHd)),
                    new SearchFunction(queryStrings["query"]),
                    new CamFunction(queryStrings["country"], (c) => c.filter(cam => cam.country === queryStrings["country"])),
                    new CamFunction(queryStrings["haircolor"], (c) => c.filter(cam => cam.hairColor === queryStrings["haircolor"])),
                    new CamFunction(queryStrings["body"], (c) => c.filter(cam => cam.body === queryStrings["body"])),
                    new CamFunction(queryStrings["language"], (c) => c.filter(cam => cam.languageKeys.some(k => k === queryStrings["language"]))),
                    new CamFunction(queryStrings["provider"], (c) => c.filter(cam => cam.provider === queryStrings["provider"])),
                    new CamFunction(queryStrings["category"], (c) => c.filter(cam => cam.categories.some(ca => ca === queryStrings["category"]))),
                    new CamFunction(queryStrings["tag"], (c) => c.filter(cam => cam.tags.some(ca => ca === queryStrings["tag"]))),
                    new SortFunction(queryStrings["order"])
                ];
            this.pageFunction = new PageFunction(queryStrings["page"]);
        }
    }

    public updateResult(result: CamResult, event: any): CamResult {
        let partners = DataUtil.getPartners(result,event);
        let newRes = new CamResult(result.cams, result.countries, 0, 0, undefined, undefined, partners, result.date, result.meta);

        this.functions.forEach(f => newRes.cams = f.processCams(newRes.cams));

        newRes.size = newRes.cams.length;
        newRes.maxPage = (~~(newRes.cams.length / HectorConfig.getPageSize())) + 1;

        // we need to filter the page at the end
        newRes.cams = this.pageFunction.processCams(newRes.cams);

        return newRes;
    }

}
