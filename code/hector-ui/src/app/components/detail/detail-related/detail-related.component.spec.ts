import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailRelatedComponent } from './detail-related.component';

describe('DetailRelatedComponent', () => {
  let component: DetailRelatedComponent;
  let fixture: ComponentFixture<DetailRelatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailRelatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailRelatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
