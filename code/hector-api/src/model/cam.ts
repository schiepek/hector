export class Cam {
    username: string;
    age: number;
    isPrivate: boolean;
    isNew: boolean;
    numberOfUsers: number;

    /** m, f, s, c **/
    gender: string;
    isHd: boolean;

    imageUrl: string;
    chatRoomUrl: string;
    embedUrl: string;
    embedUrlMobile: string;

    title: string;
    tags: string[];
    turnsOn: string;
    turnsOff: string;

    /** blonde, brunette, redhead, black **/
    hairColor: string;

    /** slim, fat, petite*/
    body: string;

    location: string;
    country: string;
    languages: string;
    languageKeys: string[];
    provider: string;

    categories: string[];

    public static of(username: string, age: number, isPrivate: boolean, isNew: boolean, numberOfUsers: number, gender: string, isHd: boolean, imageUrl: string, chatRoomUrl: string, embedUrl: string, embedUrlMobile: string, title: string, tags: string[], turnsOn: string, turnsOff: string, hairColor: string, body: string, location: string, country: string, languages: string, languageKeys: string[], provider: string): Cam {
        let cam = new Cam();

        cam.username = username;
        cam.age = age;
        cam.isPrivate = isPrivate;
        cam.isNew = isNew;
        cam.numberOfUsers = numberOfUsers;
        cam.gender = gender;
        cam.isHd = isHd;
        cam.imageUrl = imageUrl;
        cam.chatRoomUrl = chatRoomUrl;
        cam.embedUrl = embedUrl;
        cam.embedUrlMobile = embedUrlMobile;
        cam.title = title;
        cam.tags = tags;
        cam.turnsOn = turnsOn;
        cam.turnsOff = turnsOff;
        cam.hairColor = hairColor;
        cam.body = body;
        cam.location = location;
        cam.country = country;
        cam.languages = languages;
        cam.languageKeys = languageKeys;
        cam.provider = provider;
        return cam;
    }

}
