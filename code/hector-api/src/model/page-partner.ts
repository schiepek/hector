import {Partner} from './partner';

export class PagePartner {

    page: string;
    partners: Partner[] = [];

    constructor(page: string) {
        this.page = page;
    }
}