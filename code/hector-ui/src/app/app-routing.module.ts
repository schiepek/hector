import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [

  // HOME
  {path: '', loadChildren: 'app/modules/home/home.module#HomeModule'},

  // MAIN ROUTES
  {path: 'cams', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'world-map-of-sex-cams', loadChildren: 'app/modules/country/country.module#CountryModule'},
  {path: 'country', redirectTo: '/world-map-of-sex-cams'},
  {path: 'cams2', loadChildren: 'app/modules/emergency/emergency.module#EmergencyModule'},
  {path: 'roulette', loadChildren: 'app/modules/detail/detail.module#DetailModule'},

  // Categories
  {path: 'categories', loadChildren: 'app/modules/category/category.module#CategoryModule'},
  {path: 'categories/:p1', loadChildren: 'app/modules/category/category.module#CategoryModule'},

  // DETAIL CAM
  {path: 'topcam', loadChildren: 'app/modules/detail/detail.module#DetailModule'},
  {path: 'topcam/:p1', loadChildren: 'app/modules/detail/detail.module#DetailModule'},
  {path: 'chaturbate/:username', loadChildren: 'app/modules/detail/detail.module#DetailModule'},
  {path: 'bongacams/:username', loadChildren: 'app/modules/detail/detail.module#DetailModule'},
  {path: 'camsoda/:username', loadChildren: 'app/modules/detail/detail.module#DetailModule'},
  {path: 'stripchat/:username', loadChildren: 'app/modules/detail/detail.module#DetailModule'},

  // FOOTER
  {path: 'privacy', loadChildren: 'app/modules/content/content.module#ContentModule'},
  {path: 'terms', loadChildren: 'app/modules/content/content.module#ContentModule'},
  {path: '2257', loadChildren: 'app/modules/content/content.module#ContentModule'},
  {path: 'copyright', loadChildren: 'app/modules/content/content.module#ContentModule'},
  {path: 'contact', loadChildren: 'app/modules/content/content.module#ContentModule'},
  {path: 'about', loadChildren: 'app/modules/content/content.module#ContentModule'},
  {path: 'sitemap', loadChildren: 'app/modules/sitemap/sitemap.module#SitemapModule'},

  // 404
  {path: 'not-found', loadChildren: 'app/modules/not-found/not-found.module#NotFoundModule'},

  // MODEL
  {path: 'how-to-become-a-webcam-model', redirectTo: 'blog/how-to-become-a-webcam-model'},

  // BLOG
  {path: 'blog', loadChildren: 'app/modules/blog/blog.module#BlogModule'},
  {path: 'blog/:p1', loadChildren: 'app/modules/blog/blog.module#BlogModule'},

  // WEBCAM SEARCH PARAMETER ROUTES
  {path: 'search', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1/:p2', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1/:p2/:p3', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1/:p2/:p3/:p4', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1/:p2/:p3/:p4/:p5', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1/:p2/:p3/:p4/:p5/:p6', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1/:p2/:p3/:p4/:p5/:p6/:p7', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1/:p2/:p3/:p4/:p5/:p6/:p7/:p8', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1/:p2/:p3/:p4/:p5/:p6/:p7/:p8/:p9', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: 'search/:query/:p1/:p2/:p3/:p4/:p5/:p6/:p7/:p8/:p9/:p10', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},

  // WEBCAM PARAMETER ROUTES
  {path: ':p1', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: ':p1/:p2', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: ':p1/:p2/:p3', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: ':p1/:p2/:p3/:p4', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: ':p1/:p2/:p3/:p4/:p5', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: ':p1/:p2/:p3/:p4/:p5/:p6', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: ':p1/:p2/:p3/:p4/:p5/:p6/:p7', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: ':p1/:p2/:p3/:p4/:p5/:p6/:p7/:p8', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: ':p1/:p2/:p3/:p4/:p5/:p6/:p7/:p8/:p9', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},
  {path: ':p1/:p2/:p3/:p4/:p5/:p6/:p7/:p8/:p9/:p10', loadChildren: 'app/modules/webcam/webcam.module#WebcamModule'},

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})


export class AppRoutingModule {
}
