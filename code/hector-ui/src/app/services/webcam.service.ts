import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {RestUtil} from '../shared/util/rest-util';
import {CamResult} from '../../../../hector-api/src/model/cam-result';
import {StateService} from './state.service';
import {CountryService} from './country.service';
import {DevModeUtil} from '../shared/util/devmode-util';
import {PartnerService} from './partner.service';
import {Router} from '@angular/router';
import {Parameters} from '../shared/model/parameters';

@Injectable()
export class WebcamService {

  private readonly url = environment.apiUrl + '/main';
  private readonly camsFunction = "cams";
  private readonly camFunction = "cam";
  private readonly homeFunction = "home";
  private readonly menuFunction = "menu";
  private readonly randomFunction = "random";
  private readonly autoCompleteFunction = "autocomplete";
  private readonly categoriesFunction = "categories";

  constructor(private http: HttpClient,
              private router: Router,
              private countryS: CountryService,
              private partner: PartnerService,
              private state: StateService) {
  }

  public getCams(): Promise<CamResult> {
    return this.executeRequest(RestUtil.getParams(this.state, this.camsFunction, this.getPath(), environment.project));
  }

  public getCam(): Promise<CamResult> {
    return this.executeRequest(RestUtil.getParams(this.state, this.camFunction, this.getPath(), environment.project));
  }

  public getRandomCam(loadedCams: string[]): Promise<CamResult> {
    return this.executeRequest(RestUtil.getParams(this.state, this.randomFunction, this.getPath(), environment.project));
  }

  public getHome(): Promise<CamResult> {
    return this.executeRequest(RestUtil.getParams(this.state, this.homeFunction, this.getPath(), environment.project));
  }

  public getMenu(): Promise<CamResult> {
    let params = RestUtil.getParams(this.state, this.menuFunction, this.getPath(), environment.project);
    return this.executeRequest(params);
  }

  public getAutoComplete(): Promise<CamResult> {
    let params = RestUtil.getQueryParams(this.state, this.autoCompleteFunction);
    return this.executeRequest(params, false);
  }

  public getCategories(filter? : string): Promise<CamResult> {
    let params = RestUtil.getQueryParams(this.state, this.categoriesFunction);
    params = !!filter ? params.append(Parameters.FILTER, filter) : params;
    return this.executeRequest(params, true);
  }

  private prepareCams(response: any): CamResult {
    let result = response as CamResult;

    if (!result.error) {
      this.countryS.initCountryGridMenu(result.countries);
      this.partner.initPartners(result.partners);
      DevModeUtil.initDevMode(result);
    }
    return result;
  }

  private executeRequest(params: HttpParams, prepareCams: boolean = true): Promise<CamResult> {
    return this.http.get(this.url, {params: params})
      .toPromise()
      .then(response => prepareCams ? this.prepareCams(response) : response as CamResult)
      .catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return new Promise((resolve, reject) => {
      let result = new CamResult([], [], 0, 0, null, null, null);
      result.error = true;
      resolve(result);
    });
  }

  private getPath(): string {
    return this.router.url.replace(new RegExp('/', 'g'), '.');
  }

}
