import {CamFunction} from './cam-function';
import {HectorConfig} from '../util/hector-config';

export class PageFunction extends CamFunction {

    constructor(queryString: any) {
        super(queryString,
            (cams) => {
                let page = queryString - 1;
                let firstCamIndex = page * HectorConfig.getPageSize();
                let lastCamIndex = firstCamIndex + HectorConfig.getPageSize();

                return cams.slice(firstCamIndex, lastCamIndex);
            });
    }

}
