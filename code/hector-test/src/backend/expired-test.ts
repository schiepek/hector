import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {ExpirationData} from '../../../hector-api/src/util/expiration-data';
import {CamResult} from '../../../hector-api/src/model/cam-result';
import {TestConfig} from '../util/test-config';
import {TestType} from '../general/test-type';


export class ExpiredTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {

        this.tests.set("expired", (executor, config) => {
            executor.executeRestTestFunction(config.getExpiredFunction(), 'should just check if the expired cams could be loaded from s3', {},
                r => {
                    assert.isTrue(r["expired"].indexOf("successfully") > -1, "The expired response should contain with a 'successfully' string");
                }
            );
        });

        this.tests.set("expiration-data-not-expired", () => {
            it("should retrieve if the cams are expired or not", () => {
                let expirationData = new ExpirationData();
                expirationData.setDate();
                assert.isFalse(expirationData.isExpired(), "the test data should not be expired immediately")
            });
        });

        this.tests.set("expiration-data-expired", () => {
            it("should retrieve if the cams are expired or not", (done) => {
                let expirationData = new ExpirationData();
                expirationData.setDate();
                let expiredFunction = () => {
                    assert.isTrue(expirationData.isExpired(), "the test data should be expired after one minute");
                    done()
                };
                setTimeout(expiredFunction, (new ExpirationData().maxExpirationInMinutes * 60000) + 1000);
            }).timeout((new ExpirationData().maxExpirationInMinutes * 60000) + 10000);
        });

    }

    public static dateIsNotExpired(result: CamResult, config: TestConfig) : boolean {
        let maxExpMinutes = config.getEndpoint().includes("prod") ? new ExpirationData().maxExpirationInMinutes : config.getDevWarmupTime();

        let maximumExpiredDate = new Date();
        maximumExpiredDate.setTime(new Date().getTime() - ((maxExpMinutes * 60 * 1000) + 60000));

        return !!result.date ? (new Date(result.date).getTime() - new Date(maximumExpiredDate).getTime()) > 0 : false;
    }
}
