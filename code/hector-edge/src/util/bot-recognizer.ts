import {CloudFrontHeaders, CloudFrontRequest} from 'aws-lambda';

export class BotRecognizer {

    private static readonly BOT: string = "bot";
    private static readonly NO_BOT: string = "no_bot";
    private static readonly HEADER: string = "user-agent-type";

    public static setHeaderValue(request: CloudFrontRequest) : void {
        let botValue = BotRecognizer.getHeaderValue(request);
        request.headers[BotRecognizer.HEADER] = [{key: BotRecognizer.HEADER, value: botValue}];
    }

    private static getHeaderValue(request: CloudFrontRequest): string {
        return BotRecognizer.hasToBePrerendered(request) ? BotRecognizer.BOT : BotRecognizer.NO_BOT;
    }

    public static hasToBePrerendered(request: any): boolean {
        if(request.uri.startsWith('/not-found')) {
            return true;
        }
        let headers = request.headers;
        let user_agent = headers['user-agent'];
        let host = headers['host'];
        if (user_agent && host) {
            let prerender = /LinkChecker|googlebot|bingbot|yandex|baiduspider|Facebot|facebookexternalhit|twitterbot|rogerbot|linkedinbot|embedly|quora link preview|showyoubot|outbrain|pinterest|slackbot|vkShare|W3C_Validator/i.test(user_agent[0].value);
            prerender = prerender || /_escaped_fragment_/.test(request.querystring);
            prerender =  prerender && ! /\.(js|css|xml|less|png|jpg|jpeg|gif|pdf|txt|ico|rss|zip|mp3|rar|exe|wmv|doc|avi|ppt|mpg|mpeg|tif|wav|mov|psd|ai|xls|mp4|m4a|swf|dat|dmg|iso|flv|m4v|torrent|ttf|woff|svg|eot)$/i.test(request.uri);
            return prerender && !request.uri.startsWith('/assets');
        }
        return false;
    }

    public static hasBotHeader(headers: CloudFrontHeaders): boolean {
        return headers[this.HEADER] && headers[this.HEADER][0].value === this.BOT;
    }
}
