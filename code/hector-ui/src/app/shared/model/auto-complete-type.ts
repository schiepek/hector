export enum AutoCompleteType {
  user_chaturbate,
  user_bongacams,
  user_awempire,

  category,
  tag,
  gender,
  haircolor,
  attribute,
  body,
  language,
  age,

  country,
  worldmap
}

export namespace AutoCompleteType {
  export function forProvider(provider: string): AutoCompleteType {
    switch (provider) {
      case "chaturbate":
        return AutoCompleteType.user_chaturbate;
      case "bongacams":
        return AutoCompleteType.user_bongacams;
      case "awempire":
        return AutoCompleteType.user_awempire;
      default:
        return null;
    }
  }

  export function isUser(type: AutoCompleteType): boolean {
    switch (type) {
      case AutoCompleteType.user_chaturbate:
        return true;
      case AutoCompleteType.user_bongacams:
        return true;
      case AutoCompleteType.user_awempire:
        return true;
      default:
        return false;
    }
  }
}
