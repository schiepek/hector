import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryThumbComponent } from './category-thumb.component';

describe('CategoryThumbComponent', () => {
  let component: CategoryThumbComponent;
  let fixture: ComponentFixture<CategoryThumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryThumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryThumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
