import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ConstantService} from '../../../services/constant.service';

@Component({
  selector: 'app-country-thumb',
  templateUrl: './country-thumb.component.html',
  styleUrls: ['./country-thumb.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CountryThumbComponent {

  constructor(public constant: ConstantService) { }

}
