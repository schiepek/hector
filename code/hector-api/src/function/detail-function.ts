import {CamFunction} from './cam-function';
import {CamResult} from '../model/cam-result';
import {RelatedFunction} from './related-function';
import {PageFunction} from './page-function';
import {DataUtil} from '../util/data-util';

/**
 * POSSIBLE QUERY STRING PARAMETERS
 *
 * username={string}
 * provider=chaturbate,bongacams,camsoda (awempire has no detailpage)
 */
export class DetailFunction {

    private detailFunction: CamFunction;
    private offlineFunction: CamFunction;

    constructor(queryStrings: any) {
        if (queryStrings) {
            this.detailFunction = new CamFunction(queryStrings["username"], (c) => {
                return c.filter(c => c.username === queryStrings["username"] && c.provider === queryStrings["provider"]).slice(0, 1);
            });
            this.offlineFunction = new PageFunction(1);
        }
    }

    /** the first cam is the detail cam > the rest are the related cams **/
    public updateResult(result: CamResult, event: any): CamResult {
        let allCams = result.cams;

        let detailCams = this.detailFunction.processCams(allCams);

        if (detailCams.length > 0) {
            detailCams = detailCams.concat(new RelatedFunction(detailCams[0], "related").processCams(allCams));
        } else {
            detailCams = this.offlineFunction.processCams(allCams);
        }

        let partners = DataUtil.getPartners(result,event);
        return new CamResult(detailCams, result.countries, 0, 0, undefined, undefined, partners, result.date, result.meta);
    }

}
