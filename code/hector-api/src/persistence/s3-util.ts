import {CamResult} from '../model/cam-result';
import {GetObjectRequest, PutObjectRequest} from 'aws-sdk/clients/s3';
import {ProviderCamResult} from '../model/provider-cam-result';

const AWS = require('aws-sdk');
const S3 = new AWS.S3();


export class S3Util {

    public static saveCamsToS3(result: CamResult, resolve: any): void {
        let params : PutObjectRequest =  {
            Bucket: process.env.CAMS_BUCKET || '',
            Key: 'result.json',
            Body: JSON.stringify(result),
            ContentType: 'application/json'
        };
        let s3Callback = (err: any) => {
            err ? console.error(err) : resolve(result);
        };
        if(!process.env.IS_OFFLINE) {
            S3.putObject(params, s3Callback);
        } else {
            resolve(result);
        }
    }

    public static saveProviderCamsToS3(result: ProviderCamResult, provider: string, resolve: any) {
        let params : PutObjectRequest =  {
            Bucket: process.env.CAMS_BUCKET || '',
            Key: 'result-' + provider + '.json',
            Body: JSON.stringify(result),
            ContentType: 'application/json'
        };
        let s3Callback = (err: any) => {
            if(err) {
                console.log("Error saving provider result: " + provider);
                console.log(err);
            } else {
                resolve(result)
            }
        };
        if(!process.env.IS_OFFLINE) {
            S3.putObject(params, s3Callback);
        } else {
            resolve(result);
        }
    }


    public static getResultFromS3(s3Callback: any, offlineS3Callback: any) : void {
        let params : GetObjectRequest =  {
            Bucket: process.env.CAMS_BUCKET || '',
            Key: 'result.json',
        };
        if(!process.env.IS_OFFLINE) {
            return S3.getObject(params, s3Callback);
        } else {
            offlineS3Callback();
        }
    }

    public static getCamsFromS3(s3Callback: any, provider: string, offlineS3Callback: any) : void {
        let params : GetObjectRequest =  {
            Bucket: process.env.CAMS_BUCKET || '',
            Key: 'result-' + provider + '.json',
        };
        if(!process.env.IS_OFFLINE) {
            return S3.getObject(params, s3Callback);
        } else {
            offlineS3Callback();
        }
    }

}
