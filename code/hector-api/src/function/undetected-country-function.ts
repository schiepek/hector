import {Cam} from '../model/cam';
import {PropertyEvaluator} from '../util/property-evaluator';

export class UndetectedCountryFunction {

    public getUndetectedCountries(cams: Cam[]) : string[] {
        return cams
            .filter(c => !c.country && c.location)
            .filter(c=>this.isNotUndetectable(c))
            .map(c => c.location);
    }

    private isNotUndetectable(cam: Cam) : boolean {
        let isNotWithinUndetectableCountries = !PropertyEvaluator.getUndetectableLocations()
            .some(l=>cam.location.toLowerCase().includes(l.toLowerCase()));
        let isNotWithinUndetectableDirectLocations = !PropertyEvaluator.getUndetectableDirectLocations()
        .some((l=>cam.location.toLowerCase() === l.toLowerCase()))
        return isNotWithinUndetectableCountries && isNotWithinUndetectableDirectLocations;
    }

}
