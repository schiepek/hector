import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebcamTitleComponent } from './webcam-title.component';

describe('WebcamTitleComponent', () => {
  let component: WebcamTitleComponent;
  let fixture: ComponentFixture<WebcamTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebcamTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebcamTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
