import {CategoryService} from '../../services/category.service';
import {Category} from './category';
import {PassiveFilter} from './passive-filter';
import {CategoryCode} from '../../../../../hector-api/src/model/category-code';
import {provider} from '../../../../../hector-api/src/model/provider';
export class AllFilter {

  private categoryS: CategoryService;
  private categories: Category[];

  public genderFilters: PassiveFilter[] = [];
  public ageFilters: PassiveFilter[] = [];
  public categoryFilters: PassiveFilter[] = [];
  public languageFilters: PassiveFilter[] = [];
  public haircolorFilters: PassiveFilter[] = [];
  public bodyFilters: PassiveFilter[] = [];
  public siteFilters: PassiveFilter[] = [];
  public attributeFilters: PassiveFilter[] = [];

  constructor(categoryS: CategoryService, categories: Category[]) {
    this.categoryS = categoryS;
    this.categories = categories;
    this.initGenderFilters();
    this.initAgeFilters();
    this.initCategoryFilters();
    this.initLanguageFilters();
    this.initHairFilters();
    this.initBodyFilters();
    this.initSiteFilters();
    this.initAttributeFilters();
  }

  private initGenderFilters() {
    this.addFilter(this.genderFilters, CategoryCode.female, "gender-filter.female");
    this.addFilter(this.genderFilters, CategoryCode.male, "gender-filter.male");
    this.addFilter(this.genderFilters, CategoryCode.couple, "gender-filter.couple");
    this.addFilter(this.genderFilters, CategoryCode.shemale, "gender-filter.shemale");
  }

  private initAgeFilters() {
    this.addFilter(this.ageFilters, CategoryCode.undertwenty, "18-19");
    this.addFilter(this.ageFilters, CategoryCode.twenties, "20-30");
    this.addFilter(this.ageFilters, CategoryCode.thirties, "31-40");
    this.addFilter(this.ageFilters, CategoryCode.overforty, "40+");
  }

  private initCategoryFilters() {
    this.addFilter(this.categoryFilters, CategoryCode.asian, "categories.asian");
    this.addFilter(this.categoryFilters, CategoryCode.bdsm, "categories.bdsm");
    this.addFilter(this.categoryFilters, CategoryCode.bigdick, "categories.bigdick");
    this.addFilter(this.categoryFilters, CategoryCode.bigtits, "categories.bigtits");
    this.addFilter(this.categoryFilters, CategoryCode.ebony, "categories.ebony");
    this.addFilter(this.categoryFilters, CategoryCode.hairy, "categories.hairy");
    this.addFilter(this.categoryFilters, CategoryCode.latina, "categories.latina");
    this.addFilter(this.categoryFilters, CategoryCode.mature, "categories.mature");
    this.addFilter(this.categoryFilters, CategoryCode.milf, "categories.milf");
    this.addFilter(this.categoryFilters, CategoryCode.tattoo, "categories.tattoo");
    this.addFilter(this.categoryFilters, CategoryCode.teen, "categories.teen");
    this.addFilter(this.categoryFilters, CategoryCode.smalltits, "categories.smalltits");
  }

  private initLanguageFilters() {
    this.addFilter(this.languageFilters, CategoryCode.english, "language-filter.english");
    this.addFilter(this.languageFilters, CategoryCode.spanish, "language-filter.spanish");
    this.addFilter(this.languageFilters, CategoryCode.russian, "language-filter.russian");
    this.addFilter(this.languageFilters, CategoryCode.german, "language-filter.german");
    this.addFilter(this.languageFilters, CategoryCode.french, "language-filter.french");
    this.addFilter(this.languageFilters, CategoryCode.italian, "language-filter.italian");
  }

  private initHairFilters() {
    this.addFilter(this.haircolorFilters, CategoryCode.blonde, "hair-filter.blonde");
    this.addFilter(this.haircolorFilters, CategoryCode.brunette, "hair-filter.brunette");
    this.addFilter(this.haircolorFilters, CategoryCode.redhead, "hair-filter.redhead");
  }

  private initBodyFilters() {
    this.addFilter(this.bodyFilters, CategoryCode.slim, "body-filter.slim");
    this.addFilter(this.bodyFilters, CategoryCode.petite, "body-filter.petite");
    this.addFilter(this.bodyFilters, CategoryCode.fat, "body-filter.fat");
  }

  private initSiteFilters() {
    this.addFilter(this.siteFilters, CategoryCode.chaturbate, "provider." + provider.CHATURBATE);
    this.addFilter(this.siteFilters, CategoryCode.bongacams, "provider." + provider.BONGACAMS);
    this.addFilter(this.siteFilters, CategoryCode.awempire, "provider." + provider.AWEMPIRE);
    this.addFilter(this.siteFilters, CategoryCode.camsoda, "provider." + provider.CAMSODA);
    this.addFilter(this.siteFilters, CategoryCode.stripchat, "provider." + provider.STRIPCHAT);
  }

  private initAttributeFilters() {
    this.addFilter(this.attributeFilters, CategoryCode.new, "attribute-filter.isnew");
    this.addFilter(this.attributeFilters, CategoryCode.hd, "attribute-filter.ishd");
  }

  public oneAgeFilterIsSelected() {
    return this.oneFilterIsSelected(this.ageFilters);
  }

  public oneLanguageFilterIsSelected() {
    return this.oneFilterIsSelected(this.languageFilters);
  }

  public oneCategoryFilterIsSelected() {
    return this.oneFilterIsSelected(this.categoryFilters);
  }

  public oneHairFilterIsSelected() {
    return this.oneFilterIsSelected(this.haircolorFilters);
  }

  public oneBodyFilterIsSelected() {
    return this.oneFilterIsSelected(this.bodyFilters);
  }

  public oneSiteFilterIsSelected() {
    return this.oneFilterIsSelected(this.siteFilters);
  }

  public oneAttributeFilterIsSelected() {
    return this.oneFilterIsSelected(this.attributeFilters);
  }

  private oneFilterIsSelected(list: PassiveFilter[]): boolean {
    return list.some(f => f.isChecked);

  }

  private addFilter(list: PassiveFilter[], code: CategoryCode, translationCode: string): void {
    list.push(
      new PassiveFilter(
        !!this.categories.find(c => c.code === code),
        this.categoryS.getFilterLink(this.categories, code),
        translationCode)
    );
  }

}
