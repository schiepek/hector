import {Injectable} from '@angular/core';
import {FooterText} from '../shared/model/footer-text';
import {TranslateService} from '@ngx-translate/core';
import {LanguageUtil} from '../shared/util/language-util';
import {StateService} from './state.service';
import {CategoryCode} from '../../../../hector-api/src/model/category-code';
import {Category} from '../shared/model/category';
import {HttpClient} from '@angular/common/http';

import footerJson_en from 'assets/i18n/footer_en.json';
import {CountryService} from './country.service';
import {ConstantService} from './constant.service';

const FOOTER_TEXTS = {
  en: footerJson_en,
};

@Injectable()
export class FooterService {

  public footerTexts: FooterText[];

  constructor(private translate: TranslateService,
              private constant: ConstantService,
              private country: CountryService,
              private http: HttpClient,
              private state: StateService) {
    this.initFooters();
  }

  private initFooters(): void {
    this.footerTexts = Object.values(FOOTER_TEXTS[LanguageUtil.getCurrentLanguage(this.translate)]) as FooterText[];
  }

  private loadFooterTexts(): FooterText[] {
    let footerTexts = Object.values(FOOTER_TEXTS[LanguageUtil.getCurrentLanguage(this.translate)]) as FooterText[];
    footerTexts.forEach(ft => ft.showIcobox = ft.id === "home");
    return footerTexts;
  }

  public setCamsFooterText(categories: Category[]): void {
    if (categories.length > 0 && categories[0].code === CategoryCode.query) {
      // Do nothing for search query;
    } else if (categories.length > 0 && categories[0].code === CategoryCode.country) {
      this.setCountryText(this.state.country);
    } else if (categories.length > 0 && categories[0].group !== "page") {
      this.setGenericFooterText(categories[0].path);
    } else {
      this.setGenericFooterText("home");
    }
  }

  private setCountryText(countryCode: string): void {
    let countryName = this.country.getName(countryCode);
    let countryAdj = this.country.getAdjective(countryCode);

    // we need a copy here, otherwise we modify the imported list
    let footerText = Object.create(this.loadFooterTexts().find(ft => ft.id === "country"));

    footerText.title = this.translate.parser.interpolate(footerText.title, {value1: countryAdj, value2: countryName});
    footerText.descriptions = footerText.descriptions.map(d => this.translate.parser.interpolate(d, {
      value1: countryAdj,
      value2: countryName,
      subdomain: this.constant.getSubDomain()
    }));
    footerText.features = footerText.features.map(d => this.translate.parser.interpolate(d, {
      value1: countryAdj,
      value2: countryName,
      subdomain: this.constant.getSubDomain()
    }));
    this.state.footerText = footerText;
  }

  public setGenericFooterText(id: string): void {
    let footerText = this.loadFooterTexts().find(ft => ft.id === id);
    if(footerText) {
      footerText.descriptions = footerText.descriptions.map(d => this.translate.parser.interpolate(d, {
        subdomain: this.constant.getSubDomain()
      }));
      footerText.features = footerText.features.map(d => this.translate.parser.interpolate(d, {
        subdomain: this.constant.getSubDomain()
      }));
      this.state.footerText = footerText;
    }
  }


}
