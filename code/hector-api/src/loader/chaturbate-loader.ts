import {Cam} from '../model/cam';
import {Loader} from './loader';
import {PropertyEvaluator} from '../util/property-evaluator';
import {HectorConfig} from '../util/hector-config';
import {Categorizer} from '../categorizer/categorizer';
import {provider} from '../model/provider';
import {StringUtil} from '../util/string-util';
import {TagEvaluator} from '../util/tag-evaluator';

export class ChaturbateLoader extends Loader {

    private static readonly CHATURBATE_EMBEDDED_URL: string = "https://chaturbate.com/in/?track=embed&tour=9oGW&campaign=xAq5D&room={username}&disable_sound=1&mobileRedirect=auto&embed_video_only=1"

    constructor(categorizer: Categorizer) {
        super(categorizer, provider.CHATURBATE);
    }

    public getApiUrls(): string[] {
        return HectorConfig.getChaturbateUrls();
    }

    protected getJsonDataObject(res: any): any {
        return JSON.parse(res);
    }

    protected readProperties(camObj: any): Cam {
        let username = camObj["username"];
        let age = camObj["age"];
        let isPrivate = camObj["current_show"] != "public";
        let isNew = camObj["is_new"];
        let numberOfUsers = camObj["num_users"];
        let gender = PropertyEvaluator.getGenderByString(camObj["gender"]);
        let isHd = camObj["is_hd"];
        let imageUrl = camObj["image_url"];
        let chatRoomUrl = camObj["chat_room_url_revshare"];
        let embedUrl = ChaturbateLoader.getEmbedUrl(camObj["iframe_embed_revshare"]);
        let embedUrlMobile = ChaturbateLoader.CHATURBATE_EMBEDDED_URL.replace("{username}", username)
        let title = camObj["room_subject"];
        let tags = TagEvaluator.executeForCam(camObj["tags"], title);
        let turnsOn = "";
        let turnsOff = "";
        let location = camObj["location"];
        let languages = camObj["spoken_languages"];
        let languageKeys = PropertyEvaluator.getLanguageKeysByString(languages);
        let country = PropertyEvaluator.getCountryByString(location);
        let hairColor = PropertyEvaluator.getHairColorByString(tags.toString() + "," + title);
        let body = PropertyEvaluator.getBodyByString(tags.toString() + "," + title);

        return Cam.of(username, age, isPrivate, isNew, numberOfUsers, gender, isHd, imageUrl, chatRoomUrl, embedUrl, embedUrlMobile, title, tags, turnsOn, turnsOff, hairColor, body, location, country, languages, languageKeys, this.getProvider());
    }

    private static getEmbedUrl(url: string): string {
        let extractedUrl = StringUtil.extractEmbedUrl(url);
        return extractedUrl ? extractedUrl.replace('bgcolor=white', 'bgcolor=transparent') : url;
    }

}
