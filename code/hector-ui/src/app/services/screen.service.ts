import {Injectable} from '@angular/core';
import {ClientUtil} from '../shared/util/client-util';
import {ScreenSize} from '../shared/model/screen-size';

/**
 * Gives information about the current screen properties
 * The values are based on the bootstrap framework
 */
@Injectable()
export class ScreenService {

  private readonly xs: number = 554;
  private readonly sm: number = 768;
  private readonly md: number = 992;
  private readonly lg: number = 1200;

  private deviceWindowSize: number;

  constructor() {
    this.deviceWindowSize = ScreenService.windowSize();
  }

  public isHiddenXsDown(): boolean {
    return ScreenService.windowSize() >= this.xs;
  }

  public isHiddenSmDown(): boolean {
    return ScreenService.windowSize() >= this.sm;
  }

  public isHiddenMdDown(): boolean {
    return ScreenService.windowSize() >= this.md;
  }

  public isHiddenLgDown(): boolean {
    return ScreenService.windowSize() >= this.lg;
  }

  public isHiddenSmUp(): boolean {
    return ScreenService.windowSize() < this.xs;
  }

  public isHiddenMdUp(): boolean {
    return ScreenService.windowSize() < this.sm;
  }

  public isHiddenLgUp(): boolean {
    return ScreenService.windowSize() < this.md;
  }

  public isHiddenXLUp(): boolean {
    return ScreenService.windowSize() < this.lg;
  }

  public isXs(): boolean {
    return this.isHiddenSmUp();
  }

  public isSm(): boolean {
    return this.isHiddenXsDown() && this.isHiddenLgUp();
  }

  public isMd(): boolean {
    return this.isHiddenSmDown() && this.isHiddenXLUp()
  }

  public isLg(): boolean {
    return this.isHiddenMdDown() && this.isHiddenXLUp();
  }

  public isXl(): boolean {
    return this.isHiddenLgDown();
  }

  private static windowSize(): number {
    if(ClientUtil.isClientSide()) {
      return window.innerWidth;
    }
  }

  public getScreenSize(): ScreenSize {
    if (this.isXl()) {
      return ScreenSize.XL;
    } else if (this.isLg()) {
      return ScreenSize.LG;
    } else if (this.isMd()) {
      return ScreenSize.MD
    } else if (this.isSm()) {
      return ScreenSize.SM
    } else {
      return ScreenSize.XS;
    }
  }

  public requestFullScreen(element: any) {

    //Supports most browsers and their versions.
    var requestMethod = element.requestFullScreen
      || element.webkitRequestFullScreen
      || element.mozRequestFullScreen
      || element.msRequestFullScreen;

    if (requestMethod) { // Native full screen.
      requestMethod.call(element);
    }
  }

  public isIos(): boolean {
    return !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
  }

  public scroll(id) {
    let el = document.getElementById(id);
    el.scrollIntoView({behavior:"smooth"});
  }

}
