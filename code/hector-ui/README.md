# HectorUi

* ng serve (run locally)
* deploy.bat for S3 Deployment

# Build dev
npx npm-force-resolutions
npm install
ng serve

## Installation Guide For Server Side Rendering
* npm install
* npm pm run build:serverless
* create_domain certificate (has just to be done once for the custom domain)
* sls deploy

## SSR
npm install
npm run build:prod

#run universal local
npm run server 

#build application
npm run build:serverless
npm run build:canada
npm run build:russia

#build sub page with browser and server


#deploy universal server to lambda (check certs)
sls deploy --project canada
sls deploy --project lemoncams


