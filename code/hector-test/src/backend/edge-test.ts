import {AbstractTest} from './abstract-test';
import {assert} from 'chai';
import {LinkUtil} from '../../../hector-edge/src/util/link-util';
import {TestType} from '../general/test-type';

export class EdgeTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {

        this.tests.set("edge-link-util", () => {

            it("The TestUtil should process link with and without trackers", () => {
                let noTrackerLink = LinkUtil.getLink({"uri": "https://www.lemoncams.com/recommends/chaturbate-signup"}, this.getJson());
                let trackerLink = LinkUtil.getLink({
                    "uri": "https://www.lemoncams.com/recommends/chaturbate-signup",
                    "querystring": "tr=test"
                }, this.getJson());
                let brokenLink = LinkUtil.getLink({"uri": "https://www.lemoncams.com/recommends/brokenLink"}, this.getJson());

                let expectedNoTrackerLink = "https://chaturbate.com/in/?track=default&tour=3Mc9&campaign=xAq5D&redirect_to_room=-welcomepage-";
                let expectedTrackerLink = "https://chaturbate.com/in/?tour=3Mc9&campaign=xAq5D&redirect_to_room=-welcomepage-&track=test";

                assert.equal(noTrackerLink, expectedNoTrackerLink, "no tracker link didn't match the expected link generator from LinkUtil");
                assert.equal(trackerLink, expectedTrackerLink, "tracker link didn't match the expected link generator from LinkUtil");
                assert.equal(brokenLink, undefined, "a broken link should be undefined");
            });
        });

    }

    private getJson(): string {
        return "{    " +
            "\"no-tracker\": {   " +
            "   \"chaturbate-signup\": \"https://chaturbate.com/in/?track=default&tour=3Mc9&campaign=xAq5D&redirect_to_room=-welcomepage-\"   " +
            " },  " +
            "\"tracker\": " +
            "{\"chaturbate-signup\": \"https://chaturbate.com/in/?tour=3Mc9&campaign=xAq5D&redirect_to_room=-welcomepage-&track={{TRACKER}}\" }" +
            "}";

    }

}
