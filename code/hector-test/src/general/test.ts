import 'mocha';
import {TestConfig} from '../util/test-config';
import {TestExecutor} from '../util/test-executor';
import {CategoryTest} from '../backend/category-test';
import {PropertyTest} from '../backend/property-test';
import {MenuTest} from '../backend/menu-test';
import {DetailTest} from '../backend/detail-test';
import {WarmupTest} from '../backend/warmup-test';
import {ApiTest} from '../backend/api-test';
import {HomeTest} from '../backend/home-test';
import {PartnerTest} from '../backend/partner-test';
import {LinkTest} from '../ui/link-test';
import {EdgeTest} from '../backend/edge-test';
import {ExpiredTest} from '../backend/expired-test';
import {AutocompleteTest} from '../backend/autocomplete-test';
import {HomeUiTest} from '../ui/home-ui-test';
import {TestType} from './test-type';
import chai = require('chai');
import chaiHttp = require('chai-http');
import {CamsUiTest} from '../ui/cams-ui-test';
import {CountryUiTest} from '../ui/country-ui-test';
import {RandomTest} from '../backend/random-test';
import {UtilTest} from '../backend/util-test';
import {TagTest} from '../backend/tag-test';
import {CategoriesTest} from '../backend/categories-test';
import {CategoriesUiTest} from '../ui/categories-ui-test';


chai.use(chaiHttp);

describe('cam functions', () => {

    let config = new TestConfig(process.env);
    let executor = new TestExecutor(config);

    let tests = [
        // warmup
        new WarmupTest(),

        // backend
        new CategoryTest(),
        new TagTest(),
        new PropertyTest(),
        new MenuTest(),
        new DetailTest(),
        new ApiTest(),
        new HomeTest(),
        new CategoriesTest(),
        new PartnerTest(),
        new EdgeTest(),
        new AutocompleteTest(),
        new ExpiredTest(),
        new RandomTest(),
        new UtilTest(),

        // ui
        new LinkTest(),
        new HomeUiTest(),
        new CamsUiTest(),
        new CountryUiTest(),
        new CategoriesUiTest()
    ];

    tests.forEach(t => {
        if (config.isHelp()) {
            t.printTests()
        } else {
            let testType = config.getTestType();
            if (testType === TestType.ALL) {
                t.execute(executor, config)
            } else if (t.type === testType) {
                t.execute(executor, config);
            }
        }
    });

});
