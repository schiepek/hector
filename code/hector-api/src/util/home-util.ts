import {Home} from '../model/home';
import {SortFunction} from '../function/sort-function';
import {Cam} from '../model/cam';
import {Country} from '../model/country';
import {Categorizer} from '../categorizer/categorizer';
import {CategoryCode} from '../model/category-code';

export class HomeUtil {

    // actual grid size is 36, but we add one advertisement
    public static readonly topCamsByCountrySize: number = 36;
    public static readonly randomCountrySize: number = 16;
    public static readonly categoryListSize: number = 5;
    public static readonly topCamSize: number = 3;

    public static createHome(cams: Cam[], countries: Country[], categorizer: Categorizer): Home {
        if (cams.length === 0) {
            console.error("home is empty because of empty cams");
            return this.createEmptyHome();
        }

        let topCams = this.getTopCams(cams);
        let topCamsCountry = this.getTopCamsCountry(cams, countries);
        let topAsianCams = categorizer.filterByCategory(cams, CategoryCode.asian).slice(0, this.categoryListSize);
        let topLatinCams = categorizer.filterByCategory(cams, CategoryCode.latina).slice(0, this.categoryListSize);
        let randomCountries = this.getRandomCountries(countries);

        return new Home(topCams, topCamsCountry, topAsianCams, topLatinCams, randomCountries);
    }

    private static getTopCams(cams: Cam[]): Cam[] {
        return cams.slice(0, this.topCamSize);
    }


    private static getTopCamsCountry(cams: Cam[], countries: Country[]): Cam[] {
        let topCamsCountry: Cam[] = [];

        countries.forEach(co => {
            let firstCountryCam = cams.find(c => c.country === co.code);
            (firstCountryCam) ? topCamsCountry.push(firstCountryCam) : null;
        });

        return new SortFunction("most").processCams(topCamsCountry).slice(0, this.topCamsByCountrySize);
    }

    private static getRandomCountries(countries: Country[]): Country[] {
        let randomCountries: Country[] = [];

        while (randomCountries.length < this.randomCountrySize) {
            let randomElement = countries[Math.floor((Math.random() * countries.length))];
            if (randomCountries.length != 0 || !randomCountries.some(co => co.code === randomElement.code)) {
                randomCountries.push(randomElement);
            }
        }
        return randomCountries;
    }

    private static createEmptyHome() {
        return new Home([], [], [], [], []);
    }

}
