export default
{
    "AF": [
        "Afghanistan",
        "afganistan"
    ],
    "AX": [
        "Åland Islands"
    ],
    "AL": [
        "Albania"
    ],
    "DZ": [
        "Algeria"
    ],
    "AS": [
        "American Samoa"
    ],
    "AD": [
        "Andorra"
    ],
    "AO": [
        "Angola"
    ],
    "AI": [
        "Anguilla"
    ],
    "AQ": [
        "Antarctica"
    ],
    "AG": [
        "Antigua & Barbuda"
    ],
    "AR": [
        "Argentina",
        "buenos aires",
        "Buenos Aires",
		"BuenosAires"
    ],
    "AM": [
        "Armenia",
        "Yerevan"
    ],
    "AW": [
        "Aruba"
    ],
    "AC": [
        "Ascension Island"
    ],
    "AU": [
        "Australia",
        "Сидней",
        "Sidney",
        "Melbourne",
        "Perth",
        "down under"
    ],
    "AT": [
        "Austria",
        "wien",
        "vienna",
        "Österreich",
		"St. Pölten",
		"Sölden",
		"Salzburg"
    ],
    "AZ": [
        "Azerbaijan"
    ],
    "BS": [
        "Bahamas"
    ],
    "BH": [
        "Bahrain"
    ],
    "BD": [
        "Bangladesh",
		"Bonarpara"
    ],
    "BB": [
        "Barbados"
    ],
    "BY": [
        "Belarus",
        "Minsk",
        "Weissrussland",
        "Минск"
    ],
    "BE": [
        "Belgium",
        "Brussels",
        "Antwerpen",
        "Belgiue"
    ],
    "BZ": [
        "Belize"
    ],
    "BJ": [
        "Benin"
    ],
    "BM": [
        "Bermuda"
    ],
    "BT": [
        "Bhutan"
    ],
    "BO": [
        "Bolivia"
    ],
    "BA": [
        "Bosnia & Herzegovina",
        "Bosnia and Herzegovina"
    ],
    "BW": [
        "Botswana"
    ],
    "BR": [
        "Brazil",
        "São Paulo",
        "brasil",
        "rio de janeiro",
        "Rio de Janerio"
    ],
    "IO": [
        "British Indian Ocean Territory"
    ],
    "VG": [
        "British Virgin Islands"
    ],
    "BN": [
        "Brunei"
    ],
    "BG": [
        "Bulgaria",
        "Sofia",
        "Bulgary",
        "Bulgarien",
        "varna",
		"София"
    ],
    "BF": [
        "Burkina Faso"
    ],
    "BI": [
        "Burundi"
    ],
    "KH": [
        "Cambodia"
    ],
    "CM": [
        "Cameroon"
    ],
    "CA": [
        "Canada",
        "quebec",
        "vancouver",
        "Ontario",
        "Canadiana",
        "Toronto",
		"ottawa"
    ],
    "IC": [
        "Canary Islands"
    ],
    "CV": [
        "Cape Verde"
    ],
    "KY": [
        "Cayman Islands"
    ],
    "CF": [
        "Central African Republic"
    ],
    "EA": [
        "Ceuta & Melilla"
    ],
    "TD": [
        "Chad"
    ],
    "CL": [
        "Chile",
        "Valdivia",
    ],
    "CN": [
        "China",
		"shanghai",
        "Shandong"
    ],
    "CX": [
        "Christmas Island"
    ],
    "CC": [
        "Cocos (Keeling) Islands"
    ],
    "CO": [
        "Colombia",
        "Cajica",
        "cali",
        "colombiana",
        "valle",
        "Santiago de Cal",
        "Cúcuta",
        "Cucuta",
        "Antioquia",
        "medellin",
        "Medellín",
        "bogota",
        "Bogotá",
        "pereira",
        "cundinamarca",
        "Barranquilla",
        "bucaramanga",
        "BUCARAMANGA",
        "Cartagena",
        "Dosquebradas",
        "colmbia",
        "santa marta",
		"𝓒𝓸𝓵𝓸𝓶𝓫𝓲𝓪",
        "ibague",
        "CUMLOMBIA",
        "Cucutá",
        "𝐌𝐞𝐝𝐞𝐥𝐥𝐢𝐧",
        "C O L O M B I A",
        "𝒄𝒐𝒍𝒐𝒎𝒃𝒊𝒂",
        "𝓒𝓞𝓛𝓞𝓜𝓑𝓘𝓐",
        "𝒞𝒪𝐿𝒪𝑀𝐵𝐼𝒜",
        "Columbia",
        "Medellìn",
        "𝑪𝒐𝒍𝒐𝒎𝒃𝒊𝒂",
        "𝐂𝐎𝐋𝐎𝐌𝐁𝐈𝐀",
        "Col♥",
        "𝒞𝑜𝓁𝑜𝓂𝒷𝒾𝒶"
    ],
    "KM": [
        "Comoros"
    ],
    "CG": [
        "Congo - Brazzaville"
    ],
    "CD": [
        "Congo - Kinshasa"
    ],
    "CK": [
        "Cook Islands"
    ],
    "CR": [
        "Costa Rica",
        "costa  rica"
    ],
    "CI": [
        "Côte d’Ivoire"
    ],
    "HR": [
        "Croatia"
    ],
    "CU": [
        "Cuba",
        "hawana"
    ],
    "CW": [
        "Curaçao"
    ],
    "CY": [
        "Cyprus"
    ],
    "CZ": [
        "Czechia",
        "czech",
        "Czech Republic",
        "Prague",
        "prag",
        "Брно",
		"Прага",
		"Warshawa",
        "Сzech Republic"
    ],
    "DK": [
        "Denmark"
    ],
    "DG": [
        "Diego Garcia"
    ],
    "DJ": [
        "Djibouti"
    ],
    "DM": [
        "Dominica"
    ],
    "DO": [
        "Dominican Republic",
        "SANTO DOMINGO"
    ],
    "EC": [
        "Ecuador",
        "quito"
    ],
    "EG": [
        "Egypt"
    ],
    "SV": [
        "El Salvador"
    ],
    "GQ": [
        "Equatorial Guinea"
    ],
    "ER": [
        "Eritrea"
    ],
    "EE": [
        "Estonia",
        "tartu",
        "estland",
        "Tallinn",
        "Tallin"
    ],
    "ET": [
        "Ethiopia"
    ],
    "EU": [
        "Europa",
        "europe",
        "European union",
        "EE UU",
		"Europ"
    ],
    "EZ": [
        "Eurozone"
    ],
    "FK": [
        "Falkland Islands"
    ],
    "FO": [
        "Faroe Islands"
    ],
    "FJ": [
        "Fiji"
    ],
    "FI": [
        "Finland",
        "Seinajoki",
        "Helsinki"
    ],
    "FR": [
        "France",
        "paris",
        "lyon",
        "toulouse",
        "Versailles",
        "French"
    ],
    "GF": [
        "French Guiana"
    ],
    "PF": [
        "French Polynesia"
    ],
    "TF": [
        "French Southern Territories"
    ],
    "GA": [
        "Gabon"
    ],
    "GM": [
        "Gambia"
    ],
    "DE": [
        "Germany",
        "Munich",
        "Dusseldorf",
        "Berlin",
        "Stuttgart",
        "hamburg",
        "deutschland",
        "München",
        "Passau",
        "essen",
        "kassel",
        "Frankfurt",
        "Bielefeld",
        "Gemany",
        "Dortmund",
        "Germania",
        "potsdam"
    ],
    "GH": [
        "Ghana"
    ],
    "GI": [
        "Gibraltar"
    ],
    "GR": [
        "Greece",
        "athen",
        "Santorini"
    ],
    "GL": [
        "Greenland"
    ],
    "GD": [
        "Grenada"
    ],
    "GP": [
        "Guadeloupe"
    ],
    "GU": [
        "Guam"
    ],
    "GT": [
        "Guatemala"
    ],
    "GG": [
        "Guernsey"
    ],
    "GN": [
        "Guinea"
    ],
    "GW": [
        "Guinea-Bissau"
    ],
    "GY": [
        "Guyana"
    ],
    "HT": [
        "Haiti"
    ],
    "HN": [
        "Honduras"
    ],
    "HK": [
        "Hong Kong",
        "hongkong",
		"Гонконг"
    ],
    "HU": [
        "Hungary",
        "Budapest",
        "Hungarian"
    ],
    "IS": [
        "Iceland"
    ],
    "IN": [
        "India",
        "Kochi",
		"North Sentinel Island",
		"Mumbai",
		"Bombay",
        "Northeast IN"
    ],
    "ID": [
        "Indonesia"
    ],
    "IR": [
        "Iran"
    ],
    "IQ": [
        "Iraq"
    ],
    "IE": [
        "Ireland",
        "Belfast",
        "Dublin"
    ],
    "IM": [
        "Isle of Man"
    ],
    "IL": [
        "Israel",
        "Izrail"
    ],
    "IT": [
        "Italy",
        "italia",
        "Rome",
        "milano",
        "Milan",
        "Roma",
        "LOMBARDIA",
		"Bologna",
        "itly"
    ],
    "JM": [
        "Jamaica",
        "Saint Andrew"
    ],
    "JP": [
        "Japan",
        "osaka",
        "Tokyo",
        "Tokio",
        "Kyoto"
    ],
    "JO": [
        "Jordan"
    ],
    "KZ": [
        "Kazakhstan",
        "Астана",
        "kasachstan",
		"Караганда",
        "Pavlodar"
    ],
    "KE": [
        "Kenya"
    ],
    "KI": [
        "Kiribati"
    ],
    "XK": [
        "Kosovo"
    ],
    "KW": [
        "Kuwait"
    ],
    "KG": [
        "Kyrgyzstan"
    ],
    "LA": [
        "Laos"
    ],
    "LV": [
        "Latvia",
        "liepaja",
        "riga",
        "Рига"
    ],
    "LB": [
        "Lebanon"
    ],
    "LS": [
        "Lesotho"
    ],
    "LR": [
        "Liberia"
    ],
    "LY": [
        "Libya"
    ],
    "LI": [
        "Liechtenstein"
    ],
    "LT": [
        "Lithuania",
        "Litva",
		"Vilnius"
    ],
    "LU": [
        "Luxembourg"
    ],
    "MO": [
        "Macau SAR China"
    ],
    "MK": [
        "Macedonia"
    ],
    "MG": [
        "Madagascar"
    ],
    "MW": [
        "Malawi"
    ],
    "MY": [
        "Malaysia"
    ],
    "MV": [
        "Maldives"
    ],
    "ML": [
        "Mali"
    ],
    "MT": [
        "Malta"
    ],
    "MH": [
        "Marshall Islands"
    ],
    "MQ": [
        "Martinique"
    ],
    "MR": [
        "Mauritania"
    ],
    "MU": [
        "Mauritius"
    ],
    "YT": [
        "Mayotte"
    ],
    "MX": [
        "Mexico",
        "México",
        "MEXICANA",
		"Мехико",
		"guadalajara",
		"cancun",
		"playa del carmen",
        "MERIDA"
    ],
    "FM": [
        "Micronesia"
    ],
    "MD": [
        "Moldova",
        "chisinau",
        "moldavia"
    ],
    "MC": [
        "Monaco"
    ],
    "MN": [
        "Mongolia"
    ],
    "ME": [
        "Montenegro"
    ],
    "MS": [
        "Montserrat"
    ],
    "MA": [
        "Morocco"
    ],
    "MZ": [
        "Mozambique"
    ],
    "MM": [
        "Myanmar (Burma)"
    ],
    "NA": [
        "Namibia"
    ],
    "NR": [
        "Nauru"
    ],
    "NP": [
        "Nepal"
    ],
    "NL": [
        "Netherlands",
        "nederland",
        "holland",
        "amstedam",
        "Amsterdam",
        "ROTTERDAM",
        "Rotterdam"
    ],
    "NC": [
        "New Caledonia"
    ],
    "NZ": [
        "New Zealand",
        "Auckland"
    ],
    "NI": [
        "Nicaragua"
    ],
    "NE": [
        "Niger"
    ],
    "NG": [
        "Nigeria"
    ],
    "NU": [
        "Niue"
    ],
    "NF": [
        "Norfolk Island"
    ],
    "KP": [
        "North Korea"
    ],
    "MP": [
        "Northern Mariana Islands"
    ],
    "NO": [
        "Norway",
        "oslo"
    ],
    "OM": [
        "Oman"
    ],
    "PK": [
        "Pakistan"
    ],
    "PW": [
        "Palau"
    ],
    "PS": [
        "Palestinian Territories"
    ],
    "PA": [
        "Panama",
		"Panamá"
    ],
    "PG": [
        "Papua New Guinea"
    ],
    "PY": [
        "Paraguay"
    ],
    "PE": [
        "Peru",
        "Lima",
        "Lima - Perú"
    ],
    "PH": [
        "Philippines",
        "manila",
        "cebu",
        "Leyte",
        "malolos",
        "Quezon City",
        "pampanga",
        "phillippines",
        "PHILIPPINE$",
        "Pilipinas",
		"PHILLIPINES",
        "philipines",
        "phlippines"
    ],
    "PN": [
        "Pitcairn Islands"
    ],
    "PL": [
        "Poland",
        "Warsaw",
        "варшава",
        "Warszawa",
		"Катовице",
        "Краков"
    ],
    "PT": [
        "Portugal",
        "Lisbon"
    ],
    "PR": [
        "Puerto Rico"
    ],
    "QA": [
        "Qatar"
    ],
    "RE": [
        "Réunion"
    ],
    "RO": [
        "Romania",
        "Romanian",
        "Bucuresti",
        "ilfov",
        "bucharest",
        "Bacău",
        "bacau",
        "Timisoara",
        "constanta",
        "roumania",
        "Pecica"
    ],
    "RU": [
        "Russia",
        "ПЯТИГОРСК",
        "КАЛИНИНГРАД",
        "КРАСНОДАР",
        "smolensk",
        "Владивосток",
        "wladiwostok",
        "Казань",
        "Архангельск",
        "St. Petersburg",
        "Chelyabinsk",
        "russian federation",
        "moscow",
        "Moskova",
        "moskow",
        "Rjasan",
        "st petersburg",
        "st.petersburg",
        "Saint Petersburg",
        "москва",
        "Россия",
        "Самара",
        "Иркутск",
        "Офигенный",
        "Петербург",
        "ЕКБ",
        "Питер",
        "Псков",
        "Екатеринбург",
        "Zemlya",
        "Бонгакамск",
        "Jekaterinburg",
        "Химки",
        "Белгород",
        "Belgorod",
        "Saratov",
        "Боняня",
        "Сочи",
        "Хабаровск",
        "Тагил",
        "город роз",
        "Таганрог",
        "Новосибирск",
        "Уссурийск",
        "Новосибирск",
        "сибирь",
        "Барвиха",
        "Новороссийск",
        "Krasnodar",
        "Saint-Petersburg",
        "Novosibirsk",
		"Сергиев Посад",
		"Тиливилитряндия",
		"Воронеж",
		"Иваново",
		"Siberia",
		"Ростов-на-Дону",
		"Якутск",
		"Russie",
		"Dark forests of Siberia",
		"Астрахань",
        "Ростов- на -Дону",
        "Rostov",
        "Абрамцево",
        "Moscov",
        "Республика Саха",
        "Pskov",
        "St.-Petersburg",
        "Saint-Peterburg"
    ],
    "RW": [
        "Rwanda"
    ],
    "WS": [
        "Samoa"
    ],
    "SM": [
        "San Marino"
    ],
    "ST": [
        "São Tomé & Príncipe"
    ],
    "SA": [
        "Saudi Arabia"
    ],
    "SN": [
        "Senegal"
    ],
    "RS": [
        "Serbia"
    ],
    "SC": [
        "Seychelles"
    ],
    "SL": [
        "Sierra Leone"
    ],
    "SG": [
        "Singapore",
		"Singapur"
    ],
    "SX": [
        "Sint Maarten"
    ],
    "SK": [
        "Slovakia",
        "Bratislava",
        "𝓢𝓵𝓸𝓿𝓪𝓴𝓲𝓪"
    ],
    "SI": [
        "Slovenia"
    ],
    "SB": [
        "Solomon Islands"
    ],
    "SO": [
        "Somalia"
    ],
    "ZA": [
        "South Africa",
        "durban",
        "kapstadt",
        "Johannesburg",
		"Cape Town"
    ],
    "GS": [
        "South Georgia & South Sandwich Islands"
    ],
    "KR": [
        "South Korea",
        "korea",
        "Seoul",
        "𝓚𝓸𝓻𝓮𝓪"
    ],
    "SS": [
        "South Sudan"
    ],
    "ES": [
        "Spain",
        "españa",
        "espana",
        "madrid",
        "barcelona",
        "Seville",
        "valencia",
        "malaga",
        "alicante",
        "Sevilla"
    ],
    "LK": [
        "Sri Lanka"
    ],
    "BL": [
        "St. Barthélemy"
    ],
    "SH": [
        "St. Helena"
    ],
    "KN": [
        "St. Kitts & Nevis"
    ],
    "LC": [
        "St. Lucia"
    ],
    "MF": [
        "St. Martin"
    ],
    "PM": [
        "St. Pierre & Miquelon"
    ],
    "VC": [
        "St. Vincent & Grenadines"
    ],
    "SD": [
        "Sudan"
    ],
    "SR": [
        "Suriname"
    ],
    "SJ": [
        "Svalbard & Jan Mayen"
    ],
    "SZ": [
        "Swaziland"
    ],
    "SE": [
        "Sweden"
    ],
    "CH": [
        "Switzerland",
        "Schweiz",
        "SWIZERLAND",
        "swiss",
        "Geneva",
        "genf",
        "zürich",
        "zurich",
        "Basel",
        "suisse"
    ],
    "SY": [
        "Syria"
    ],
    "TW": [
        "Taiwan"
    ],
    "TJ": [
        "Tajikistan"
    ],
    "TZ": [
        "Tanzania"
    ],
    "TH": [
        "Thailand",
        "Phuket",
        "Bangkok",
        "Pattaya",
        "𝑻𝒉𝒂𝒊𝒍𝒂𝒏𝒅"
    ],
    "TL": [
        "Timor-Leste"
    ],
    "TG": [
        "Togo"
    ],
    "TK": [
        "Tokelau"
    ],
    "TO": [
        "Tonga"
    ],
    "TT": [
        "Trinidad & Tobago"
    ],
    "TA": [
        "Tristan da Cunha"
    ],
    "TN": [
        "Tunisia"
    ],
    "TR": [
        "Turkey",
        "türkiye",
        "Istanbul",
        "turkiye",
        "M E R S İ N",
        "Smyrna-Anatolia",
        "türkey"
    ],
    "TM": [
        "Turkmenistan"
    ],
    "TC": [
        "Turks & Caicos Islands"
    ],
    "TV": [
        "Tuvalu"
    ],
    "UM": [
        "U.S. Outlying Islands"
    ],
    "VI": [
        "U.S. Virgin Islands"
    ],
    "UG": [
        "Uganda"
    ],
    "UA": [
        "Ukraine",
        "Ялта",
        "донецк",
        "Odessa",
        "Львов",
        "Ukrainian",
        "Украина",
        "Kiev",
        "Dnepro",
        "ucraine",
        "Харьков",
        "Николаев",
        "Киев",
        "Dnipro",
        "ДНЕПР",
        "Одесса",
        "Севастополь",
        "Херсон",
        "Кировоград",
        "Ucraina",
		"Фастов",
		"donetsk",
        "Симферополь",
        "Саратов",
        "Чудесный",
        "𝒖𝒌𝒓𝒂𝒊𝒏𝒆"
    ],
    "AE": [
        "United Arab Emirates",
        "Dubai"
    ],
    "GB": [
        "United Kingdom",
        "UNITED KINGDOM",
        "england",
        "united kngdom",
        "unitedkingdom",
        "london",
        "UK",
        "U.K",
        "Liverpool",
        "manchester",
        "Лондон",
        "Great Britain",
        "Nottingham",
        "Bristol",
        "leicester",
		"Scotland",
		"schotland",
        "sheffiled",
        "𝐋𝐨𝐧𝐝𝐨𝐧",
        "Britain"
    ],
    "UN": [
        "United Nations"
    ],
    "US": [
        "United States",
        "Unites States",
        "unitedstates",
        "Washington",
        "Brooklyn",
        "Detroit",
        "new orleans",
        "san francisco",
        "U:S:A",
        "usa",
        "U.S.A",
        "U.S",
        "NYC",
        "Massachusetts",
        "U.S.",
        "Kentucky",
        "atlanta",
        "Hawaii",
        "new mexico",
        "New Jersey",
        "Wisconsin",
        "Minnesota",
        "Alabama",
        "Arizona",
        "Indiana",
        "Michigan",
        "beverly hills",
        "Pennsylvania",
        "L.A.",
        "miami",
        "las vegas",
        "chicago",
        "los angeles",
        "new york",
        "upstate",
        "texas",
        "california",
        "san diego",
        "florida",
        "virginia",
        "Colorado",
        "North Carolina",
        "otawa",
        "Dallas",
        "Missouri",
        "Seattle",
        "Boston",
        "UNITED STATE OF AMERICA",
        "Estados Unidos",
        "Big Lake MN",
        "Louisiana",
        "United State",
        "FL,middleburg",
        "Phoenix",
        "New Hampshire, US",
        "Oregon",
        "Wash DC",
        "South, U.S",
        "Tampa",
        "Illinois",
		"palm beach",
		"West Coast (PST)",
		"New-York",
		"America",
		"Sacramento",
		"Western NY",
		"East Coast",
		"East Coast US",
        "Hollywood",
        "Connecticut",
        "Nevada",
        "Alaska",
        "Arkanasas",
        "Houston"
    ],
    "UY": [
        "Uruguay"
    ],
    "UZ": [
        "Uzbekistan"
    ],
    "VU": [
        "Vanuatu"
    ],
    "VA": [
        "Vatican City"
    ],
    "VE": [
        "Venezuela",
        "Caracas",
        "venezolana"
    ],
    "VN": [
        "Vietnam",
        "hanoi"
    ],
    "WF": [
        "Wallis & Futuna"
    ],
    "EH": [
        "Western Sahara"
    ],
    "YE": [
        "Yemen"
    ],
    "ZM": [
        "Zambia"
    ],
    "ZW": [
        "Zimbabwe"
    ]
}
