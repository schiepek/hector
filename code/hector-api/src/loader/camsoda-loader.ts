import {Cam} from '../model/cam';
import {Loader} from './loader';
import {PropertyEvaluator} from '../util/property-evaluator';
import {HectorConfig} from '../util/hector-config';
import {Categorizer} from '../categorizer/categorizer';
import {provider} from '../model/provider';
import {TagEvaluator} from '../util/tag-evaluator';

export class CamSodaLoader extends Loader {

    constructor(categorizer: Categorizer) {
        super(categorizer, provider.CAMSODA);
    }

    public getApiUrls(): string[] {
        return HectorConfig.getCamSodaUrls();
    }

    protected getJsonDataObject(res: any): any {
        return JSON.parse(res)["results"];
    }

    protected readProperties(camObj: any): Cam {
        let username = camObj["username"];
        let age = 0;
        let isPrivate = camObj["status"] == "private";
        let isNew = camObj["is_new"];
        let numberOfUsers = camObj["viewers"];
        let gender = PropertyEvaluator.getGenderByString(camObj["gender"]);
        let isHd = false;
        let imageUrl = camObj["thumb"];
        let chatRoomUrl = camObj["link"];
        let embedUrl = camObj["link_iframe"];
        let title = camObj["subject"];
        let tags = TagEvaluator.executeForCam(CamSodaLoader.extractTags(camObj["tags"]), title);
        let turnsOn = "";
        let turnsOff = "";
        let location = "";
        let languages = "";
        let languageKeys: string[] = [];
        let country = PropertyEvaluator.getCountryByString(tags.join(" "));
        let hairColor = PropertyEvaluator.getHairColorByString(tags.toString() + "," + title);
        let body = PropertyEvaluator.getBodyByString(tags.toString() + "," + title);

        return Cam.of(username, age, isPrivate, isNew, numberOfUsers, gender, isHd, imageUrl, chatRoomUrl, embedUrl, null, title, tags, turnsOn, turnsOff, hairColor, body, location, country, languages, languageKeys, this.getProvider());
    }

    private static extractTags(tags: any): string[] {
        return !tags ? [] : Array.isArray(tags) ? tags : Object.values(tags);
    }

}
