import {Cam} from '../model/cam';
export class CamValidator {

    public static isValid(cam: Cam): boolean {
        let isValid = true;

        isValid = isValid && !!cam.imageUrl;
        isValid = isValid && !!cam.username;

        return isValid
    }
}