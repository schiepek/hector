import {TestExecutor} from '../util/test-executor';
import {TestConfig} from '../util/test-config';
import {TestFunction} from '../general/test-function';
import {TestType} from '../general/test-type';

export abstract class AbstractTest {

    public type: TestType;
    protected tests: Map<string, TestFunction> = new Map();

    constructor() {
        this.initTests();
        this.setTestType();
    }

    public execute(executor: TestExecutor, config: TestConfig): void {
        let func = config.getTestFunction();

        if (func) {
            let testFunction = this.tests.get(func);
            testFunction ? testFunction(executor, config) : null;
        } else {
            this.tests.forEach(tf => tf(executor, config));
        }
    }

    public printTests() {
        [...this.tests.keys()].forEach(k=>console.log(k));
    }

    protected abstract initTests(): void;

    protected abstract setTestType(): void;

}
