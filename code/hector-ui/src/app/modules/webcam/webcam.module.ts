import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {WebcamComponent} from '../../components/webcam/webcam.component';
import {WebcamFilterComponent} from '../../components/webcam/webcam-filter/webcam-filter.component';
import {WebcamTitleComponent} from '../../components/webcam/webcam-title/webcam-title.component';
import {TranslateModule} from '@ngx-translate/core';
import {WebcamThumbModule} from '../../shared/webcam-thumb/webcam-thumb.module';
import {AdModule} from '../../shared/ad/ad.module';
import {CountryThumbModule} from '../../shared/country-thumb/country-thumb.module';
import {ModelThumbModule} from '../../shared/model-thumb/model-thumb.module';
import {RouletteThumbModule} from '../../shared/roulette-thumb/roulette-thumb.module';
import {CategoryThumbModule} from '../../shared/category-thumb/category-thumb.module';

const routes: Routes = [
  {path: 'cams', component: WebcamComponent, pathMatch: 'full'},
  {path: '', component: WebcamComponent, pathMatch: 'prefix'}
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        TranslateModule.forChild(),
        WebcamThumbModule,
        CountryThumbModule,
        ModelThumbModule,
        AdModule,
        RouletteThumbModule,
        CategoryThumbModule
    ],
  declarations: [
    WebcamComponent,
    WebcamFilterComponent,
    WebcamTitleComponent,
  ]
})
export class WebcamModule { }
