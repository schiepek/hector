import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModelThumbComponent} from '../../components/model/become-model/model-thumb/model-thumb.component';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild()
  ],
  declarations: [
    ModelThumbComponent
  ],
  exports: [
    ModelThumbComponent
  ]
})
export class ModelThumbModule {
}

