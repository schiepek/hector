import {Cam} from '../model/cam';
import {CamValidator} from '../util/cam-validator';
import {Categorizer} from '../categorizer/categorizer';
import {CamUtil} from '../util/cam-util';

let rp = require('request-promise');

export abstract class Loader {

    private categorizer: Categorizer;
    private isCompleted: boolean = false;
    private readonly provider: string;
    private static readonly TIMEOUT: number = 40000;


    protected constructor(categorizer: Categorizer, provider: string) {
        this.categorizer = categorizer;
        this.provider = provider;
    }

    public loadCams(): Promise<{}[]> {
        return Promise.all(this.getApiUrls().map(url => {
            return new Promise((resolve, reject) => {
                rp(url, {timeout: Loader.TIMEOUT}).then((res: any) => {
                    let cams = this.transformJsonToModel(res);
                    (cams && cams.length > 0) ? resolve(cams) : reject("error: no cams loaded");
                }).catch((error: any) => {
                    let message = "error loading content of url: " + url + " ERROR: " + error;
                    console.error(message, error);
                    reject(message);
                });
            });
        }));
    }

    public complete(): void {
        this.isCompleted = true;
    }

    public isComplete(): boolean {
        return this.isCompleted;
    }

    public getProvider() : string {
        return this.provider;
    }

    private transformJsonToModel(res: any): Cam[] {
        let cams: Cam[] = [];
        let jsonData = this.getJsonDataObject(res);


        for (let i = 0; i < jsonData.length; i++) {
            let cam = this.readProperties(jsonData[i]);
            if (CamValidator.isValid(cam)) {
                cam.categories = this.categorizer.getCamCategories(cam);
                CamUtil.useHttpsForUrls(cam);
                cams.push(cam);
            }

        }
        return cams;
    }

    public abstract getApiUrls(): string[];

    protected abstract getJsonDataObject(res: any): any;

    protected abstract readProperties(camObj: any): Cam;
}
