import {AbstractTest} from '../backend/abstract-test';
import {assert} from 'chai';
import {TestType} from '../general/test-type';
import {parse} from 'node-html-parser';
import {HectorConfig} from '../../../hector-api/src/util/hector-config';


export class CamsUiTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.UI;
    }

    protected initTests(): void {

        this.tests.set("cams-ui", (executor) => {

            let baseUrl = executor.config.getUiRenderUrlProd();

            let providerUrls = [
                baseUrl + "/chaturbate-cams",
                baseUrl + "/bongacams-cams",
                baseUrl + "/camsoda-cams",
                baseUrl + "/livejasmin-cams",
                baseUrl + "/stripchat-cams"
            ];

            providerUrls.forEach(url=> {
                executor.executeRestTest(url, url + ': provider cams site should have valid content', res => {
                    assert.equal(res.status, 200, url + ' does not return status 200');

                    CamsUiTest.assertAllCamsAreLoaded(res.text);
                    CamsUiTest.assertThumbIsComplete(res.text);
                    CamsUiTest.assertHasMultiplePages(res.text);

                })
            })
        });

        this.tests.set("cams-ui-smalltits", (executor) => {

            let url = executor.config.getUiRenderUrlProd() + "/smalltits";

            executor.executeRestTest(url, url + ': small tits cams site should have valid content', res => {
                    assert.equal(res.status, 200, url + ' does not return status 200');

                    CamsUiTest.assertAllCamsAreLoaded(res.text);
                    CamsUiTest.assertThumbIsComplete(res.text);
                    CamsUiTest.assertHasMultiplePages(res.text);

                })
        });


    }

    private static assertAllCamsAreLoaded(html: string): void {
        let doc = parse(html);
        let countryRow = doc.querySelectorAll(".posts")[0];
        assert.equal(HectorConfig.getPageSize(), countryRow.childNodes.length)
    }

    private static assertThumbIsComplete(html: string): void {
        let doc = parse(html);
        let mainRow = doc.querySelectorAll(".posts")[0];
        mainRow.childNodes.forEach(node => {
            let parsedNode = parse(node.toString());
            assert.isNotEmpty(parsedNode.querySelector(".footer-username").innerText,
                "title of cam should be set");
            let imageSrc = parsedNode.querySelector("img").getAttribute("src");
            assert.isNotEmpty(imageSrc);
            assert.isTrue(imageSrc ? CamsUiTest.isImage(imageSrc): false)
        })
    }
    
    private static isImage(imageSrc: string) : boolean {
        let endings = [".jpg", ".png"];
        return endings.some(ending=>imageSrc.includes(ending));
    }


    private static assertHasMultiplePages(html: string) {
        let doc = parse(html);
        let paginationElements = doc.querySelectorAll(".pagination");
        assert.equal(5, paginationElements.length)
    }
}
