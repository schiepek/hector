import {CategoryCode} from '../../../../../hector-api/src/model/category-code';
import {CategoryFunction} from './category-function';
export class Category {

  code: CategoryCode;
  path: string;
  func: CategoryFunction;
  group?: string;

  constructor(code: CategoryCode, path: string, func: CategoryFunction, group: string) {
    this.code = code;
    this.path = path;
    this.func = func;
    this.group = group;
  }

}
