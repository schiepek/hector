import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelThumbComponent } from './model-thumb.component';

describe('ModelThumbComponent', () => {
  let component: ModelThumbComponent;
  let fixture: ComponentFixture<ModelThumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelThumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelThumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
