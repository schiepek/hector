import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {isNumeric} from 'rxjs/internal-compatibility';
import {WebcamService} from '../../services/webcam.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  public page: number;
  public url: string;

  constructor(private cdRef: ChangeDetectorRef,
              private webcam: WebcamService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.webcam.getMenu();
    this.route.params.subscribe(params => this.initPageOrUrl(params["p1"]));
  }

  private initPageOrUrl(param: any) {
    if (param && isNumeric(param)) {
      this.page = param as number;
    } else if (param) {
      this.url = param as string;
    } else {
      this.page = 1;
    }
  }

}
