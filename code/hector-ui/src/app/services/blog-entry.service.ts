import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BlogEntry} from '../shared/model/blog-entry';
import {BlogEntryPage} from '../shared/model/blog-entry-page';

@Injectable({
  providedIn: 'root'
})
export class BlogEntryService {

  private static readonly BLOG_BASE_URL: string = "/assets/blog/"
  private static readonly BLOG_ENTRIES_URL: string = BlogEntryService.BLOG_BASE_URL + "blog_entries_{page}.json"
  private static readonly BLOG_ENTRY_URL: string = BlogEntryService.BLOG_BASE_URL + "{url}/entry.json"

  constructor(private http: HttpClient) {
  }

  public getBlogEntries(page: number): Promise<BlogEntryPage> {
    return this.http
      .get(BlogEntryService.BLOG_ENTRIES_URL.replace("{page}", page.toString()))
      .toPromise()
      .then(response => response as BlogEntryPage)
  }

  public getBlogEntry(blogUrl: string): Promise<BlogEntry> {
    return this.http
      .get(BlogEntryService.BLOG_ENTRY_URL.replace("{url}", blogUrl))
      .toPromise()
      .then(response => response["entry"] as BlogEntry)
  }



}
