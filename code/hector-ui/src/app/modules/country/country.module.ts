import {NgModule} from '@angular/core';
import {CountryComponent} from '../../components/country/country.component';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {ScriptLoaderService} from '../../services/script-loader.service';

const routes: Routes = [
  {path: '', component: CountryComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild()
  ],
  providers: [
    ScriptLoaderService
  ],
  declarations: [
    CountryComponent
  ]
})
export class CountryModule {
}
