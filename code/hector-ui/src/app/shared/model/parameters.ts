export class Parameters {
  public static MAXAGE: string = 'maxage';
  public static MINAGE: string = 'minage';
  public static ISNEW: string = 'isnew';
  public static GENDER: string = 'gender';
  public static ISHD: string = 'ishd';
  public static QUERY: string = 'query';
  public static COUNTRY: string = 'country';
  public static HAIRCOLOR: string = 'haircolor';
  public static BODY: string = 'body';
  public static LANGUAGE: string = 'language';
  public static LOADED: string = 'loaded';
  public static ORDER: string = 'order';
  public static PAGE: string = 'page';
  public static PROVIDER: string = 'provider';
  public static USERNAME: string = 'username';
  public static CATEGORY: string = 'category';
  public static TAG: string = 'tag';
  public static FUNCTION: string = 'function';
  public static PATH: string = "path";
  public static PROJECT: string = "project";
  public static TIMESTAMP: string = 'tsp';
  public static TRACKER: string = 'tr';
  public static FILTER: string = 'filter';
}
