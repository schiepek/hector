export class BlogEntry {
  category: string;
  title: string;
  url: string;
  date: string;
  description: string
}
