import {Injectable} from '@angular/core';
import {MetaService} from './meta.service';
import {Cam} from '../../../../hector-api/src/model/cam';
import {ConstantService} from './constant.service';
import {CountryService} from './country.service';

@Injectable({
  providedIn: 'root'
})
export class SocialMediaService {

  private readonly TWITTER_CARD: string = "twitter:card";
  private readonly TWITTER_SITE: string = "twitter:site";
  private readonly TWITTER_TITLE: string = "twitter:title";
  private readonly TWITTER_DESCRIPTION: string = "twitter:description";
  private readonly TWITTER_IMAGE: string = "twitter:image";
  private readonly SUMMARY: string = "summary";

  constructor(private constant: ConstantService,
              private country: CountryService,
              private meta: MetaService) {
  }

  public updateTwitterCard(cam: Cam): void {
    this.meta.updateTag(this.TWITTER_CARD, this.SUMMARY);
    this.meta.updateTag(this.TWITTER_SITE, cam.username);
    this.meta.updateTag(this.TWITTER_TITLE, this.getTitle(cam));
    this.meta.updateTag(this.TWITTER_DESCRIPTION, this.getDescription(cam));
    this.meta.updateTag(this.TWITTER_IMAGE, cam.imageUrl);
  }

  private getTitle(cam: Cam) : string {
    if(cam.country) {
      return cam.username + " from " + this.country.getName(cam.country) + " LIVE NOW!!"
    }
    return cam.username + " LIVE NOW!!"
  }

  private getDescription(cam: Cam): string {
    return "View Live Sex Show of " + cam.username + " on " + this.constant.getSubDomain();
  }
}
