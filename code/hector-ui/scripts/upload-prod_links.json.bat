set S3BUCKET=%1
set CLOUDFRONT_DISTRIBUTION_ID=%2

echo 'upload links.json to %S3BUCKET%'
call aws s3 cp ../../src/assets/links/links.json %S3BUCKET%/assets/links/links.json --profile hector --acl public-read --cache-control max-age=2592000,public
call aws cloudfront create-invalidation --profile hector --distribution-id %CLOUDFRONT_DISTRIBUTION_ID% --paths /assets/links/*

echo 'upload done'

pause
