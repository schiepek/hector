import {Cam} from '../model/cam';
import {SpecificCamFunction} from './specific-cam-function';

export class CamFunction {

    private readonly queryString: any;
    protected specificFunction?: SpecificCamFunction;

    constructor(queryString: any, specificFunction?: SpecificCamFunction) {
        this.queryString = queryString;
        this.specificFunction = specificFunction;
    }


    public processCams(cams: Cam[]): Cam[] {
        if (this.queryString && this.specificFunction) {
            return this.specificFunction(cams)
        } else if (this.queryString && !this.specificFunction) {
            console.warn("function for query string: " + this.getQueryString() + "isn't defined")
        }
        return cams;
    }

    public getQueryString(): any {
        return this.queryString;
    }

}
