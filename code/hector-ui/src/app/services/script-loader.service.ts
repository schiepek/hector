
import { Injectable } from '@angular/core';

declare var document: any;

@Injectable()
export class ScriptLoaderService {

  private scripts: string[] = [];

  load(...sources: string[]) {
    const promises: any[] = [];
    sources.forEach(src => promises.push(this.loadScript(src)));
    return Promise.all(promises);
  }

  loadScript(src: string) {
    return new Promise((resolve, reject) => {
      if (!(this.scripts.indexOf(src) > -1)) {

        //load script
        let script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = src;
        script.async = false;
        if (script.readyState) {  //IE
          script.onreadystatechange = () => {
            if (script.readyState === "loaded" || script.readyState === "complete") {
              script.onreadystatechange = null;
              this.scripts.push(src);
              resolve({script: name, loaded: true, status: 'Loaded'});
            }
          };
        } else {  //Others
          script.onload = () => {
            this.scripts.push(src);
            resolve({script: name, loaded: true, status: 'Loaded'});
          };
        }
        document.getElementsByTagName('body')[0].appendChild(script);
      } else {
        resolve({ script: name, loaded: true, status: 'Already Loaded' });
      }
    });
  }

}
