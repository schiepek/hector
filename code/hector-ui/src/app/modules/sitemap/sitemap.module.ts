import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SitemapComponent} from '../../components/sitemap/sitemap.component';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {path: '', component: SitemapComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild()
  ],
  declarations: [
    SitemapComponent
  ]
})
export class SitemapModule { }
