import {Cam} from '../model/cam';
import {StringUtil} from './string-util';

export class CamUtil {

    private static readonly fakyTags: string[] = ["asian", "german", "french", "latin", "indian"];
    private static readonly fakyTagCount: number = 3;

    private static readonly http: string = "http://";
    private static readonly https: string = "https://";

    public static containsBasicInfoButNot(cam: Cam, excludes: string[], ...categories: string[]): boolean {
        let contents = this.getBasicContent(cam);

        for (let category of categories) {
            let hasCategory = StringUtil.containsCaseIns(contents, category);
            let hasNoExcludes = !excludes.some(ex => StringUtil.containsCaseIns(contents, ex));
            if (hasCategory && hasNoExcludes) {
                return true;
            }
        }
        return false;
    }

    public static containsBasicInfo(cam: Cam, ...categories: string[]): boolean {
        for (let category of categories) {
            if (StringUtil.containsCaseIns(this.getBasicContent(cam), category)) {
                return true;
            }
        }
        return false;
    }

    public static contentIsNotFaky(cam: Cam): boolean {
        let basicContent = this.getBasicContent(cam);
        return this.fakyTags.filter(t => StringUtil.containsCaseIns(basicContent, t)).length < this.fakyTagCount;
    }

    public static getBasicContent(cam: Cam): string {
        let content = cam.username + cam.title + cam.tags.toString();
        content.replace(/\s/g, '');
        return content;
    }

    public static isSameCountry(left: Cam, right: Cam): boolean {
        if (left.country && right.country) {
            return left.country === right.country;
        }
        return false;
    }

    public static hasAgeOffset(left: Cam, right: Cam, ageOffset: number): boolean {
        let maxAge = left.age + ageOffset;
        let minAge = left.age - ageOffset;
        return right.age >= minAge && right.age <= maxAge;
    }

    public static containsInTags(cam: Cam, ...strings: string[]): boolean {
        for (let str of strings) {
            for (let tag of cam.tags) {
                if (StringUtil.containsCaseIns(tag, str)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static useHttpsForUrls(cam: Cam): void {
        this.isHttpUrl(cam.imageUrl) ? cam.imageUrl = cam.imageUrl.replace(this.http, this.https) : null;
        this.isHttpUrl(cam.chatRoomUrl) ? cam.chatRoomUrl = cam.chatRoomUrl.replace(this.http, this.https) : null;
        this.isHttpUrl(cam.embedUrl) ? cam.embedUrl = cam.embedUrl.replace(this.http, this.https) : null;

    }

    private static isHttpUrl(url: string): boolean {
        return !!url && url.startsWith(this.http);
    }

}