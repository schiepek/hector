import {assert} from 'chai';
import {TestUtil} from '../util/test-util';
import {HectorConfig} from '../../../hector-api/src/util/hector-config';
import {AbstractTest} from './abstract-test';
import {TestType} from '../general/test-type';


export class ApiTest extends AbstractTest {

    private readonly minimumCams: number = 10;

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {

        this.tests.set("api-chaturbate", (executor) => {
            HectorConfig.getChaturbateUrls().forEach(url => {

                executor.executeRestJsonTest(url, 'chaturbate should return json data in the expected format', res => {

                    assert.isTrue(res.length > this.minimumCams, "The cams are not loaded or there are not enough");

                    res.forEach((r: any) => {
                        [
                            new ApiTester(r["is_new"], "is_new", v => TestUtil.isBoolean(v)),
                            new ApiTester(r["num_followers"], "num_followers", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(r["iframe_embed"], "iframe_embed", v => TestUtil.isIframe(v)),
                            new ApiTester(r["display_name"], "display_name", v => TestUtil.isString(v)),
                            new ApiTester(r["tags"], "tags", v => TestUtil.isArray(v)),
                            new ApiTester(r["iframe_embed_revshare"], "iframe_embed_revshare", v => TestUtil.isIframe(v)),
                            new ApiTester(r["location"], "location", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["chat_room_url_revshare"], "chat_room_url_revshare", v => TestUtil.isUrl(v)),
                            new ApiTester(r["username"], "username", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["spoken_languages"], "spoken_languages", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["current_show"], "current_show", v => TestUtil.isOneOfStrings(["private", "public", "hidden", "away", "group"], v)),
                            new ApiTester(r["is_hd"], "is_hd", v => TestUtil.isBoolean(v)),
                            new ApiTester(r["seconds_online"], "seconds_online", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(r["gender"], "gender", v => TestUtil.isOneOfStrings(["m", "f", "s", "c"], v)),
                            new ApiTester(r["age"], "age", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(r["num_users"], "num_users", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(r["image_url"], "image_url", v => TestUtil.isImageUrl(v)),
                            new ApiTester(r["room_subject"], "room_subject", v => TestUtil.isString(v))
                        ].forEach(t => t.test());
                    })
                })
            });
        });


        this.tests.set("api-bongacams", (executor) => {
            HectorConfig.getBongacamsUrls().forEach(url => {

                executor.executeRestJsonTest(url, 'bongacams should return json data in the expected format', res => {

                    assert.isTrue(res.length > this.minimumCams, "The cams are not loaded or there are not enough");

                    res.forEach((r: any) => {
                        [
                            new ApiTester(r["username"], "username", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["display_name"], "display_name", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["display_age"], "display_age", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(r["profile_images"]["thumbnail_image_big_live"], "profile_images.thumbnail_image_big_live", v => TestUtil.isImageUrl(v)),
                            new ApiTester(r["chat_url"], "chat_url", v => TestUtil.isUrl(v)),
                            new ApiTester(r["chat_url_on_home_page"], "chat_url_on_home_page", v => TestUtil.isUrl(v)),
                            new ApiTester(r["direct_chat_url"], "direct_chat_url", v => TestUtil.isUrl(v)),
                            new ApiTester(r["profile_page_url"], "profile_page_url", v => TestUtil.isUrl(v)),
                            new ApiTester(r["online_time"], "online_time", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(r["chat_status"], "chat_status", v => TestUtil.isOneOfStrings(["private", "public"], v)),
                            new ApiTester(r["status"], "status", v => TestUtil.isBoolean(v)),
                            new ApiTester(r["hometown"], "hometown", v => TestUtil.isStringOrNull(v)),
                            new ApiTester(r["tags"], "tags", v => TestUtil.isArrayNotEmpty(v)),
                            new ApiTester(r["turns_on"], "turns_on", v => TestUtil.isStringOrNull(v)),
                            new ApiTester(r["turns_off"], "turns_off", v => TestUtil.isStringOrNull(v)),
                            new ApiTester(r["signup_date"], "signup_date", v => TestUtil.isDate(v)),
                            new ApiTester(r["last_update_date"], "last_update_date", v => TestUtil.isDate(v)),
                            new ApiTester(r["members_count"], "members_count", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(r["primary_language_key"], "primary_language_key", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["gender"], "gender", v => TestUtil.strContainsOneOfStrings(v, ["male", "female", "couple", "trans"])),
                            new ApiTester(r["hair_color"], "hair_color", v => TestUtil.strContainsOneOfStringsOrNull(v, ["blonde", "brunette", "redhead", "black"])),
                            new ApiTester(r["primary_language"], "primary_language", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["embed_chat_url"], "embed_chat_url", v => TestUtil.isUrl(v))

                        ].forEach(t => t.test());
                    })
                })
            });
        });

        this.tests.set("api-awempire", (executor) => {
            HectorConfig.getAwEmpireUrls().forEach(url => {

                executor.executeRestJsonTest(url, 'awempire should return json data in the expected format', res => {

                    let models = res["data"]["models"];
                    assert.isTrue(models.length > this.minimumCams, "The cams are not loaded or there are not enough");

                    models.forEach((r: any) => {
                        let d = r["details"];
                        let a = d["about"];
                        let p = r["persons"][0];
                        let b = p["body"];

                        [
                            new ApiTester(r["performerId"], "performerId", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["status"], "status", v => TestUtil.isOneOfStrings(["private_chat", "free_chat", "member_chat", "members_only", "vip_show", "offline"], v)),
                            new ApiTester(r["category"], "category", v => TestUtil.isString(v)),
                            new ApiTester(r["country"], "country", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["chatRoomUrl"], "chatRoomUrl", v => TestUtil.isUrl(v)),
                            new ApiTester(d["streamQuality"], "streamQuality", v => TestUtil.isPositiveNumber(v) && Number(v) < 10),
                            new ApiTester(a["biography"], "biography", v => TestUtil.isString(v)),
                            new ApiTester(a["turnsOn"], "biography", v => TestUtil.isString(v)),
                            new ApiTester(a["turnsOff"], "biography", v => TestUtil.isString(v)),
                            new ApiTester(d["languages"], "languages", v => TestUtil.isArray(v)),
                            new ApiTester(d["willingnesses"], "willingnesses", v => TestUtil.isArray(v) || TestUtil.isStringOrNull(v)),
                            new ApiTester(p["sex"], "sex", v => TestUtil.isOneOfStrings(["female", "male", "shemale", "couple"], v)),
                            new ApiTester(p["age"], "age", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(b["build"], "build", v => (v) ? TestUtil.isOneOfStrings(["slim", "above average", "average", "athletic", "petite", "obese", "curvy"], v) : true),
                            new ApiTester(b["hairColor"], "hairColor", v => (v) ? TestUtil.isOneOfStrings(["blonde", "auburn", "brown", "black", "other", "pink", "fire red", "orange"], v) : true),
                        ].forEach(t => t.test());
                    })
                })
            });
        });

        this.tests.set("api-camsoda", (executor) => {
            HectorConfig.getCamSodaUrls().forEach(url => {

                executor.executeRestJsonTest(url, 'camsoda should return json data in the expected format', res => {

                    let results = res["results"];

                    assert.isTrue(results.length > this.minimumCams, "The cams are not loaded or there are not enough");

                    results.forEach((r: any) => {
                        [
                            new ApiTester(r["is_new"], "is_new", v => TestUtil.isBoolean(v)),
                            new ApiTester(r["viewers"], "viewers", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(r["link_iframe"], "link_iframe", v => TestUtil.isUrl(v)),
                            new ApiTester(r["name"], "display_name", v => TestUtil.isString(v)),
                            new ApiTester(r["tags"], "tags", v => TestUtil.isArray(v) || TestUtil.isObject(v)),
                            new ApiTester(r["link"], "link", v => TestUtil.isUrl(v)),
                            new ApiTester(r["username"], "username", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["gender"], "gender", v => TestUtil.isOneOfStrings(["m", "f", "t", "c"], v)),
                            new ApiTester(r["status"], "status", v => TestUtil.isOneOfStrings(["online", "private", "limited"], v)),
                            new ApiTester(r["thumb"], "thumb", v => TestUtil.isImageUrl(v)),
                            new ApiTester(r["subject"], "subject", v => TestUtil.isString(v))
                        ].forEach(t => t.test());
                    })
                })
            });
        });

        this.tests.set("api-stripchat", (executor) => {
            HectorConfig.getStripchatUrls().forEach(url => {

                executor.executeRestJsonTest(url, 'stripchat should return json data in the expected format', res => {

                    let results = res["models"];

                    assert.isTrue(results.length > this.minimumCams, "The cams are not loaded or there are not enough");

                    results.forEach((r: any) => {
                        [
                            new ApiTester(r["username"], "username", v => TestUtil.isStringNotEmpty(v)),
                            new ApiTester(r["profileBirthDate"], "profileBirthDate", v => TestUtil.isDateOrNull(v)),
                            new ApiTester(r["status"], "status", v => TestUtil.isOneOfStrings(["public", "private", "groupShow", "p2p", "virtualPrivate", "p2pVoice"], v)),
                            new ApiTester(r["isNew"], "isNew", v => TestUtil.isBoolean(v)),
                            new ApiTester(r["viewersCount"], "viewersCount", v => TestUtil.isPositiveNumber(v)),
                            new ApiTester(r["gender"], "gender", v => TestUtil.isOneOfStrings(["male", "female", "tranny", "maleFemale", "males", "females",
                                "trannies", "maleTranny", "femaleTranny"], v)),
                            new ApiTester(r["broadcastHD"], "broadcastHD", v => TestUtil.isBoolean(v)),
                            new ApiTester(r["widgetPreviewUrl"], "widgetPreviewUrl", v => TestUtil.isUrl(v)),
                            new ApiTester(r["goalMessage"], "goalMessage", v => TestUtil.isStringOrNull(v)),
                            new ApiTester(r["profileEthnicity"], "profileEthnicity", v => TestUtil.isOneOfStringsOrNull(["ethnicityWhite", "ethnicityAsian", "ethnicityIndian",
                                "ethnicityLatino", "ethnicityEbony", "ethnicityMiddleEastern", "ethnicityMixed"], v)),
                            new ApiTester(r["profileSpecifics"], "profileSpecifics", v => TestUtil.arrayContainsOneOfStringsOrNull(["specificsBigAss", "specificsBigTits", "specificShaven",
                                "specificsHairy", "specificSmallTits", "specificsBigCock", "specificsTattoos", "specificsPiercing", "specificTrimmed",
                                "specificFtM", "specificHairyMan", "specificTG", "specificUncut", "specificShemale", "specificTS", "specificTV", "specificMtF", "specificCD"], v)),
                            new ApiTester(r["languages"], "languages", v => TestUtil.isArray(v)),
                            new ApiTester(r["modelsCountry"], "modelsCountry", v => TestUtil.isStringOrNull(v)),
                            new ApiTester(r["profileHairColor"], "profileHairColor", v => TestUtil.isOneOfStringsOrNull(["hairColorBlack", "hairColorBlonde", "hairColorRed",
                                "hairColorOther", "hairColorColorful", "specificsBigCock", "hairColorHairless"], v)),
                            new ApiTester(r["profileBodyType"], "profileBodyType", v => TestUtil.isOneOfStringsOrNull(["bodyTypeThin", "bodyTypeCurvy", "bodyTypeAthletic",
                                "bodyTypeAverage", "bodyTypeLarge"], v)),
                        ].forEach(t => t.test());
                    })
                })
            });
        });

    }
}

export class ApiTester {

    private value: any;
    private message: string;
    private testFunc: TestBoolFunction;


    constructor(value: any, selector: string, testFunc: TestBoolFunction) {
        this.value = value;
        this.message = selector + " had not the right format: (" + value + ")";
        this.testFunc = testFunc;
    }

    public test(): void {
        assert.isTrue(this.testFunc(this.value), this.message);
    }

}

export interface TestBoolFunction {
    (val: any): boolean;
}
