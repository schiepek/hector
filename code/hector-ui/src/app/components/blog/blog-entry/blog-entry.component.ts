import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {BlogEntryService} from '../../../services/blog-entry.service';
import {BlogEntry} from '../../../shared/model/blog-entry';
import {ComponentReloader} from '../../../shared/util/component-reloader';
import {ClientUtil} from '../../../shared/util/client-util';
import {provider} from '../../../../../../hector-api/src/model/provider';
import {ConstantService} from '../../../services/constant.service';
import {StateService} from '../../../services/state.service';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {MetaService} from '../../../services/meta.service';


@Component({
  selector: 'app-blog-entry',
  templateUrl: './blog-entry.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./blog-entry.component.css']
})
export class BlogEntryComponent implements OnInit {

  public entry: BlogEntry;

  @Input()
  public url: string
  public currentUrl: string;
  public currentAssetsFolder: string;
  public externalDoc: any;

  constructor(
    private http: HttpClient,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private location: Location,
    private state: StateService,
    private constant: ConstantService,
    private blogEntry: BlogEntryService,
    private meta: MetaService,
    private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.blogEntry.getBlogEntry(this.url).then(entry => {
      this.entry = entry;
      this.currentUrl = ClientUtil.isClientSide() && window.location.href;
      this.currentAssetsFolder = "./assets/blog/" + entry.url;
      this.meta.setMetaTag(entry.title, entry.description);
      this.http.get(this.currentAssetsFolder + "/index.html", {responseType: 'text'}).subscribe(data => this.setVariables(data));
    });
  }

  private setVariables(data: any): void {
    this.state.getTracker(this.route, this.location).then(tracker => {
      this.externalDoc = this.translate.parser.interpolate(data, this.getInterpolations(tracker));
      ComponentReloader.detectChanges(this.cdRef)
    });
  }

  private getInterpolations(tracker: string): any {
    return {
      chaturbateSignupUrl: this.constant.getSignupUrl(provider.CHATURBATE, tracker),
      bongacamsSignupUrl: this.constant.getSignupUrl(provider.BONGACAMS, tracker),
      livejasminSignupUrl: this.constant.getSignupUrl(provider.AWEMPIRE, tracker),
      camsodaSignupUrl: this.constant.getSignupUrl(provider.CAMSODA, tracker),
      stripchatSignupUrl: this.constant.getSignupUrl(provider.STRIPCHAT, tracker),
      chaturbateModelSignupUrl: this.constant.getModelSignupUrl(provider.CHATURBATE, tracker),
      bongacamsModelSignupUrl: this.constant.getModelSignupUrl(provider.BONGACAMS, tracker),
      livejasminModelSignupUrl: this.constant.getModelSignupUrl(provider.AWEMPIRE, tracker),
      camsodaModelSignupUrl: this.constant.getModelSignupUrl(provider.CAMSODA, tracker),
      stripchatModelSignupUrl: this.constant.getModelSignupUrl(provider.STRIPCHAT, tracker),
      currentAssetsFolder: this.currentAssetsFolder,
      twitterLink: this.constant.TWITTER_LINK,
      website: this.constant.getWebsite(),
      domain: this.constant.getDomain(),
      aboutUsImg: "assets/images_hector/logo/" + this.constant.getProject() + "/about_us.png"
    }
  }

}
