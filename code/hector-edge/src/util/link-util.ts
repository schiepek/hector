export class LinkUtil {

    private static readonly noTracker = "no-tracker";
    private static readonly tracker = "tracker";
    private static readonly trackerVariable = "{{TRACKER}}";

    public static getLink(request: any, response: any): string {
        let type = request.uri.split('/').pop() as string | "";
        let tracker = request.querystring ? request.querystring.split('tr=').pop() : undefined;
        return LinkUtil.getFinalLink(JSON.parse(response), type, tracker);
    }

    private static getFinalLink(jsonResponse: any, type: string, tracker: string | undefined): string {
        let category = tracker ? LinkUtil.tracker : LinkUtil.noTracker;
        let rareLink = jsonResponse[category][type];
        return tracker ? rareLink.replace(LinkUtil.trackerVariable, tracker) : rareLink;
    }

}