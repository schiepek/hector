export class Partner {

  public link: string;
  public text: string;
  public type: string;
  public page: string;
  public project: string;

}
