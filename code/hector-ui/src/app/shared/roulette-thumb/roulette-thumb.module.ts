import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouletteThumbComponent } from '../../components/roulette/roulette-thumb/roulette-thumb.component';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [RouletteThumbComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild()
  ],
  exports: [
    RouletteThumbComponent
  ]
})
export class RouletteThumbModule { }
