import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WebcamThumbComponent} from '../../components/webcam/webcam-thumb/webcam-thumb.component';
import {TranslateModule} from '@ngx-translate/core';
import {RouterModule} from '@angular/router';
import {InViewportModule} from 'ng-in-viewport';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    RouterModule,
    InViewportModule
  ],
  declarations: [
    WebcamThumbComponent
  ],
  exports: [
    WebcamThumbComponent
  ]
})
export class WebcamThumbModule { }
