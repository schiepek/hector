User-agent: *
Sitemap: https://www.canadiansexcams.com/sitemap.xml

#Template Files
Allow: /assets/*
Allow: /php/*

#Cam
Allow: /chaturbate/*
Allow: /bongacams/*
Allow: /livejasmin/*
Allow: /myfreecams/*

#Topcam
Allow: /topcam/*

#Footer
Disallow: /privacy
Disallow: /terms
Disallow: /2257
Disallow: /copyright

#Search
Disallow: /search/*

#Categories
Disallow: /*/*
