import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {WebcamService} from '../../services/webcam.service';
import {MetaService} from '../../services/meta.service';
import {StateService} from '../../services/state.service';
import {ConstantService} from '../../services/constant.service';
import {TranslateService} from '@ngx-translate/core';
import {provider} from '../../../../../hector-api/src/model/provider';
import {Location} from '@angular/common';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {


  private readonly path: string = "./assets/content";
  private readonly ending: string = ".html";

  public title: string;
  public externalDoc: any;

  constructor(private webcam: WebcamService,
              private router: Router,
              private state: StateService,
              private meta: MetaService,
              private location: Location,
              private route: ActivatedRoute,
              private http: HttpClient,
              private constant: ConstantService,
              private translate: TranslateService) {
    this.state.reset();
    this.meta.setFooterMetaTags(this.getUrl().replace('/', ''));
  }

  ngOnInit(): void {
    this.webcam.getMenu();
    let file = this.path + this.getUrl() + this.getUrl() + this.ending;
    this.http.get(file, {responseType: 'text'}).subscribe(data => this.setVariables(data));
  }

  private setVariables(data: any): void {
    this.state.getTracker(this.route, this.location).then(tracker => {
      this.externalDoc = this.translate.parser.interpolate(data, this.getInterpolations(tracker));
      this.title = new DOMParser().parseFromString(this.externalDoc, 'text/html').getElementById("title").innerText;
    });
  }

  private getInterpolations(tracker: string): any {
    return {
      chaturbateSignupUrl: this.constant.getSignupUrl(provider.CHATURBATE, tracker),
      bongacamsSignupUrl: this.constant.getSignupUrl(provider.BONGACAMS, tracker),
      livejasminSignupUrl: this.constant.getSignupUrl(provider.AWEMPIRE, tracker),
      camsodaSignupUrl: this.constant.getSignupUrl(provider.CAMSODA, tracker),
      stripchatSignupUrl: this.constant.getSignupUrl(provider.STRIPCHAT, tracker),
      twitterLink: this.constant.TWITTER_LINK,
      website: this.constant.getWebsite(),
      domain: this.constant.getDomain(),
      aboutUsImg: "assets/images_hector/logo/" + this.constant.getProject() + "/about_us.png"
    }
  }

  private getUrl() {
    return this.router.url.indexOf('?') >= 0 ? this.router.url.substr(0, this.router.url.indexOf('?')) : this.router.url;
  }

}
