export class EventUtil {

  private static readonly ARROW_KEYS: string[] = ["ArrowDown", "ArrowUp", "ArrowRight", "ArrowLeft"];
  private static readonly ENTER_KEY: string = "Enter";
  private static readonly ESCAPE_KEY: string = "Escape";

  public static isArrowOrEnterOrEsc(event: KeyboardEvent): boolean {
    if (event && event.key) {
      let hasArrow = EventUtil.ARROW_KEYS.includes(event.key);
      let hasEnter = event.key == EventUtil.ENTER_KEY;
      let hasEscape = event.key == EventUtil.ESCAPE_KEY;
      return hasArrow || hasEnter || hasEscape;
    }
    return false;
  }

  public static relatedTargetMatches(event: FocusEvent, id: string) {
    return event && event.relatedTarget && event.relatedTarget["id"] == id;
  }

}
