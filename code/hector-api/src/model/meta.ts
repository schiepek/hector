export class Meta{
    public static readonly S3_NULL: string = "s_null";
    public static readonly S3_EXP: string = "s_exp";
    public static readonly MEMORY: string = "m";
    public static readonly WARMUP: string = "w";
}
