export class AllPagination {

  public paginations: Pagination[];
  public basicUrl: string;

  constructor(paginations: Pagination[], basicUrl: string) {
    this.paginations = paginations;
    this.basicUrl = basicUrl;
  }
}

export class Pagination {
  page: number;
  isPage: boolean;
  isActive: boolean;

  constructor(page: number, isPage: boolean, currentPage: number) {
    this.page = page;
    this.isPage = isPage;
    this.isActive = page === currentPage;
  }

}
