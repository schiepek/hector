import { WebcamTagcloudModule } from './webcam-tagcloud.module';

describe('WebcamTagcloudModule', () => {
  let webcamTagcloudModule: WebcamTagcloudModule;

  beforeEach(() => {
    webcamTagcloudModule = new WebcamTagcloudModule();
  });

  it('should create an instance', () => {
    expect(webcamTagcloudModule).toBeTruthy();
  });
});
