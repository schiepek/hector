import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnInit} from '@angular/core';
import {StringUtil} from '../../shared/util/string-util';
import {ComponentReloader} from '../../shared/util/component-reloader';
import {CategoryService} from '../../services/category.service';
import {StateService} from '../../services/state.service';
import {WebcamService} from '../../services/webcam.service';
import {CountryService} from '../../services/country.service';
import {CategoryUI} from '../../shared/model/CategoryUI';
import {ScreenService} from '../../services/screen.service';
import {ScreenSize} from '../../shared/model/screen-size';
import {MetaService} from '../../services/meta.service';
import {CategoryItemType} from '../../../../../hector-api/src/model/category-item-type';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  private static readonly NON_LETTER_TAG = "#";
  public readonly filteredKey = "filtered";

  public tags: Map<string, CategoryUI[]> = new Map<string, CategoryUI[]>();
  public tagKeys: string[];
  public sortedCategories: CategoryUI[];
  public currentTags: Map<string, CategoryUI[]> = new Map<string, CategoryUI[]>();

  public filter: string;
  public filterTranslationKey: string;
  public startsWith: boolean = false;
  public filteredTags: CategoryUI[];
  public filterTerm: string = "";

  private screenSize: ScreenSize;

  constructor(public screen: ScreenService,
              private cdRef: ChangeDetectorRef,
              private meta: MetaService,
              private state: StateService,
              private webcam: WebcamService,
              private route: ActivatedRoute,
              private categoryS: CategoryService,
              public country: CountryService) { }

  ngOnInit() {
    this.state.reset();
    this.screenSize = this.screen.getScreenSize();
    this.route.params.subscribe(params => this.initPageAndCategories(params["p1"]));


  }

  private initPageAndCategories(filter?: string) : void {
    this.initFilterKey(filter);
    this.webcam.getCategories(filter).then(result => {
      this.meta.setCategoriesMetaTags(filter);
      this.country.initAllCountries(result.countries);
      this.sortedCategories = this.categoryS.prepareLiveCategories(result, this.country.allCountries);
      this.fillTags(this.sortedCategories);
    })
  }

  private initFilterKey(filter?: string) : void {
    this.filter= filter;
    this.filterTranslationKey = filter ? "categories." + filter : "category-page.all-gender";
  }


  private fillTags(sortedCategories: CategoryUI[]): void {
    for (let cat of sortedCategories) {
      if (!StringUtil.startsWithAlphanumeric(cat.name)) {
        CategoryComponent.pushInto2dimensionalMap(this.tags, CategoryComponent.NON_LETTER_TAG, cat);
      } else {
        let firstChar = cat.name.charAt(0).toUpperCase();
        CategoryComponent.pushInto2dimensionalMap(this.tags, firstChar, cat);
      }
    }
    this.tagKeys = Array.from(this.tags.keys());
    this.resetCurrentTagsForAll();
    ComponentReloader.detectChanges(this.cdRef);
  }

  public static pushInto2dimensionalMap(map: Map<string, CategoryUI[]>, key: string, value: CategoryUI) {
    let arr = map.get(key);
    if (!arr) {
      let array: CategoryUI[] = [value];
      map.set(key, array);
    } else {
      map.get(key).push(value);
    }
  }

  public keyUpInput(event: any, startsWith: boolean = false) {
    this.keyUp(event.target.value ? event.target.value : null, startsWith)
  }

  public keyUp(key: string, startsWith: boolean = false): void {
    this.startsWith = startsWith;
    this.filterTerm = key;
    if (this.filterTerm) {
      this.filteredTags = this.sortedCategories
        .filter(t => CategoryComponent.matchesFilter(t.name, this.filterTerm, startsWith));
      this.currentTags = new Map([
        [this.filteredKey, this.filteredTags]
      ]);
    } else {
      this.resetCurrentTagsForAll();
    }
    ComponentReloader.detectChanges(this.cdRef);
  }

  private static matchesFilter(key: string, filterTerm: string, startsWith: boolean) {
    let left = key.toLowerCase();
    let right = filterTerm.toLowerCase();

    if (startsWith && right === CategoryComponent.NON_LETTER_TAG) {
      return left.match(/^\d/) //starts with number
    } else if (startsWith) {
      return left.startsWith(right);
    }
    return left.includes(right);
  }

  private resetCurrentTagsForAll(): void {
    this.currentTags = this.tags;
  }

  public getNrOfRows(listSize: number): number {
    let rowsDec = listSize / this.getColumnSize();
    return (rowsDec % 1 != 0) ? Math.floor(rowsDec) + 1 : rowsDec;
  }

  public isCountry(tag: CategoryUI): boolean {
    return tag.type === CategoryItemType.COUNTRY;
  }

  private getColumnSize(): number {
    switch (this.screenSize) {
      case ScreenSize.XL:
        return 5;
      case ScreenSize.LG:
        return 4;
      case ScreenSize.MD:
        return 3;
      default:
        return 2;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    let newScreenSize = this.screen.getScreenSize();
    if (newScreenSize != this.screenSize) {
      this.screenSize = newScreenSize;
      if (this.filterTerm) {
        this.keyUp(this.filterTerm, this.startsWith);
      } else {
        this.tags = new Map<string, CategoryUI[]>();
        this.fillTags(this.sortedCategories);
      }
    }
  }

}
