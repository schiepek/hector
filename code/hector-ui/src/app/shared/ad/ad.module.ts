import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdComponent} from '../../components/ad/ad.component';
import {PipeModule} from '../pipe/pipe.module';
import {TranslateModule} from '@ngx-translate/core';
import {InViewportModule} from 'ng-in-viewport';

@NgModule({
  imports: [
    CommonModule,
    PipeModule,
    TranslateModule.forChild(),
    InViewportModule
  ],
  declarations: [
    AdComponent
  ],
  exports: [
    AdComponent
  ]
})
export class AdModule { }
