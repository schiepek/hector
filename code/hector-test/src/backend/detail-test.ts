import 'mocha';
import chai = require('chai');
import {assert} from 'chai';
import {AbstractTest} from './abstract-test';
import {CamUtil} from '../../../hector-api/src/util/cam-util';
import {TestUtil} from '../util/test-util';
import {provider} from '../../../hector-api/src/model/provider';
import {HectorConfig} from '../../../hector-api/src/util/hector-config';
import {TestType} from '../general/test-type';

export class DetailTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {

        this.tests.set("detail-cam", (executor, config) => {

            [provider.CHATURBATE, provider.BONGACAMS, provider.CAMSODA, provider.STRIPCHAT].forEach(p => {
                it('should only return a detailed cam with provider ' + p, (done) => {
                    let parameters = {page: 1, provider: p, 'function': config.getCamsFunction()};
                    chai.request(config.getEndpoint()).get(config.getMainPath()).query(parameters)
                        .end((err, res) => {
                            let r = JSON.parse(res.text);

                            let username = r.cams[0].username;
                            let provider = r.cams[0].provider;
                            let parameters = {
                                username: username,
                                provider: provider,
                                'function': config.getCamFunction()
                            };

                            chai.request(config.getEndpoint()).get(config.getMainPath()).query(parameters)
                                .end((err, res) => {
                                    let r = JSON.parse(res.text);
                                    assert.equal(r.cams.length, 10, "the size of the detail cam should be 10 (1 + 9)");
                                    assert.equal(r.cams[0].username, username, "the username of the detail cam should be " + username);
                                    assert.equal(r.cams[0].provider, provider, "the provider of the detail cam should be " + provider);
                                    assert.isTrue(TestUtil.isHttpsUrl(r.cams[0].embedUrl), "Embed Url has to be https");
                                    assert.isTrue(TestUtil.isHttpsUrl(r.cams[0].imageURL), "Image Url has to be https");
                                    assert.isTrue(TestUtil.isHttpsUrl(r.cams[0].chatRoomUrl), "Chatroom Url has to be https");
                                    assert.equal(p, provider, "the provider should be the same as the test parameter: " + p);
                                    done();
                                });
                        });
                });

            });
        });


        this.tests.set("detail-relatedcams", (executor, config) => {
            it('should return a detailed cam with related cams', (done) => {
                let parameters = {page: 1, country: "co", minage: 20, maxage: 20, 'function': config.getCamsFunction()};
                chai.request(config.getEndpoint()).get(config.getMainPath()).query(parameters)
                    .end((err, res) => {
                        let r = JSON.parse(res.text);

                        let username = r.cams[0].username;
                        let provider = r.cams[0].provider;
                        let parameters = {username: username, provider: provider, 'function': config.getCamFunction()};

                        chai.request(config.getEndpoint()).get(config.getMainPath()).query(parameters)
                            .end((err, res) => {
                                let r = JSON.parse(res.text);
                                assert.equal(r.cams.length, 10, "the size of the detail cam should be 10 (1 + 9 related cams)");

                                for (let i = 1; i < r.cams.length; i++) {
                                    assert.isTrue(r.cams[i].username !== username, "the related cams should not be the same as the detail cam");
                                    assert.isTrue(r.cams[i].country === "co", "the related cams country should be colombian");
                                    assert.isTrue(CamUtil.hasAgeOffset(r.cams[0], r.cams[i], 3), "the age offset should be maximum 3 years");
                                }
                                done();
                            });
                    });
            });
        });

        this.tests.set("detail-offline", (executor, config) => {
            executor.executeRestTestFunction(config.getCamFunction(),'should return the first page of cams (cause the detail cam we wanted was offline)', {
                    username: "notExistingUsername",
                    provider: "chaturbate"
                },
                r => {
                    assert.isTrue(r.cams.length === HectorConfig.getPageSize(), "Offline cams size should have page size");
                }
            );
        });



    }

}
