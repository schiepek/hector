export class DateUtil {

    private static readonly TwentyFourHoursInMilliseconds = 86400000;

    public static dateIsNewerThan10Days(date: Date): boolean {
        return date > this.addDays(new Date(), -10);
    }

    public static addDays(date: Date, days: number): Date {
        return new Date(date.getTime() + days * this.TwentyFourHoursInMilliseconds);
    }
}