import {Component, OnInit, ViewChild} from '@angular/core';
import {WebcamService} from '../../services/webcam.service';
import {CountryService} from '../../services/country.service';
import {CategoryService} from '../../services/category.service';
import {StateService} from '../../services/state.service';
import {Cam} from '../../../../../hector-api/src/model/cam';
import {ActivatedRoute, Router} from '@angular/router';
import {CamResult} from '../../../../../hector-api/src/model/cam-result';
import {MetaService} from '../../services/meta.service';
import {LinkService} from '../../services/link.service';
import {AdService} from '../../services/ad.service';
import {ScreenService} from '../../services/screen.service';
import {RouterUtil} from '../../shared/util/router-util';
import {provider} from '../../../../../hector-api/src/model/provider';
import {ConstantService} from '../../services/constant.service';
import {ClientUtil} from '../../shared/util/client-util';
import {Location} from '@angular/common';
import {SocialMediaService} from '../../services/social-media.service';
import {DetailTitleComponent} from './detail-title/detail-title.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  @ViewChild(DetailTitleComponent, {static: false})
  public titleComponent: DetailTitleComponent

  private static readonly MAX_LOADED_CAMS_SIZE : number = 5;

  public cam: Cam;
  public relatedCams: Cam[] = [];
  public embedUrl: string;
  public signupUrl: string;
  public modelSignupUrl: string;
  public isOffline: boolean;

  public isRandom: boolean;
  private loadedCams: CamResult[] = [];
  private randomCamIndex: number = -1;

  public countryName: string;
  public countryUrl: string;

  constructor(private router: Router,
              private link: LinkService,
              private location: Location,
              public constant: ConstantService,
              public meta: MetaService,
              public ad: AdService,
              public screen: ScreenService,
              public webcam: WebcamService,
              public countryS: CountryService,
              public categoryS: CategoryService,
              private socialMedia: SocialMediaService,
              public state: StateService,
              public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.state.reset();
    this.link.setCanonicalUrl();
    this.initParamsAndWebCam();
  }

  private initParamsAndWebCam(): void {
    this.route.parent.url.subscribe(urlSegments => {
      this.route.params.subscribe(p => {
        if (urlSegments[0].path === 'topcam') {
          this.initTopCams(p['p1']);
        } else if (urlSegments[0].path === 'roulette') {
          this.initRandomCams();
        } else {
          this.initStateAndCam(p['username'], urlSegments[0].path);
          this.webcam.getCam().then(res => this.initWebcam(res.cams[0], res));
        }
      });
    });
  }

  private initRandomCams() : void {
    this.isRandom = true;
    this.meta.setRandomMetaTags();
    this.loadRandomCams();
  }

  private loadRandomCams(): void {
      this.webcam.getRandomCam(this.toLoadedCamsString()).then(res => {
        this.state.username = res.cams[0] ? res.cams[0].username : null;
        this.state.provider = res.cams[0] ? res.cams[0].provider : null;
        this.loadedCams.push(res);
        this.randomCamIndex = this.loadedCams.length - 1;
        this.shiftLoadedCams();
        this.initWebcam(res.cams[0], res)
      });
  }

  public hasPreviousButton(): boolean {
    return this.loadedCams.length > 1;
  }

  public nextRandomCam(): void {
    if(this.randomCamIndex === -1 || this.randomCamIndex === this.loadedCams.length-1) {
      this.loadRandomCams();
    } else {
      this.randomCamIndex += 1;
      this.thisLoadFromLoadedCams();
    }
  }

  public previousRandomCam(): void {
    if(this.randomCamIndex > 0) {
      this.randomCamIndex -= 1;
      this.thisLoadFromLoadedCams();
    }
  }

  private thisLoadFromLoadedCams(): void {
    let res = this.loadedCams[this.randomCamIndex];
    this.state.username = res.cams[0] ? res.cams[0].username : null;
    this.state.provider = res.cams[0] ? res.cams[0].provider : null;
    this.initWebcam(res.cams[0], res)
  }

  get nextRandomCamFunc() {
    return this.nextRandomCam.bind(this);
  }

  get hasPreviousButtonFunc() {
    return this.hasPreviousButton.bind(this);
  }

  get previousRandomCamFunc() {
    return this.previousRandomCam.bind(this);
  }

  private initTopCams(categoryParameter: string): void {
    let category = categoryParameter ? this.categoryS.findCategory(categoryParameter, undefined) : null;
    category ? category.func(this.state) : null;
    this.webcam.getCams().then(result => {
      if (result.cams.length === 0) {
        RouterUtil.goToHome(this.router);
      } else {
        let topCam = result.cams.find(c => !c.isPrivate);
        this.initStateAndCam(topCam.username, topCam.provider);
        this.initWebcam(topCam, result);
      }
    });
  }

  private initStateAndCam(username: string, prov: string): void {
    this.state.username = username;
    this.state.provider = prov;
    this.meta.setDetailMetaTags(this.state.username, this.state.provider);
  }

  private initWebcam(cam: Cam, res: CamResult): void {
    if (this.camIsNotOffline(res, cam)) {
      this.setSignUpUrlAndCam(cam);
    } else {
      this.isOffline = true;
    }
    if (res.cams.length === 0) {
      RouterUtil.goToHome(this.router);
    } else {
      this.setRelatedCams(res);
    }
  }

  private camIsNotOffline(res: CamResult, cam: Cam) {
    return res.cams.length > 0 && cam.username === this.state.username && cam.provider === this.state.provider;
  }

  private setSignUpUrlAndCam(cam: Cam) {
    this.state.getTracker(this.route, this.location).then(tracker => {
      this.signupUrl = this.constant.getSignupUrl(cam.provider, tracker);
      this.modelSignupUrl = this.constant.getModelSignupUrl(this.state.provider, tracker);
      if (cam.provider === provider.CHATURBATE) {
        let trackerAddon = this.constant.getTrackerAddon(tracker);
        cam.embedUrl = cam.embedUrl.replace("track=embed", "track=" + trackerAddon);
        cam.chatRoomUrl = cam.chatRoomUrl.replace("track=default", "track=" + trackerAddon);
      }
      this.cam = cam;
      this.initEmbedUrl();
      this.initSocialMediaCard();
      this.initCountryName();
    })
  }

  private initEmbedUrl(): void {
    this.embedUrl = this.screen.isHiddenLgUp() && this.cam.embedUrlMobile ?
      this.cam.embedUrlMobile : this.cam.embedUrl;
    console.log(this.embedUrl);
  }

  private initCountryName(): void {
    if (this.cam.country) {
      this.countryName = this.countryS.getName(this.cam.country);
      this.countryUrl = this.countryS.getUrl(this.cam.country);
    }
  }

  private setRelatedCams(res: CamResult) : void {
    if (res.cams.length > 1) {
      this.relatedCams = res.cams.slice(1, res.cams.length);
    }
  }

  private initSocialMediaCard(): void {
    this.socialMedia.updateTwitterCard(this.cam);
  }

  public openChatroom(): void {
    ClientUtil.isClientSide() && window.open(this.cam.chatRoomUrl, "_blank");
  }

  private toLoadedCamsString() : string[] {
    return this.loadedCams.map(r=>r.cams[0].username);
  }

  private shiftLoadedCams(): void {
    if(this.loadedCams.length > DetailComponent.MAX_LOADED_CAMS_SIZE) {
      this.loadedCams.shift();
      this.randomCamIndex -= 1;
    }
  }

}
