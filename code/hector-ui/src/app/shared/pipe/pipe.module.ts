import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SafePipe} from './safe/safe.pipe';
import {SafeHtmlPipe} from './safe-html/safe-html.pipe';
import {DateStringPipe} from './date-string/date-string.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SafePipe,
    SafeHtmlPipe,
    DateStringPipe
  ],
  exports: [
    SafePipe,
    SafeHtmlPipe,
    DateStringPipe
  ]
})
export class PipeModule { }
