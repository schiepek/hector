import {CamFunction} from './cam-function';
import {Cam} from '../model/cam';
import {CamResult} from '../model/cam-result';

export class AutocompleteFunction extends CamFunction {

    public static readonly RESULTS_MAX_SIZE: number = 10;

    constructor(queryString: any) {
        super(queryString,
            (cams) => {
                let query = queryString["query"];
                return cams
                    .filter(c => AutocompleteFunction.filterByUserName(c, query))
                    .map(c => AutocompleteFunction.mapToReducedCam(c))
                    .sort((a,b) => AutocompleteFunction.sortByStartsWith(a,b,query))
                    .slice(0, AutocompleteFunction.RESULTS_MAX_SIZE)
            });
    }

    private static filterByUserName(cam: Cam, querystring: string): boolean {
        return cam.username.toLowerCase().includes(querystring.toLowerCase());
    }

    private static mapToReducedCam(cam: Cam): Cam {
        let reducedCam = new Cam();
        reducedCam.username = cam.username;
        reducedCam.provider = cam.provider;
        return reducedCam;
    }

    private static sortByStartsWith(a: Cam, b: Cam, searchTerm: string): number {
        let aStartsWith = a.username.toLowerCase().startsWith(searchTerm.toLowerCase());
        let bStartsWith = b.username.toLowerCase().startsWith(searchTerm.toLowerCase());

        if (aStartsWith && !bStartsWith) {
            return -1;
        } else if (!aStartsWith && bStartsWith) {
            return 1;
        } else {
            return a.username.toLowerCase().localeCompare(b.username.toLowerCase());
        }
    }

    public updateResult(result: CamResult): CamResult {
        let autoCompleteCams = this.processCams(result.cams);
        return new CamResult(autoCompleteCams, [], autoCompleteCams.length, 0, undefined, undefined, [], undefined);
    }

}
