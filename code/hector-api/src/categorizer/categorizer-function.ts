import {Cam} from '../model/cam';
import {SpecificCategorizerFunction} from './specific-categorizer-function';
import {CategoryCode} from '../model/category-code';

export class CategorizerFunction {

    public code: CategoryCode;
    protected specificFunction: SpecificCategorizerFunction;

    constructor(code: CategoryCode, specificFunction: SpecificCategorizerFunction) {
        this.code = code;
        this.specificFunction = specificFunction;
    }

    public isMatchingCategory(cam: Cam): boolean {
        return this.specificFunction(cam);
    }

}