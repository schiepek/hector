import {Cam} from './cam';
import {ExpirationData} from '../util/expiration-data';

export class ProviderCamResult {

    cams: Cam[];
    size?: number;
    date?: Date;

    constructor(cams: Cam[], size?: number, date?: Date) {
        this.cams = cams;
        this.size = size;
        this.date = date;
    }

    public static expiredEmpty(): ProviderCamResult {
        return new ProviderCamResult([], 0, ExpirationData.createExpiredDate())
    }
}
