export default {
    "couple": [
        "c",
        "couple",
        "maleFemale",
        "females"
    ],
    "female": [
        "f",
        "female",
        "girl"
    ],
    "male": [
        "m",
        "male",
        "gay",
        "males"
    ],
    "shemale": [
        "t",
        "s",
        "shemale",
        "transgender",
        "transexual",
        "trans",
        "trannies",
        "maleTranny",
        "femaleTranny",
        "tranny"
    ]
}
