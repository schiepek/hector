export class Country {

    code: string;
    size: number;

    constructor(code: string, size: number) {
        this.code = code;
        this.size = size;
    }

}