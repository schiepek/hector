import {CamFunction} from './cam-function';
import {Cam} from '../model/cam';
import {provider} from '../model/provider';

export class SortFunction extends CamFunction {

    private readonly maxAge: number = 85;
    private readonly minAge: number = 18;
    private static readonly avgCount: number = 15;
    private static readonly bongaCamsBoostFactor = -0.2;
    private static readonly camSodaBoostFactor = 0.05;
    private static readonly stripchatBoostFactor = 0.3;

    constructor(queryString: any) {
        super(queryString,
            (cams) => {
                switch (queryString) {
                    case "balanced":
                        return SortFunction.sortBalanced(cams);
                    case "most":
                        return SortFunction.sortByUsers(cams);
                    case "youngest":
                        let noChildCams = cams.filter(c => c.age >= this.minAge);
                        return noChildCams.sort((l, r) => l.age - r.age);
                    case "oldest":
                        let noFakeCams = cams.filter(c => c.age < this.maxAge);
                        return noFakeCams.sort((l, r) => r.age - l.age);
                    default:
                        return cams;
                }
            });
    }

    private static sortBalanced(cams: Cam[]): Cam[] {
        if(this.oneProviderIsMissing(cams, provider.CHATURBATE, provider.BONGACAMS, provider.CAMSODA, provider.STRIPCHAT)) {
            return SortFunction.sortByUsers(cams);
        }

        let sortedAvgCams = this.getAvgScores(cams, provider.CHATURBATE, provider.BONGACAMS, provider.CAMSODA, provider.STRIPCHAT);

        if(sortedAvgCams.length === 0 || sortedAvgCams[0].provider !== provider.CHATURBATE) {
            console.error("first provider was not chaturbate");
            return SortFunction.sortByUsers(cams);
        }

        this.assignFactor(sortedAvgCams);

        let avgCamsWithoutHighestProvider = sortedAvgCams.slice(1);

        return cams.sort((l, r) => {
            let leftAvg = avgCamsWithoutHighestProvider.find(avg=>avg.provider === l.provider);
            let rightAvg = avgCamsWithoutHighestProvider.find(avg=>avg.provider === r.provider);
            let leftVal = leftAvg ? leftAvg.factor * l.numberOfUsers : l.numberOfUsers;
            let rightVal = rightAvg ? rightAvg.factor * r.numberOfUsers : r.numberOfUsers;
            return rightVal - leftVal
        });
    }

    private static oneProviderIsMissing(cams: Cam[], ...providers: string[]): boolean {
        for (let prov of providers) {
            if (!cams.some(c => c.provider === prov)) {
                return true;
            }
        }
        return false
    }

    private static assignFactor(avgCams: AvgCam[]): void {
        let highValue = avgCams[0].avg;

        for(let avg of avgCams.slice(1)) {
            let lowValue = avg.avg;
            avg.factor = (lowValue + ((highValue - lowValue) * SortFunction.getBoostFactor(avg.provider))) / lowValue;
        }

    }

    private static getBoostFactor(p: provider): number {
        switch (p) {
            case provider.BONGACAMS:
                return this.bongaCamsBoostFactor;
            case provider.CAMSODA:
                return this.camSodaBoostFactor;
            case provider.STRIPCHAT:
                return this.stripchatBoostFactor;
            default:
                return 0;
        }
    }

    private static getAvgScores(cams: Cam[], ...providers: string[]): AvgCam[] {
        const avgScores: AvgCam[] = [];
        const avgCount = this.avgCount;

        providers.forEach(p => {
            let avgScore = cams
                .filter(c => c.provider === p)
                .slice(0, avgCount)
                .reduce((sum, b) => sum + b.numberOfUsers, 0) / avgCount;
            avgScores.push({provider: p, avg: avgScore, factor: 0});
        });
        return avgScores.sort((l, r) => r.avg - l.avg);
    }

    private static sortByUsers(cams: Cam[]): Cam[] {
        return cams.sort((l, r) => r.numberOfUsers - l.numberOfUsers);
    }

}

interface AvgCam {
    provider: string;
    avg: number;
    factor: number;
}
