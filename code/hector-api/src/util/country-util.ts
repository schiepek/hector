import {Country} from '../model/country';
import {Cam} from '../model/cam';
export class CountryUtil {

    public static getCountrySet(cams: Cam[]): Country[] {
        let countries: Country[] = [];

        let set = new Set(cams.map(c => c.country));
        set.forEach(k => (k) ? countries.push((new Country(k, cams.filter(c => c.country === k).length))) : null);

        return countries.sort((l, r) => r.size - l.size);
    }
}