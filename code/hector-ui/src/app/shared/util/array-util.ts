export class ArrayUtil {

  /**
   * @param cols the number of columns of the grid
   * @param rows the number of columns of the grid
   * @param array the original array
   * @returns a two dimensional array with a certain number of columns
   */
  public static getGridArray(cols: number, rows: number, array: Object[]): Object[][] {
    let i = 0;
    let j = 0;


    for (var gridArray = []; gridArray.push([]) < cols;);

    let realRows = rows? rows: Math.floor(array.length / cols) + 1;

    for (let object of array) {
      gridArray[i][j] = object;
      j++;
      if (j == realRows) {
        j = 0;
        i++;
      }
      if (i == cols) {
        return gridArray;
      }
    }

    return gridArray;
  }

  /**
   * @param cols the number of columns of the grid
   * @param rows the number of columns of the grid
   * @param array the original array
   * @returns a two dimensional array with a certain number of columns
   */
  public static getGridArray2(cols: number, rows: number, array: Object[]): Object[][] {
    let i = 0;
    let j = 0;


    for (var gridArray = []; gridArray.push([]) < cols;);

    let realRows = rows? rows: Math.floor(array.length / cols) + 1;

    for (let object of array) {
      gridArray[i][j] = object;
      i++;
      if (i == cols) {
        i = 0;
        j++;
      }
      if (j == realRows) {
        return gridArray;
      }
    }

    return gridArray;
  }

  public static getGridArrayRemoveLast(cols: number, rows: number, array: Object[]): Object[][] {
    let grid = this.getGridArray(cols, rows, array);
    grid[cols - 1] = grid[cols - 1].splice(0, rows-1);
    return grid;
  }

  public static getRandomElement(elements: any[]): any {
    if(elements && elements.length > 0) {
      return elements[Math.floor(Math.random() * elements.length)];
    }
    return null;
  }

}
