import {CategorizerFunction} from './categorizer-function';
import {Cam} from '../model/cam';
import {AsianCategorizer} from './asian-categorizer';
import {CamUtil} from '../util/cam-util';
import {LatinaCategorizer} from './latina-categorizer';
import {CategoryCode} from '../model/category-code';
export class Categorizer {

    private functions: CategorizerFunction[] = [];

    constructor() {
        this.functions =
            [
                new AsianCategorizer(),
                new CategorizerFunction(CategoryCode.bdsm, c => CamUtil.containsBasicInfo(c, "bdsm", "fetish", "sadomaso", "bondage", "latex") && CamUtil.contentIsNotFaky(c)),
                new CategorizerFunction(CategoryCode.bigdick, c => CamUtil.containsBasicInfo(c, "bigdick", "bigcock", "hugedick", "hugecock", "monstercock") && c.gender !== "female"),
                new CategorizerFunction(CategoryCode.bigtits, c => CamUtil.containsBasicInfo(c, "bbw", "bigtits", "hugetits", "bigboobs") && c.gender !== "male"),
                new CategorizerFunction(CategoryCode.ebony, c => CamUtil.containsBasicInfoButNot(c, ["asian"], "ebony")),
                new CategorizerFunction(CategoryCode.hairy, c => CamUtil.containsBasicInfo(c, "hairy") && CamUtil.contentIsNotFaky(c)),
                new LatinaCategorizer(),
                new CategorizerFunction(CategoryCode.mature, c => (c.age > 40 && c.age < 76) || (CamUtil.containsBasicInfo(c, "mature") && CamUtil.contentIsNotFaky(c))),
                new CategorizerFunction(CategoryCode.milf, c => c.age > 25 && c.gender !== "male" && CamUtil.containsBasicInfo(c, "milf") && CamUtil.contentIsNotFaky(c)),
                new CategorizerFunction(CategoryCode.smalltits, c => CamUtil.containsBasicInfo(c, "smalltits", "tinytits", "smallboobs")),
                new CategorizerFunction(CategoryCode.tattoo, c => CamUtil.containsBasicInfo(c, "tattoo", "tatuaje")),
                new CategorizerFunction(CategoryCode.teen, c => CamUtil.containsInTags(c, "teen") && CamUtil.contentIsNotFaky(c))
            ];
    }

    public getCamCategories(cam: Cam): string[] {
        return this.functions.filter(f => f.isMatchingCategory(cam)).map(f => CategoryCode[f.code]);
    }

    public filterByCategory(cams: Cam[], code: CategoryCode): Cam[] {
        let func = this.functions.find(f => f.code === code);
        return func ? cams.filter(c => func ? func.isMatchingCategory(c) : null) : cams;
    }

}
