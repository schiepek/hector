import {StringUtil} from '../../../hector-api/src/util/string-util';
import {assert} from 'chai';
import {CamUtil} from '../../../hector-api/src/util/cam-util';
import {AsianCategorizer} from '../../../hector-api/src/categorizer/asian-categorizer';
import {LatinaCategorizer} from '../../../hector-api/src/categorizer/latina-categorizer';
import {AbstractTest} from './abstract-test';
import {ExpiredTest} from './expired-test';
import {TestType} from '../general/test-type';


export class CategoryTest extends AbstractTest {

    protected setTestType() {
        this.type = TestType.BACKEND;
    }

    protected initTests(): void {
        this.tests.set("category-asian", (executor, config) => {
            executor.executeRestTestCams('should return cams with asian countries or tag asian', {
                    page: 1,
                    category: "asian"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 asian cams");

                    r.cams.forEach(c => {
                        let basicContent = CamUtil.getBasicContent(c);
                        assert.isTrue(c.categories.some(ca => ca === "asian"), "category should be set");
                        if (!new AsianCategorizer().isAsianCountry(c)) {
                            assert.isTrue(StringUtil.containsCaseIns(basicContent, "asian"), "content should contain term 'asian'");
                            assert.isFalse(StringUtil.containsCaseIns(basicContent, "german"), "content should not contain term 'german'");
                            assert.isFalse(StringUtil.containsCaseIns(basicContent, "french"), "content should not contain term 'french'");
                            assert.isFalse(StringUtil.containsCaseIns(basicContent, "ebnony"), "content should not contain term 'ebony'");
                            assert.isTrue(ExpiredTest.dateIsNotExpired(r,config), "the S3 Data shouldn't be expired")
                        }
                    })
                }
            );
        });


        this.tests.set("category-bdsm", (executor) => {
            executor.executeRestTestCams('should return cams with bdsm/festish and should be have no faky content', {
                    page: 1,
                    category: "bdsm"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 bdsm cams");
                    r.cams.forEach(c => {
                        let basicContent = CamUtil.getBasicContent(c);
                        assert.isTrue(c.categories.some(ca => ca === "bdsm"), "category should be set");
                        assert.isTrue(StringUtil.containsCaseInsArr(basicContent, ["bdsm", "fetish", "sadomaso", "bondage", "latex"]));
                        assert.isTrue(CamUtil.contentIsNotFaky(c));
                    })

                }
            );
        });

        this.tests.set("category-bigdick", (executor) => {
            executor.executeRestTestCams('should return cams with big dicks and should not be female', {
                    page: 1,
                    category: "bigdick"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 big dick cams");

                    r.cams.forEach(c => {
                        let basicContent = CamUtil.getBasicContent(c);
                        assert.isTrue(c.categories.some(ca => ca === "bigdick"), "category should be set");
                        assert.isTrue(StringUtil.containsCaseInsArr(basicContent, ["bigdick", "bigcock", "hugedick", "hugecock", "monstercock"]));
                        assert.isTrue(c.gender !== "female");
                    })

                }
            );
        });

        this.tests.set("category-bigtits", (executor) => {
            executor.executeRestTestCams('should return cams with big tits and should not be male', {
                    page: 1,
                    category: "bigtits"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 big tits cams");

                    r.cams.forEach(c => {
                        let basicContent = CamUtil.getBasicContent(c);
                        assert.isTrue(c.categories.some(ca => ca === "bigtits"), "category should be set");
                        assert.isTrue(StringUtil.containsCaseInsArr(basicContent, ["bbw", "bigtits", "hugetits", "bigboobs"]));
                        assert.isTrue(c.gender !== "male");
                    })

                }
            );
        });

        this.tests.set("category-ebony", (executor) => {
            executor.executeRestTestCams('should return cams with ebony and should not contain cams with asian', {
                    page: 1,
                    category: "ebony"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 ebony cams");

                    r.cams.forEach(c => {
                        let basicContent = CamUtil.getBasicContent(c);
                        assert.isTrue(c.categories.some(ca => ca === "ebony"), "category should be set");
                        assert.isTrue(StringUtil.containsCaseIns(basicContent, "ebony"), "cam should contain 'ebony'");
                        assert.isFalse(StringUtil.containsCaseIns(basicContent, "asian"), "cam should not contain 'asian'");
                    })

                }
            );
        });

        this.tests.set("category-hairy", (executor) => {
            executor.executeRestTestCams('should return cams with hairy and no faky cams', {
                    page: 1,
                    category: "hairy"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 hairy cams");

                    r.cams.forEach(c => {
                        let basicContent = CamUtil.getBasicContent(c);
                        assert.isTrue(c.categories.some(ca => ca === "hairy"), "category should be set");
                        assert.isTrue(StringUtil.containsCaseIns(basicContent, "hairy"), "cam should contain 'hairy'");
                        assert.isTrue(CamUtil.contentIsNotFaky(c), "cam content should not be fake for hairy");
                    })

                }
            );
        });

        this.tests.set("category-latina", (executor) => {
            executor.executeRestTestCams('should return cams with latina and not german, french or asian', {
                    page: 1,
                    category: "latina"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 latin cams");

                    r.cams.forEach(c => {
                        let basicContent = CamUtil.getBasicContent(c);
                        assert.isTrue(c.categories.some(ca => ca === "latina"), "category should be set");
                        let categorizer = new LatinaCategorizer();
                        if (!categorizer.isLatinCountry(c) && StringUtil.containsCaseIns(basicContent, "latin")) {
                            assert.isFalse(StringUtil.containsCaseIns(basicContent, "german"), "content should not contain term 'german'");
                            assert.isFalse(StringUtil.containsCaseIns(basicContent, "french"), "content should not contain term 'french'");
                        } else {
                            assert.isTrue(new LatinaCategorizer().isLatinCountry(c), "country should be latin if there is no 'latin' string");
                        }
                    })

                }
            );
        });

        this.tests.set("category-mature", (executor) => {
            executor.executeRestTestCams('should return cams with mature and age between 40 and 76', {
                    page: 1,
                    category: "mature"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 mature cams");

                    r.cams.forEach(c => {
                        let basicContent = CamUtil.getBasicContent(c);
                        assert.isTrue(c.categories.some(ca => ca === "mature"), "category should be set");
                        let isMature = StringUtil.containsCaseIns(basicContent, "mature");
                        let isNotFake = CamUtil.contentIsNotFaky(c);
                        let isCorrectAge = c.age > 40 && c.age < 76;

                        assert.isTrue(isCorrectAge || (isMature && isNotFake), "should match to the mature conditions");
                    })

                }
            );
        });

        this.tests.set("category-milf", (executor) => {
            executor.executeRestTestCams('should return cams with milfs and age > 25', {
                    page: 1,
                    category: "milf"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 milf cams");

                    r.cams.forEach(c => {
                        assert.isTrue(c.age > 25, "age should be above 25 as a milf");
                        assert.isTrue(c.categories.some(ca => ca === "milf"), "category should be set");
                        assert.isTrue(CamUtil.contentIsNotFaky(c), "content should not be faky with milf category");
                        assert.isTrue(CamUtil.containsBasicInfo(c, "milf"), "content should contain 'milf'");
                    })

                }
            );
        });

        this.tests.set("category-smalltits", (executor) => {
            executor.executeRestTestCams('should return cams with small tits', {
                    page: 1,
                    category: "smalltits"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 small tits cams");
                    assert.isTrue(r.cams.some(c => c.categories.some(ca => ca === "smalltits"), "category should be set"));
                    assert.isTrue(r.cams.some(c => CamUtil.containsBasicInfo(c, "smalltits", "tinytits")), "cam should contain the small tits key word");
                }
            );
        });

        this.tests.set("category-tattoo", (executor) => {
            executor.executeRestTestCams('should return cams with tattoo', {
                    page: 1,
                    category: "tattoo"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 tattoo cams");
                    assert.isTrue(r.cams.some(c => c.categories.some(ca => ca === "tattoo")), "cam should contain the teen key word");
                    assert.isTrue(r.cams.some(c => CamUtil.containsBasicInfo(c, "tattoo", "tatuaje")), "cam should contain the tattoo key word");
                }
            );
        });

        this.tests.set("category-teen", (executor) => {
            executor.executeRestTestCams('should return cams with teen', {
                    page: 1,
                    category: "teen"
                },
                r => {
                    assert.isTrue(r.cams.length > 10, "There should be more than 10 small tits cams");
                    assert.isTrue(r.cams.some(c => c.categories.some(ca => ca === "teen")), "cam should contain the teen key word");
                    assert.isTrue(r.cams.some(c => CamUtil.containsBasicInfo(c, "teen")), "cam should contain the teen key word");
                    assert.isTrue(r.cams.some(c => CamUtil.contentIsNotFaky(c)), "content should not be faky with teen category");

                }
            );
        });

    }
}
