import {Injectable} from '@angular/core';
import {ScreenService} from './screen.service';
import {ArrayUtil} from '../shared/util/array-util';
import {Category} from '../shared/model/category';
import {CategoryCode} from '../../../../hector-api/src/model/category-code';
import {CategoryFunction} from '../shared/model/category-function';
import {CountryService} from './country.service';
import {AutoCompleteType} from '../shared/model/auto-complete-type';
import {provider} from '../../../../hector-api/src/model/provider';
import {TagService} from './tag.service';
import {CamResult} from '../../../../hector-api/src/model/cam-result';
import {CategoryUI} from '../shared/model/CategoryUI';
import {CountryUI} from '../shared/model/country-ui';
import {CategoryItemType} from '../../../../hector-api/src/model/category-item-type';
import {StringUtil} from '../shared/util/string-util';

@Injectable()
export class CategoryService {

  public categoriesMenu: Category[][] = [];
  public tagCloud: Category[];
  public allCategories: Category[];
  public providers: Category[];

  private readonly noOfColumnsDesktop: number = 4;
  private readonly noOfRowsDesktop: number = 6;

  private readonly noOfColumnsMobile: number = 1;
  private readonly noOfRowsMobile: number = 24;

  private readonly columns: number;
  private readonly rows: number;


  constructor(private screen: ScreenService,
              private tagS: TagService,
              private country: CountryService) {
    this.columns = this.screen.isHiddenMdDown() ? this.noOfColumnsDesktop : this.noOfColumnsMobile;
    this.rows = this.screen.isHiddenMdDown() ? this.noOfRowsDesktop : this.noOfRowsMobile;
    this.initAllCategories();
    this.initProviders();
    this.initCategoriesMenu();
    this.initTagCloud();
  }

  public getFilterLink(currentCategories: Category[], code: CategoryCode): string {
    let categories = currentCategories.map(c => c);

    let newCat = this.allCategories.find(c => c.code === code);
    let existingCat = categories.find(c => c.group === newCat.group);

    if (existingCat && existingCat.code === newCat.code) {
      categories.splice(categories.indexOf(existingCat), 1);
    } else if (existingCat) {
      categories[categories.indexOf(existingCat)] = newCat;
    } else {
      categories.push(newCat);
    }
    categories = categories.filter(c => c.code !== CategoryCode.page);

    if (categories.filter(c => c.code === CategoryCode.query).length > 0) {
      return 'search/' + categories.map(c => c.path).join('/').toLowerCase();
    } else {
      return categories.length > 0 ? categories.map(c => c.path).join('/').toLowerCase() : '/cams';
    }

  }

  public prepareLiveCategories(result: CamResult, countries: CountryUI[]): CategoryUI[] {
    return result.categories.map(cat => {
      let name = StringUtil.capitalizeFirstLetter(cat.code);
      let link = cat.code;
      if (cat.type === CategoryItemType.COUNTRY) {
        let countryUI = countries.find(c => c.code === cat.code);
        name = countryUI ? countryUI.name : cat.code;
        link = countryUI ? countryUI.link : cat.code;
      }
      return new CategoryUI(cat.code, name, cat.size, link, cat.type);
    }).sort((a, b) => a.name.localeCompare(b.name));
  }


  private initAllCategories() {
    this.allCategories = [
      new Category(CategoryCode.eighteen, "eighteen", this.createAgeCatFunc(18, 18), AutoCompleteType[AutoCompleteType.age]),
      new Category(CategoryCode.asian, "asian", st => st.category = "asian", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.bdsm, "bdsm", st => st.category = "bdsm", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.bigdick, "bigdick", st => st.category = "bigdick", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.bigtits, "bigtits", st => st.category = "bigtits", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.blonde, "blonde", st => st.haircolor = "blonde", AutoCompleteType[AutoCompleteType.haircolor]),
      new Category(CategoryCode.brunette, "brunette", st => st.haircolor = "brunette", AutoCompleteType[AutoCompleteType.haircolor]),
      new Category(CategoryCode.couple, "couple", st => st.gender = "couple", AutoCompleteType[AutoCompleteType.gender]),
      new Category(CategoryCode.fat, "curvy", st => st.body = "fat", AutoCompleteType[AutoCompleteType.body]),
      new Category(CategoryCode.ebony, "ebony", st => st.category = "ebony", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.female, "female", st => st.gender = "female", AutoCompleteType[AutoCompleteType.gender]),
      new Category(CategoryCode.hairy, "hairy", st => st.category = "hairy", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.latina, "latina", st => st.category = "latina", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.male, "male", st => st.gender = "male", AutoCompleteType[AutoCompleteType.gender]),
      new Category(CategoryCode.mature, "mature", st => st.category = "mature", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.milf, "milf", st => st.category = "milf", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.new, "new", st => st.isNew = true, AutoCompleteType[AutoCompleteType.attribute]),
      new Category(CategoryCode.hd, "hd", st => st.isHd = true, AutoCompleteType[AutoCompleteType.attribute]),
      new Category(CategoryCode.petite, "petite", st => st.body = "petite", AutoCompleteType[AutoCompleteType.body]),
      new Category(CategoryCode.redhead, "redhead", st => st.haircolor = "redhead", AutoCompleteType[AutoCompleteType.haircolor]),
      new Category(CategoryCode.shemale, "shemale", st => st.gender = "shemale", AutoCompleteType[AutoCompleteType.gender]),
      new Category(CategoryCode.slim, "slim", st => st.body = "slim", AutoCompleteType[AutoCompleteType.body]),
      new Category(CategoryCode.smalltits, "smalltits", st => st.category = "smalltits", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.tattoo, "tattoo", st => st.category = "tattoo", AutoCompleteType[AutoCompleteType.category]),
      new Category(CategoryCode.teen, "teen", st => st.category = "teen", AutoCompleteType[AutoCompleteType.category]),

      new Category(CategoryCode.undertwenty, "undertwenty", this.createAgeCatFunc(18, 19), "age"),
      new Category(CategoryCode.twenties, "twenties", this.createAgeCatFunc(20, 30), "age"),
      new Category(CategoryCode.thirties, "thirties", this.createAgeCatFunc(31, 40), "age"),
      new Category(CategoryCode.overforty, "overforty", this.createAgeCatFunc(40, 75), "age"),

      new Category(CategoryCode.english, "english", st => st.language = "en", "language"),
      new Category(CategoryCode.spanish, "spanish", st => st.language = "es", "language"),
      new Category(CategoryCode.russian, "russian", st => st.language = "ru", "language"),
      new Category(CategoryCode.german, "german", st => st.language = "de", "language"),
      new Category(CategoryCode.french, "french", st => st.language = "fr", "language"),
      new Category(CategoryCode.italian, "italian", st => st.language = "it", "language"),

      new Category(CategoryCode.chaturbate, "chaturbate-cams", st => st.provider = provider.CHATURBATE, "provider"),
      new Category(CategoryCode.bongacams, "bongacams-cams", st => st.provider = provider.BONGACAMS, "provider"),
      new Category(CategoryCode.awempire, "livejasmin-cams", st => st.provider = provider.AWEMPIRE, "provider"),
      new Category(CategoryCode.camsoda, "camsoda-cams", st => st.provider = provider.CAMSODA, "provider"),
      new Category(CategoryCode.stripchat, "stripchat-cams", st => st.provider = provider.STRIPCHAT, "provider"),

    ];
    this.categoriesMenu = ArrayUtil.getGridArray(this.columns, this.rows, this.allCategories) as Category[][];
  }

  private initProviders() : void {
    this.providers = this.allCategories.filter(c=>c.group === "provider");
  }

  public findCategory(parameter: string, key: string): Category {
    let param = parameter.toLowerCase();

    if (key === "query") {
      let queryCategory = this.retrieveCategoryOrTagOrCountry(param);
      return queryCategory ? queryCategory : new Category(CategoryCode.query, parameter, st => st.query = parameter, "query");
    }

    if (!isNaN(Number(parameter))) {
      return new Category(CategoryCode.page, parameter, st => st.page = +parameter, "page");
    }

    return this.retrieveCategoryOrTagOrCountry(param);
  }

  public categoryPathExists(categoryPath: string): boolean {
    return this.allCategories.map(c => c.path).includes(categoryPath)
  }

  public findProviderLink(prov: string) {
    return this.allCategories
      .find(c => c.group === "provider" && c.code === CategoryCode[prov]).path;
  }

  private retrieveCategoryOrTagOrCountry(param: string): Category {
    let toLowerCaseParam = param.toLowerCase();
    let category = this.allCategories.find(c => c.path.toLowerCase() === toLowerCaseParam.toLowerCase().replace(/\s+/g, ''));
    if (!category) {
      let code = this.country.getCode(toLowerCaseParam);
      category = code ? new Category(CategoryCode.country, this.country.getUrl(code), st => st.country = code.toLowerCase(), "country") : null;
    }
    if (!category) {
      let tag = this.tagS.getTag(toLowerCaseParam)
      return tag ? new Category(CategoryCode.tag, tag, st => st.tag = tag.toLowerCase(), "tag") : null;
    }
    return category;
  }

  private createAgeCatFunc(minAge: number, maxAge: number): CategoryFunction {
    return st => {
      st.minAge = minAge;
      st.maxAge = maxAge;
    }
  }

  private initCategoriesMenu(): void {
    let menuCategories = [
      CategoryCode.eighteen, CategoryCode.asian, CategoryCode.bdsm, CategoryCode.bigdick, CategoryCode.bigtits,
      CategoryCode.blonde, CategoryCode.brunette, CategoryCode.couple, CategoryCode.fat, CategoryCode.ebony,
      CategoryCode.female, CategoryCode.hairy, CategoryCode.latina, CategoryCode.male, CategoryCode.mature,
      CategoryCode.milf, CategoryCode.new, CategoryCode.petite, CategoryCode.redhead, CategoryCode.shemale,
      CategoryCode.slim, CategoryCode.smalltits, CategoryCode.tattoo, CategoryCode.teen].map(cc => this.allCategories.find(c => c.code === cc));
    this.categoriesMenu = ArrayUtil.getGridArray(this.columns, this.rows, menuCategories) as Category[][];
  }

  private initTagCloud(): void {
    this.tagCloud = [
      CategoryCode.female, CategoryCode.couple, CategoryCode.shemale, CategoryCode.male,

      CategoryCode.undertwenty, CategoryCode.twenties, CategoryCode.thirties, CategoryCode.overforty,

      CategoryCode.asian, CategoryCode.bdsm, CategoryCode.bigdick, CategoryCode.bigtits,
      CategoryCode.ebony, CategoryCode.hairy, CategoryCode.latina, CategoryCode.mature,
      CategoryCode.milf, CategoryCode.smalltits, CategoryCode.tattoo, CategoryCode.teen,

      CategoryCode.english, CategoryCode.spanish, CategoryCode.russian, CategoryCode.german, CategoryCode.french, CategoryCode.italian,

      CategoryCode.blonde, CategoryCode.brunette, CategoryCode.redhead,
      CategoryCode.slim, CategoryCode.petite, CategoryCode.fat,
      CategoryCode.new, CategoryCode.hd
    ].map(cc => this.allCategories.find(c => c.code === cc));
  }

}
