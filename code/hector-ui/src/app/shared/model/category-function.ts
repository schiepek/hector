import {StateService} from '../../services/state.service';

export interface CategoryFunction {
    (state: StateService): void;
}
