import {HttpParams} from '@angular/common/http';
import {StateService} from '../../services/state.service';
import {Parameters} from '../model/parameters';
import {SubUtil} from './sub-util';

export class RestUtil {

  public static getParams(state: StateService, func: string, path: string, project: string): HttpParams {
    let paramsMap = new Map([
      [Parameters.PAGE, state.page.toString()],
      [Parameters.MINAGE, state.minAge ? state.minAge.toString() : null],
      [Parameters.MAXAGE, state.maxAge ? state.maxAge.toString() : null],
      [Parameters.GENDER, state.gender ? state.gender : null],
      [Parameters.ISHD, state.isHd ? state.isHd.toString() : null],
      [Parameters.ISNEW, state.isNew ? state.isNew.toString() : null],
      [Parameters.COUNTRY, state.country ? state.country : null],
      [Parameters.LANGUAGE, state.language ? state.language : null],
      [Parameters.HAIRCOLOR, state.haircolor ? state.haircolor : null],
      [Parameters.BODY, state.body ? state.body : null],
      [Parameters.CATEGORY, state.category ? state.category : null],
      [Parameters.TAG, state.tag ? state.tag : null],
      [Parameters.QUERY, state.query ? state.query.slice(0, 30) : null],
      [Parameters.USERNAME, state.username ? state.username : null],
      [Parameters.PROVIDER, state.provider ? state.provider : null],
      [Parameters.FUNCTION, func],
      [Parameters.PATH, path],
      [Parameters.PROJECT, project],
      [Parameters.TIMESTAMP, new Date().getTime().toString()]
    ]);

    paramsMap = SubUtil.modifyParameters(paramsMap);

    return RestUtil.toHttpParams(paramsMap);
  }

  public static getQueryParams(state: StateService, func: string): HttpParams {
    let paramsMap = new Map([
      [Parameters.FUNCTION, func],
      [Parameters.QUERY, state.searchTerm],
    ]);

    return RestUtil.toHttpParams(paramsMap);
  }

  private static toHttpParams(paramsMap) : HttpParams {
    let params = new HttpParams();

    if (paramsMap && paramsMap.size > 0) {
      paramsMap.forEach((value: string, key: string) => {
        if (value) {
          params = params.append(key, value);
        }
      });
    }

    return params;
  }

}
